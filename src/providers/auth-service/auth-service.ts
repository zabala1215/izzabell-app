import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers, RequestOptions } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';

//Modelos
import { Usuario } from '../../custom_elems/modelos/usuario';
import { Ubicacion } from '../../custom_elems/modelos/ubicacion';


export class User {
  telefono: string;
  email: string;
  nombre: string;
  picture: string;
  tipo_plan: string;
  direccion_casa: string;
  direccion_trabajo: string;
  direccion_otro: string;
  id: string;

  constructor(telefono: string) {
    this.telefono = telefono;
  }
}

@Injectable()
export class AuthServiceProvider {

  public currentUser: User;
  private url:string="http://izzabell.com/api-kingbakery/login/";

  constructor(public http: Http, private nativeStorage: NativeStorage) {
    console.log('Proveedor de datos de solicitud');
  }

  //Función para realizar login
  public login(creds) {
    console.log("...Iniciando login...");
    let social = creds.telefono == null || creds.telefono == "" ? true : false;
    let respuesta = {estado: '', usuario: ''};
    if(creds === null) {
      //Datos nulos, no procede
      console.log("Datos nulos "+JSON.stringify(creds));
      return Observable.throw("Credenciales vacios");
    } else {
      //Se recibio credenciales, procedemos
      console.log("Se recibio credenciales, procedemos...");
      return Observable.create(observer => {
        //Cabecera de post
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });

        //Promise con consulta al servidor
        return new Promise((resolve, reject) => {
          console.log("Entramos a promise");
          this.http.post(this.url, JSON.stringify({telefono: creds.telefono, email: creds.email}), options)
            .map(res => res.json())
            .subscribe(res => {

              //Respuesta de login
              console.log("Recibimos respuesta "+JSON.stringify(res));

              if(res.login == true) {
                console.log("Login...");
                //Logueado correcto
                //Verificamos como se registro para comprobar
                if (!social) {
                  //Telefono, verificamos numeros
                  if(res.usuario.telefono != creds.telefono) {
                    console.log("Telefonos no iguales");
                    observer.next(false);observer.complete();
                  } 
                } else {
                  //Social, verificamos correos
                  if(res.usuario.email != creds.email) {
                    //Correos no iguales
                    observer.next(false);observer.complete();
                  }
                }

                //Guardamos en local
                this.saveLocal(res.usuario).then(() => {
                  console.log("Retorno de saveLocal");
                  //Verificamos si tiene el numero o el correo ingresado
                  console.log("TELEFONO: "+res.usuario.telefono);
                  console.log("EMAIL: "+res.usuario.email);
                  if(social) {
                    if(res.usuario.telefono == "" || res.usuario.telefono == null || res.usuario.telefono == " ") {
                      console.log("Telefono falta");
                      observer.next("tel");
                      observer.complete();
                    } else {
                      console.log("TELEFONO BIEN");
                      resolve(true);
                      observer.next(true);
                      observer.complete();
                    }
                  } else {
                    if(res.usuario.email == "" || res.usuario.email == null || res.usuario.email == " ") {
                      console.log("Correo falta");
                      observer.next("email");
                      observer.complete();
                    } else {
                      console.log("EMAIL BIEN");
                      resolve(true);
                      observer.next(true);
                      observer.complete();
                    }
                  }
                }, error => {
                  console.log("Error de saveLocal "+error);
                  observer.next(false);
                  observer.complete();
                });

              } else {
                //Procedemos a registro
                console.log("Registro...");
                respuesta.usuario = creds;
                if(social) {
                  respuesta.estado = "tel"; //Que datos necesitaremos
                  observer.next(respuesta);
                  observer.complete();
                } else {
                  respuesta.estado = "email"; //Que datos necesitaremos
                  observer.next(respuesta);
                  observer.complete();
                }
              }

            }, (error) => {
              console.log("Error en Verificacion servidor "+error);
              reject(error);
              observer.next(false);
              observer.complete();
            });
        });
      });
    }
  }

  //Función para realizar registro
  public registro(creds) {
    console.log("...Iniciando registro...");
    if(creds === null) {
      //Datos nulos, no procede
      console.log("Datos nulos "+JSON.stringify(creds));
      return Observable.throw("Credenciales vacios");
    } else {
      //Se recibio credenciales, procedemos
      console.log("Se recibio credenciales, procedemos...");
      return Observable.create(observer => {
        //Cabecera de post
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });

        //Promise con consulta al servidor
        return new Promise((resolve, reject) => {
          console.log("Entramos a promise");
          this.http.post("http://izzabell.com/api-kingbakery/usuarios/", JSON.stringify(creds), options)
            .map(res => res.json())
            .subscribe(res => {

              //Respuesta de registro
              console.log("Recibimos respuesta "+JSON.stringify(res));

              if(res.registro == true) {
                console.log("Registro...");
                //Registro correcto
                //Guardamos en local
                this.saveLocal(res.usuario).then(() => {
                  console.log("Retorno de saveLocal");
                  observer.next(true);
                  observer.complete();
                }, error => {
                  console.log("Error de saveLocal");
                  observer.next(false);
                  observer.complete();
                });

              } else {
                //Error durante registro
                console.log("Registro fallido...");
                observer.next(res.msg);
                observer.complete();
              }

            }, (error) => {
              console.log("Error en Verificacion servidor "+error);
              reject(error);
              observer.next(false);
              observer.complete();
            });
        });
      });
    }
  }

  //Guardar datos en local
  private saveLocal(usuario: any) {
    //Guardamos en local
    return new Promise((resolve, reject) => {
      this.nativeStorage.setItem('user', {
        id: usuario.id,
        nombre: usuario.nombre,
        telefono: usuario.telefono,
        email: usuario.email,
        picture: usuario.picture,
        facebook: usuario.facebook,
        idSocial: usuario.idSocial,
        latitud: usuario.latitud,
        longitud: usuario.longitud,
        altitud: usuario.altitud,
        tipo_plan: usuario.tipo_plan
      }).then(function() {
        console.log("Completado guardado -> PASAMOS A NUEVA IMPLEMENTACION");
        //resolve();
      }, function(err) {
        console.log("Error guardando datos");
        console.log(err);
        //reject(err);
      });

      //Nueva implementacion -> Modelo / Clase
      //Asignamos los datos a su respectivo modelo
      let dirCasa = new Ubicacion(usuario.casa.latitud, usuario.casa.longitud, usuario.casa.altitud, usuario.casa.exactitud, usuario.casa.direccion);
      console.log("DIR CASA "+JSON.stringify(dirCasa));
      let dirTrabajo = new Ubicacion(usuario.trabajo.latitud, usuario.trabajo.longitud, usuario.trabajo.altitud, usuario.trabajo.exactitud, usuario.trabajo.direccion);
      console.log("DIR TRABAJO "+JSON.stringify(dirTrabajo));
      var dirOtros: Ubicacion[] = []; //Otras ubicaciones
      console.log("DIR Otros INICIAL "+JSON.stringify(dirOtros));

      //Recorremos otras direcciones y populamos el arreglo
      for(var i = 0; i < usuario.otras_ubicaciones.length; i++) {
        console.log("DIR Otros ITEM "+JSON.stringify(usuario.otras_ubicaciones[i]));
        dirOtros.push(new Ubicacion(usuario.otras_ubicaciones[i].latitud, usuario.otras_ubicaciones[i].longitud, usuario.otras_ubicaciones[i].altitud, usuario.otras_ubicaciones[i].exactitud, usuario.otras_ubicaciones[i].direccion));
      }
      console.log("DIR Otros FINAL "+JSON.stringify(dirOtros));

      //Creamos el usuario
      let nuevoUsuario = new Usuario(usuario.id, usuario.nombre, usuario.telefono, usuario.email, usuario.picture, usuario.facebook, usuario.idSocial, dirCasa, dirTrabajo, dirOtros, usuario.tipo_plan, 0, 0);
      console.log("USUARIO FINAL "+JSON.stringify(nuevoUsuario));

      //Guardamos el usuario
      this.nativeStorage.setItem('usuario', nuevoUsuario).then(function() {
        console.log("SE GUARDO COMO NUEVA IMPLEMENTACION");
        resolve();
      }, error => {
        console.log("Error nuevo IMPLEMENTACION "+error);
        reject(error);
      });
    });
  }

  //Obtener usuario logueado
  usuarioLogueado(): Promise<Usuario> {
    let self = this;

    //Manejar condiciones
    return new Promise((resolver, rechazar) => {
      //Obtenemos
      self.nativeStorage.getItem("usuario")
        .then((usuario) => {
          console.log("AUTH SERVICE -> Se obtuvo usuario correcto");
          resolver(usuario);
        })
        .catch((error) => {
          console.log("AUTH SERVICE -> Error obteniendo usuario "+error);
          rechazar();
        });
    });
  }

  public register(credentials) {
    console.log(credentials);
    if (credentials.telefono === null || credentials.nombre === null) {
      return Observable.throw("Por favor inserte credenciales");
    } else {

      return Observable.create(observer => {

        console.log('entro?');
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });

        return new Promise((resolve, reject) => {
          console.log('Los datos son', JSON.stringify(credentials));
          this.http.post('http://izzabell.com/api-kingbakery/usuarios/', JSON.stringify(credentials), options)
            .map(res => res.json())
            .subscribe(res => {

              console.log('Subscribe aqui con res', JSON.stringify(res));

              if(res != null){
                console.log("La respuesta para el registro es", JSON.stringify(res));
                this.nativeStorage.setItem('user',
                {
                  id: res.id,
                  nombre: res.nombre,
                  telefono: res.telefono,
                  email: res.email,
                  picture: res.picture,
                  facebook: res.facebook,
                  idSocial: res.idSocial,
                  latitud: res.latitud,
                  longitud: res.longitud,
                  altitud: res.altitud,
                  direccion_casa: res.direccion_casa,
                  direccion_trabajo: res.direccion_trabajo,
                  direccion_otro: res.direccion_otro,
                  tipo_plan: res.tipo_plan,
                  password: res.password
                })
                .then(function(){
                  //Guardado correctamente
                  observer.next(true);
                  observer.complete();
                }, function() {
                  //Error guardando
                  observer.next(false);
                  observer.complete();
                })
              } else{
                //Res nulo
                console.log("Res nulo");
                observer.next(false);
                observer.complete();
              }
                // resolve(res);
            }, (err) => {
              reject(err);
            });
        });

      });
    }
  }

  public getUserInfo() {
    return this.currentUser;
  }

  // public loginSocial(credentials) {
  //   if (credentials.id === null && credentials.nombre === null) {
  //     return Observable.throw("Please insert credentials");
  //   } else {
  //     return Observable.create(observer => {
  //       let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
  //       let options = new RequestOptions({ headers: headers });

  //       let a = this;

  //       return new Promise((resolve, reject) => {
  //         this.http.post(this.url+'social', JSON.stringify(credentials), options)
  //           .map(res => res.json())
  //           .subscribe(res => {
  //             console.log("La respuesta es ", res.user);
  //             if(res.user != null){
  //               this.nativeStorage.setItem('user',
  //               {
  //                 id: res.user.id,
  //                 nombre: res.user.nombre,
  //                 email: res.user.email,
  //                 picture : res.user.picture
  //               })
  //               .then(function(){
  //                 observer.next(true);
  //                 observer.complete();
  //               })
  //             }else{
  //               console.log('No existe el usuario');
  //               this.http.post('https://izzabell.com/api-kingbakery/usuarios/social/', JSON.stringify(credentials), options)
  //                 .map(res => res.json())
  //                 .subscribe(res => {
  //                   console.log("La respuesta para el registro es", res);
  //                   if(res != null){
  //                     this.nativeStorage.setItem('user',
  //                     {
  //                       id: res.user.id,
  //                       nombre: res.user.nombre,
  //                       email: res.user.email,
  //                       picture : res.user.picture
  //                     })
  //                     .then(function(){
  //                       console.log('entro 5.0');
  //                       observer.next(true);
  //                       observer.complete();
  //                     })
  //                   }else{
  //                     observer.next(false);
  //                     observer.complete();
  //                   }
  //                     // resolve(res);
  //                 }, (err) => {
  //                   reject(err);
  //                 });
  //             }
  //               // resolve(res);
  //           }, (err) => {
  //             reject(err);
  //           });
  //       });
  //       // At this point make a request to your backend to make a real check!
  //     });
  //   }
  // }

  // public registerSocial(credentials) {
  //   console.log(credentials);
  //   console.log('Entro?');
  //   if (credentials.id === null || credentials.nombre === null) {
  //     console.log('Entro2.0?');
  //     return Observable.throw("Por favor inserte credenciales");

  //   } else {

  //     let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
  //     let options = new RequestOptions({ headers: headers });

  //     this.http.post('https://izzabell.com/api-kingbakery/usuarios/social/', JSON.stringify(credentials), options)
  //       .map(res => res.json())
  //       .subscribe(res => {
  //         console.log("La respuesta para el registro es", res);
  //         if(res != null){
  //           this.nativeStorage.setItem('user',
  //           {
  //             id: res.user.id,
  //             nombre: res.user.nombre,
  //             email: res.user.email,
  //             picture : res.user.picture
  //           })
  //           .then(function(){
  //             observer.next(true);
  //             observer.complete();
  //           })
  //         }else{
  //           observer.next(false);
  //           observer.complete();
  //         }
  //           // resolve(res);
  //       }, (err) => {
  //         reject(err);
  //       });
  //   }
  // }

  public logout() {
    return Observable.create(observer => {
      this.currentUser = null;
      this.nativeStorage.clear();
      console.log('Cerrando sesion...');
      observer.next(true);
      observer.complete();
    });
  }

}
