import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

//Modelos
import { SolicitudCajero } from '../../custom_elems/modelos/solicitud_cajero';
import { MetodoPago } from '../../custom_elems/modelos/metodo_pago';
import { Ubicacion } from '../../custom_elems/modelos/ubicacion';

@Injectable()
export class CajeroProvider {

  URL: string = "http://www.izzabell.com/api-kingbakery/cajero/";

  constructor(public http: Http) {
    console.log('Hello CajeroProvider Provider');
  }

  addCajero(solicitud: SolicitudCajero) {
    console.log("Enviando "+JSON.stringify(solicitud));
    //Asignamos
    let self = this;
    //Cabecera de post
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return Observable.create(resolver => {
      //Verificamos
      if(solicitud.ubicacion === null || solicitud.metodo_pago === null || solicitud.monto === null || solicitud.usuario === null) {
        console.log("No paso la verificacion");
        resolver.next(false);
        resolver.complete();
      }else {
        //Datos correctos
        self.http.post(self.URL, JSON.stringify(solicitud), options).map(respuesta => respuesta.json())
          .subscribe(respuesta => {
            //Recibimos los datos
            console.log("DATOS RECIBIDOS "+JSON.stringify(respuesta));
            resolver.next(respuesta);
            resolver.complete();
          }, error => {
            console.log("Error "+error);
            resolver.next(false);
            resolver.complete();
          });
      }

    });
  }

}
