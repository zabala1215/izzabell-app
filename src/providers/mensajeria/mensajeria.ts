import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

//Modelos
import { SolicitudMensaje } from '../../custom_elems/solicitud_mensaje/solicitud_mensaje';

@Injectable()
export class MensajeriaProvider {

  url: string = "http://www.izzabell.com/api-kingbakery"; //URL Base

  constructor(public http: Http) {

  }

  addMensajeria(solicitud: SolicitudMensaje) {
    console.log("Enviando "+JSON.stringify(solicitud));
    //Asignamos
    let self = this;
    //Cabecera de post
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return Observable.create(resolver => {
      //Verificamos
      if(solicitud.ubicacion === null || solicitud.metodo_pago === null || solicitud.texto === null || solicitud.usuario === null) {
        console.log("No paso la verificacion");
        resolver.next(false);
        resolver.complete();
      }else {
        //Datos correctos
        self.http.post(self.url+"/mensajeria/", JSON.stringify(solicitud), options).map(respuesta => respuesta.json())
          .subscribe(respuesta => {
            //Recibimos los datos
            console.log("DATOS RECIBIDOS "+JSON.stringify(respuesta));
            resolver.next(respuesta);
            resolver.complete();
          }, error => {
            console.log("Error "+error);
            resolver.next(false);
            resolver.complete();
          });
      }

    });
  }

}
