import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers, RequestOptions } from '@angular/http';
import { NativeStorage } from '@ionic-native/native-storage';

@Injectable()
export class UsuarioServiceProvider {

  // private allTask:Task[]=[];
  data: any;
  private url:string="http://izzabell.com/api-kingbakery/usuarios/";

  constructor(public http: Http, private nativeStorage: NativeStorage) {
    console.log('Proveedor de datos de solicitud');
  }

  getAllUsuarios() {
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http.get(this.url)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }


  deleteUsuario(data) {
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({ headers: headers });
    // return this.http.delete(this.url+item.Id, options)
    //   .map((response:Response)=>response.json());
  }

  addUsuario(data) {
    console.log(data);
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    return new Promise((resolve, reject) => {
      this.http.post(this.url, data, options)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  getUsuarioId(id:any) {
    // let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    // let options = new RequestOptions({ headers: headers });

    return new Promise(resolve => {
      this.http.get(this.url+id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  getPlan(id:any) {
    // let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    // let options = new RequestOptions({ headers: headers });

    return new Promise(resolve => {
      this.http.get(this.url+'plan/'+id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  editColUsuario(image , data){

    let datos: any = {};

    //Recuperamos los datos locales del usuario
    console.log("--------------------Recuperacion de datos L-------------------------");
    this.nativeStorage.getItem('user')
      .then(
        data => datos = data,
        error => console.error(error)
      );

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    console.log('La data de la imagen es:', data);

    return new Promise((resolve, reject) => {
      this.http.put(this.url+'item', data, options)
        .subscribe(res => {
          datos["picture"] = image;
          this.nativeStorage.setItem('user', datos).then(function() {
            //Enviamos que todo esta correcto
            resolve(res);
          }, function(err) {
            //Mostramos error
            console.log(err)
          });
        }, (err) => {
          reject(err);
        });
    });
  }

  editDireccion(data, latitud, longitud, id, dir){

    let datos: any = {};

    //Recuperamos los datos locales del usuario
    console.log("--------------------Recuperacion de datos L-------------------------");
    this.nativeStorage.getItem('user')
      .then(
        data => datos = data,
        error => console.error(error)
      );

    //Cabecera
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    //Asignamos query de envio
    let queryEnvio = 'direccion='+data+'&latitud='+latitud+'&longitud='+longitud+'&id='+id;

    console.log("Query: "+queryEnvio);

    return new Promise((resolve, reject) => {
      console.log("Dirección: "+this.url+"direccion-"+dir);
      //Enviamos la direccion al servidor
      this.http.put(this.url+"direccion-"+dir, queryEnvio, options)
        .subscribe(res => {
          //Guardamos dirección localmente
          switch(dir) {
            case "casa":
              datos["direccion_casa"] = data;
              datos["casa_lat"] = latitud;
              datos["casa_long"] = longitud;
              console.log("Guardamos casa");
              break;
            case "trabajo":
              datos["direccion_trabajo"] = data;
              datos["trabajo_lat"] = latitud;
              datos["trabajo_long"] = longitud;
              console.log("Guardamos trabajo");
              break;
            case "otro":
              datos["direccion_otro"] = data;
              datos["otro_lat"] = latitud;
              datos["otro_long"] = longitud;
              console.log("Guardamos otro");
              break;
          }
          this.nativeStorage.setItem('user', datos).then(function() {
              //Enviamos que todo esta correcto
              resolve(res);
            }, function(err) {
              //Mostramos error
              console.log(err)
            });
        }, (err) => {
          reject(err);
        });
    });
  }


  //Editar el plan
  editPlan(plan, id){
    console.log("Editando.............");

    //Datos de usuario para modificar
    let usuario = {};
    this.nativeStorage.getItem('user')
        .then(
            usu => {
              usuario = usu;
            },
            err => console.log("Error obteniendo usuario "+err)
        );

    //Query de modificación
    let query = 'plan='+plan+'&user_id='+id;
    console.log("Query --- "+query);

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    return new Promise((resolve, reject) => {
      console.log("Promise---------");
      this.http.put(this.url+'plan', query, options)
        .subscribe(res => {
          console.log("Envio datos----------");
          //Datos correctos, guardamos
          usuario["tipo_plan"] = plan;
          this.nativeStorage.setItem('user', usuario)
              .then(
                  data => {
                    //Guardado correctamente
                    console.log("-----Guardado--");
                    console.log(JSON.stringify(data));
                    resolve();
                  },
                  error => {
                    console.log("Error guardando usuario"+error);
                    reject(error);
                  }
              );

        }, (err) => {
          reject(err);
        });
    });
  }

  editUsuario(data){
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    console.log('El nombre es:', data.nombre);
    console.log('El telefono es:', data.telefono);
    console.log('El email es:', data.email);
    console.log('La imagen es:', data.picture);

    return new Promise((resolve, reject) => {
      this.http.put(this.url, JSON.stringify(data), options)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

}
