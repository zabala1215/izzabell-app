import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers, RequestOptions } from '@angular/http';
import { NativeStorage } from "@ionic-native/native-storage";

@Injectable()
export class SolicitudserviceProvider {

  // private allTask:Task[]=[];
  data: any;
  private url:string="http://izzabell.com/api-kingbakery/solicitudes/";
  private url2:string="http://izzabell.com/api-kingbakery/servicios-pre/";
  private url3:string="http://izzabell.com/api-kingbakery/servicios/";

  constructor(public http: Http, private nativeStorage: NativeStorage) {
    console.log('Proveedor de datos de solicitud');
  }

  getAllSolicitudes() {
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http.get(this.url)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  getSolicitudesUsuario(id) {

    return new Promise(resolve => {
      this.http.get(this.url+'usuarios/'+id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  getServiciosUsuario(id) {

    return new Promise(resolve => {
      this.http.get(this.url2+'usuarios/'+id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  getPagosUsuario(id) {

    return new Promise(resolve => {
      this.http.get(this.url3+'usuarios/'+id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  getSolicitudesComentarios(id) {

    console.log('Traer nuevos comentarios con id', id);
    return new Promise(resolve => {
      this.http.get(this.url+'comentarios/'+id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  getSolicitud(id) {

    return new Promise(resolve => {
      this.http.get(this.url+id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  deleteSolicitud(id) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    
    console.log('El id a borrar es', id);
    return new Promise((resolve, reject) => {
      this.http.delete(this.url+id, options)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  addSolicitud(data) {
    
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    console.log('La data de la solicitud a agregar es: ', data);
    return new Promise((resolve, reject) => {
      this.http.post(this.url, data, options)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          console.log("Error en Solicitud Service "+err);
          reject(false);
        });
    });
  }

  addSolicitud2(solicitud, adjunto, direccionSolicitud, direccionActual, metodoPago) {
    let me = this;
    let usuario; //Datos del usuario

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    return new Promise((accept,reject) => {
      //Obtenemos el usuario realizando el pago
      me.nativeStorage.getItem('user')
        .then(user => {
          let enviar = "solicitud="+solicitud+"&adjunto="+adjunto+"&direccionSolicitud="+direccionSolicitud+"&direccionActual="+direccionActual+"&metodoPago="+metodoPago+"&tipo=5&id_usuario="+user.id;
          console.log("DATOS ENVIADOS PARA LA NUEVA SOLICITUD DE SERVICIOS: "+enviar);
          //Se encontro usuario, se procede a agregar nuevo pago
          this.http.post(this.url+"cualquiera/", enviar, options)
            .map(res => res.json())
            .subscribe(respuesta => {
              console.log("Respuesta de SERVIDOR "+JSON.stringify(respuesta));
              //Recibimos respuesta, se realizo el pago
              if(respuesta.estado == 100) {
                //Se agrego correctamente
                accept(respuesta);
              } else {
                reject(respuesta);
              }
            }, error => {
              console.log("Ha ocurrido un error intentando agregar la solicitud", error);
              reject(false);
            });
        }, error => {
          //No se encontro usuario
          console.log("No se encontro usuario");
          reject(false);
        });
    });
  }

  addSolicitudCajero(solicitud, direccionActual, metodoPago, total) {
    let me = this;
    let usuario; //Datos del usuario

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    return new Promise((accept,reject) => {
      //Obtenemos el usuario realizando el pago
      me.nativeStorage.getItem('user')
        .then(user => {
          let enviar = "solicitud="+solicitud+"&direccionActual="+direccionActual+"&metodoPago="+metodoPago+"&tipo=6&id_usuario="+user.id+"&total="+total;
          console.log("DATOS ENVIADOS PARA LA NUEVA SOLICITUD DE SERVICIOS: "+enviar);
          //Se encontro usuario, se procede a agregar nuevo pago
          this.http.post(this.url+"cajero/", enviar, options)
            .map(res => res.json())
            .subscribe(respuesta => {
              console.log("Respuesta de SERVIDOR "+JSON.stringify(respuesta));
              //Recibimos respuesta, se realizo el pago
              if(respuesta.estado == 100) {
                //Se agrego correctamente
                accept(respuesta);
              } else {
                reject(respuesta);
              }
            }, error => {
              console.log("Ha ocurrido un error intentando agregar la solicitud", error);
              reject(false);
            });
        }, error => {
          //No se encontro usuario
          console.log("No se encontro usuario");
          reject(false);
        });
    });
  }

  addComentarios(data) {
    
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    console.log('La data de la solicitud a agregar es: ', data);
    return new Promise((resolve, reject) => {
      this.http.post(this.url+'comentarios/', data, options)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  reordenarSolicitud(data) {
    
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let valores = 'nombre='+data.solicitud+'&adjunto=null&id_usuario='+data.id_usuario;
    console.log('La data de la solicitud a agregar es: ', data);
    return new Promise((resolve, reject) => {
      this.http.post(this.url, valores, options)
        .map(res => res.json())
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  getSolicitudId(id:any) {
    return new Promise(resolve => {
      this.http.get(this.url+'/id')
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  getRating(id:any) {
    return new Promise(resolve => {
      this.http.get(this.url+'rating/'+id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  editSolicitud(data){
    // let body = JSON.stringify(item);
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.put(this.url+item.Id, body, options)
    //   .map((response:Response)=>response.json());
  }

}

