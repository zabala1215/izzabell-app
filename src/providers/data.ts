import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
 
@Injectable()
export class Data {
 
    solicitudes: any;
 
    constructor(public http: Http) {
 
        this.http.get('http://www.izzabell.com/api-kingbakery/solicitudes/53').map(res => res.json()).subscribe(data => {
            this.solicitudes = data;
        });
 
    }
 
    filterSolicitudes(searchTerm){
 
        return this.solicitudes.filter((item) => {
            return item.title.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
        });     
 
    }
 
}