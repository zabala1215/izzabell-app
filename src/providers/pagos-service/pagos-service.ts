import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers, RequestOptions } from '@angular/http';

//Modelos
import { MetodoPago } from '../../custom_elems/metodo_pago/metodo_pago';

//Nativo

/*
  Generated class for the PagosServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class PagosServiceProvider {

  // private allTask:Task[]=[];
  data: any;
  private url:string="http://izzabell.com/api-kingbakery/pagos/";
  
  constructor(public http: Http) {
    console.log('Proveedor de datos de solicitud');
  }

  getAllPagos() {
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http.get(this.url)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }
  

  deletePago(data) {  
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({ headers: headers });
    // return this.http.delete(this.url+item.Id, options)
    //   .map((response:Response)=>response.json());   
  }

  addPago(data) {
    console.log(data);
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    
    return new Promise((resolve, reject) => {
      this.http.post(this.url, data, options)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  addTarjeta(data) {
    console.log(data);
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    
    return new Promise((resolve, reject) => {
      this.http.post(this.url+'tarjeta', data, options)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  getPagoId(id:any) {
    // let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    // let options = new RequestOptions({ headers: headers });

    return new Promise(resolve => {
      this.http.get(this.url+id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  //Obtener métodos de pago
  getTarjetasId(id:any): Promise<MetodoPago[]> {
    // let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    // let options = new RequestOptions({ headers: headers });

    let self = this;

    return new Promise<MetodoPago[]>(resolve => {
      this.http.get(this.url+'tarjeta/'+id)
        .map(res => res.json())
        .subscribe(data => {
          //Al obtener los metodos de pago llenar los modelos
          let metodos: MetodoPago[] = []; //Arreglo de metodos
          for(let metodo of data) {
            metodos.push(new MetodoPago(metodo.id,metodo.nombre,metodo.tipo,metodo.ultimos,metodo.user_id,metodo.marca));
          }
          self.data = metodos;
          resolve(metodos);
        });
    });
  }

  editPago(data){
    // let body = JSON.stringify(item);
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    // let options = new RequestOptions({ headers: headers });
    // return this._http.put(this.url+item.Id, body, options)
    //   .map((response:Response)=>response.json());
  }

}
