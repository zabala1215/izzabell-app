import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers, RequestOptions } from '@angular/http';
import { NativeStorage } from "@ionic-native/native-storage";


@Injectable()
export class ServiciosProvider {

  // private allTask:Task[]=[];
  data: any;
  private url:string="http://izzabell.com/api-kingbakery/servicios-pre/";

  constructor(public http: Http, private nativeStorage: NativeStorage) {
    console.log('Proveedor de datos de solicitud');
  }

  getAllServicios() {
    if (this.data) {
      return Promise.resolve(this.data);
    }

    return new Promise(resolve => {
      this.http.get(this.url)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        }, error => {
          console.log("Error obteniendo datos: "+error);
        });
    });
  }

  getServicio(id) {

    return new Promise(resolve => {
      this.http.get(this.url+id)
        .map(res => res.json())
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  deleteServicio(id) {
   
  }

  addServicio(id_servicio, nombre) {
    let me = this;
    let usuario; //Datos del usuario

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });

    return new Promise((accept,reject) => {
      //Obtenemos el usuario realizando el pago
      me.nativeStorage.getItem('user')
        .then(user => {
          let enviar = "id_servicio="+id_servicio+"&id_usuario="+user.id+"&titulo="+nombre+"&tipo=2";
          console.log("DATOS ENVIADOS PARA LA NUEVA SOLICITUD DE SERVICIOS: "+enviar);
          //Se encontro usuario, se procede a agregar nuevo pago
          this.http.post(this.url, enviar, options)
            .map(res => res.json())
            .subscribe(respuesta => {
              console.log("Respuesta de SERVIDOR "+JSON.stringify(respuesta));
              //Recibimos respuesta, se realizo el pago
              if(respuesta.estado == 100) {
                //Se agrego correctamente
                accept(respuesta);
              } else {
                reject(respuesta);
              }
            }, error => {
              console.log("Ha ocurrido un error intentando agregar la solicitud", error);
              reject(false);
            });
        }, error => {
          //No se encontro usuario
          console.log("No se encontro usuario");
          reject(false);
        });
    });
  }

  editSolicitud(data){
   
  }

}
