export class SelectView {

    //Propiedades
    sel_opt: number;
    selToConfirm: number;
    select_state: string;

    constructor() {
        this.sel_opt = 0;
        this.selToConfirm = 0;
        this.select_state = "cerrado";
    }

    //Al clickear en pais
    clickedItem(codigo) {
        //Asignamos la opcion clickeada
        this.selToConfirm = codigo;
    }

    //Abrir select view
    clickSelect() {
        this.select_state = this.select_state == "abierto" ? "cerrado" : "abierto";
    }

    //Al oprimir cancelar en select
    cancelSel() {
        this.selToConfirm = this.sel_opt;
        this.select_state = "cerrado";
    }

    //Al confirmar opcion de pais
    confirmSel() {
        this.sel_opt = this.selToConfirm;
        this.select_state = "cerrado";
    }
}