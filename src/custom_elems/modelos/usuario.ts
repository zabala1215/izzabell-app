//Modelos
import { Ubicacion } from './ubicacion';

//Nativo
import { NativeStorage } from "@ionic-native/native-storage";

export class Usuario {
	//Datos del usuario
	public id: number;
	public nombre: string;
	public telefono: string;
	public email: string;
	public imgPerfil: string;
	public idFacebook: string;
	public idGoogle: string;
	public dirCasa: Ubicacion;
	public dirTrabajo: Ubicacion;
	public dirOtros: Ubicacion[];
	public tipoPlan: number;
	public fechaCreacion: any;
	public fechaActualizacion: any;

	constructor(id, nombre?, telefono?, email?, imgPerfil?, idFacebook?, idGoogle?, dirCasa?, dirTrabajo?, dirOtros?, tipoPlan?, fechaCreacion?, fechaActualizacion?) {
		let self = this; //Evitar errores
		self.id = id;
		self.nombre = nombre;
		self.telefono = telefono;
		self.email = email;
		self.imgPerfil = imgPerfil;
		self.idFacebook = idFacebook;
		self.idGoogle = idGoogle;
		self.dirCasa = dirCasa;
		self.dirTrabajo = dirTrabajo;
		self.dirOtros = dirOtros;
		self.tipoPlan = tipoPlan;
		self.fechaCreacion = fechaCreacion;
		self.fechaActualizacion = fechaActualizacion;
	}
}