//Angular
import { NgZone } from '@angular/core';

import {
 Geocoder,
 GeocoderRequest,
 GeocoderResult,
 BaseArrayClass
} from '@ionic-native/google-maps';

//Modelo de ubicacion
export class Ubicacion {
  public latitud: number = null;
  public longitud: number = null;
  public altitud: number = null;
  public exactitud: number = null;
  public direccion: string = null;
  public placeId: string = null;

  constructor(lat, lon, alt, exact, direccion?, placeId?) {
    this.latitud = lat;
    this.longitud = lon;
    this.altitud = alt;
    this.exactitud = exact;
    this.direccion = direccion;
    this.placeId = placeId;
  }

  //Obtener dirección
  getDireccion() {
    //Asignacion
    let self = this;
    let ubis = []; //Arreglo para obtener las direcciones diferentes
    let request: GeocoderRequest = {
      position: {lat: self.latitud, lng: self.longitud}
    };

    console.log("Antes de Zona");
    new NgZone({enableLongStackTrace: false}).run(function() {
      console.log("ZONA RUN"); 
      //Obtenemos direccion
      new Geocoder().geocode(request).then((resultado: BaseArrayClass<GeocoderResult>) => {
        ubis = []; //Reiniciamos arreglo
        console.log("Direccion Obtenida "+JSON.stringify(resultado));

        /*------------------------ ALGORITMO - OBTENER DIRECCIÓN VERDADERA -----------------------*/
        /* Obtenemos un arreglo con muchas calles y direccione separadas entre si,                */
        /* sin saber cual corresponde a cual.                                                     */
        /*                                                                                        */
        /* -> PROCEDIMIENTO                                                                       */
        /* Recorremos el arreglo y buscamos todo lo que no sea igual y no diga Panamá, lo ponemos */
        /* en orden y luego construimos el string en base al arreglo construido.                  */
        /* Por último agregamos ", Panamá".                                                       */
        /*----------------------------------------------------------------------------------------*/
        console.log("-------> PROCESO DE ALGORITMO <-----");
        var cn = 0;
        resultado.forEach(function(res) {
          console.log("RESULTADO "+cn);
          cn++;
          if(res.countryCode == "PA") {
            console.log("   PA");
            var ln = 0;
            res.extra.lines.forEach(function(linea) {
              console.log("   LINEA "+ln);
              ln++;
              if(!(ubis.indexOf(linea) > -1)) {
                if(linea != "Panamá" || linea !== "Panamá" || linea != "Panamá") {
                  console.log("     PASÓ");
                  ubis.push(linea);
                }
              }
            });
          }
        });
        console.log("UBIS "+JSON.stringify(ubis));
        console.log(" RECORRIENDO UBIS");
        //Asignamos la nueva direccion
        for(var i = 0; i < ubis.length; i++) {
          console.log("   UBI "+i);
          self.direccion = i == 0 ? ubis[i] : self.direccion+", "+ ubis[i]; 
          console.log("     "+self.direccion);
        }
        self.direccion = self.direccion + ", Panamá";
        console.log("Direccion final "+self.direccion);
      }).catch((error) => console.log("Error obteniendo direccion "+error));
    });
  }

}