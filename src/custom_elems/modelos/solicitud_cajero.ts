//Modelos
import { Ubicacion } from './ubicacion';
import { MetodoPago } from './metodo_pago';

export class SolicitudCajero {
	public usuario: number;
	public monto: any;
	public total = {};
	public ubicacion: Ubicacion;
	public metodo_pago: MetodoPago;

	constructor(usuario, monto, ubicacion?, metodo_pago?) {
		this.usuario = usuario;
		this.monto = monto;
		this.ubicacion = ubicacion;
		this.metodo_pago = metodo_pago;
		console.log("CONSTRUYO");
	}
}