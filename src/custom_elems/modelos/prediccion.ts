export class Prediccion {
	public latitud: number;
	public longitud: number;
	public placeId: string;
	public description: string;

	constructor(description, placeId, latitud?, longitud?) {
		this.description = description;
		this.placeId = placeId;
		this.latitud = latitud;
		this.longitud = longitud;
	}
}