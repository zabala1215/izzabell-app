//Modelos
import { Ubicacion } from '../../custom_elems/ubicacion/ubicacion';
import { MetodoPago } from '../../custom_elems/metodo_pago/metodo_pago';

export class SolicitudMensaje {
    public usuario: number = null;
    public texto: string = null;
    public ubicacion: Ubicacion = null;
    public metodo_pago: MetodoPago = null;
    public total = {sub: 0, tot: 0}

    //Establecer todos los valores
    constructor(texto, usuario, ubicacion?, metodo_pago?, sub?, tot?) {
        this.texto = texto;
        this.usuario = usuario;
        this.ubicacion = ubicacion;
        this.metodo_pago = metodo_pago;
        this.total.sub = sub;
        this.total.tot = tot;
    }
}