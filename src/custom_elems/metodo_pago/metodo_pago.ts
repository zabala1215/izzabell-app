export class MetodoPago {

  private id;
  private nombre;
  private tipo;
  private ultimos;
  private user_id;
  private marca;

  constructor(id, nombre, tipo, ultimos, user_id, marca) {
    this.id = id;
    this.nombre = nombre;
    this.tipo = tipo;
    this.ultimos = ultimos;
    this.user_id = user_id;
    this.marca = marca;
  }

}