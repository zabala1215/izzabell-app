//Modelo de ubicacion
export class Ubicacion {
  public latitud: number = null;
  public longitud: number = null;
  public altitud: number = null;
  public exactitud: number = null;
  public direccion: string = null;

  constructor(lat, lon, alt, exact) {
    this.latitud = lat;
    this.longitud = lon;
    this.altitud = alt;
    this.exactitud = exact;
  }

}