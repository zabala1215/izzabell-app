import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ToastController, AlertController, ModalController, ActionSheetController, Platform } from 'ionic-angular';
import { RatingServiceProvider } from '../../providers/rating-service/rating-service';

import { SolicitudserviceProvider } from '../../providers/solicitudservice/solicitudservice';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { NativeStorage } from '@ionic-native/native-storage';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-solicitud',
  templateUrl: 'solicitud.html',
})
export class SolicitudPage {

  solicitud: any;
  loading: Loading;
  rate: any;
  readOnly: boolean;

  //PARA AGREGAR COMENTARIOS
  adjunto: any;
  query: any;
  lastImage: string = null;
  resultado: any;
  cargaComentarios: boolean = true;
  comentarios: any;

  placeholderText = 'Escriba aquí un comentario';

  constructor(public navCtrl: NavController, public navParams: NavParams, public RatingServiceProvider: RatingServiceProvider, public SolicitudService: SolicitudserviceProvider, private loadingCtrl: LoadingController, private toastCtrl: ToastController, private alertCtrl: AlertController, private modalCtrl: ModalController, private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController,  public platform: Platform) {

    this.solicitud = navParams.get('solicitud');
    this.comentarios = [];

    console.log(this.solicitud);

  }

  ionViewWillEnter() {
    this.showLoading();
    this.SolicitudService.getRating(this.solicitud.id)
    .then(
      data => {
        this.rate = data['stars'];
        if (this.rate > 0) {
          this.readOnly = true;
          this.loading.dismiss();
        }else{
          this.readOnly = false;
          this.loading.dismiss();
        }
      },
      error => console.error(error)
    );

    this.SolicitudService.getSolicitudesComentarios(this.solicitud.id)
    .then(data => {
      this.comentarios = data;
      this.cargaComentarios = false;
      console.log(this.comentarios);
    });
  }

  ratingModal (rating) {
    //Abrimos modal de ubicacion
    let modal = this.modalCtrl.create('RatingModalPage', {rating: rating, id : this.solicitud.id});
    modal.onDidDismiss(data => {
      if(data){
        this.rate = data;
        this.readOnly = true;
        this.presentToast('Gracias por calificar esta solicitud');
      }else{
        this.rate = 0;
      }
    });
    modal.present();
  }

  comentariosModal () {
    //Abrimos modal de ubicacion
    let modal = this.modalCtrl.create('SolicitudAdicionalesPage', {solicitud : this.solicitud});
    modal.onDidDismiss(data => {
      if(data){
        this.rate = data;
        this.readOnly = true;
        // this.presentToast('Gracias por calificar esta solicitud');
      }else{
        this.rate = 0;
      }
    });
    modal.present();
  }

  borrarSolicitud() {
    this.SolicitudService.deleteSolicitud(this.solicitud.id)
    .then(data => {
      this.loading.dismiss();
      console.log(data);
      this.navCtrl.pop();
    });
  }

  reordenarSolicitud() {

    let adjunto = null;
    let resultado: any = {};

    this.SolicitudService.addSolicitud('nombre='+this.solicitud.nombre+'&adjunto='+adjunto+'&id_usuario='+this.solicitud.id_usuario).then((result) => {
      resultado = result;
      console.log('resultado ', resultado.rest);

      if(result != null && result != false){
        this.RatingServiceProvider.addRating('rating=0&id='+resultado.rest).then((result2) => {
          console.log(result2);
          this.navCtrl.pop();
        }, (err) => {
          console.log(err);
        });
      }
    }, (err) => {
      console.log(err);
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  onModelChange(event){
    console.log('mostrar el valor: ', event);
    this.ratingModal(event);
    // this.RatingServiceProvider.editRating('rating='+event+'&id='+this.solicitud.id).then((result) => {
    //   this.readOnly = true;
    //   this.presentToast('Gracias por calificar esta solicitud');
    //   console.log(result);
    // }, (err) => {
    //   console.log(err);
    // });
  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      message: '¿Estás seguro que quieres eliminar la solicitud?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancelar presionado');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            console.log('Eliminando solicitud...');
            this.borrarSolicitud();
          }
        }
      ]
    });
    confirm.present();
  }

  showConfirm2() {
    let confirm = this.alertCtrl.create({
      message: '¿Estás seguro que quieres reordenar esta solicitud?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancelar presionado');
          }
        },
        {
          text: 'Confirmar',
          handler: () => {
            console.log('Eliminando solicitud...');
            this.reordenarSolicitud();
          }
        }
      ]
    });
    confirm.present();
  }

  /**
   * AGREGAR COMENTARIOS A LA PAGINA
   */

   addComentario(query){

    console.log(query);
    this.adjunto = null;
    this.adjunto = this.lastImage;

    this.SolicitudService.addComentarios('comentario='+query+'&adjunto='+this.lastImage+'&id_solicitud='+this.solicitud.id).then((result) => {
      this.resultado = result;
      if(this.lastImage){
        console.log('lastimage:', this.lastImage);
        this.comentarios.push({comentario: query, adjunto: this.lastImage});
        this.uploadImage();
      }else{
        this.comentarios.push({comentario: query, adjunto: 'null'});
      }
      // this.content.scrollToBottom();
      this.query = '';
      console.log('resultado ', this.resultado);
    }, (err) => {
      console.log(err);
    });
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Enviar Archivo',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Agregar Documento',
          icon: 'document',
          handler: () => {
            //Adjuntar documento
          }
        },
        {
          text: 'Imagen de la galería',
          icon: 'image',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Tomar foto',
          icon: 'camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
  // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
}

// Create a new name for the image
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}

// Copy the image to a local folder
private copyFileToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.lastImage = newFileName;
  }, error => {
    this.presentToast('Ha ocurrido un error mientras se almacenaba el archivo.');
  });
}

// Always get the accurate path to your apps folder
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + img;
  }
}

public uploadImage() {
  // Destination URL
  var url = "http://www.izzabell.com/subirImg.php";

  // File for Upload
  var targetPath = this.pathForImage(this.lastImage);

  // File name only
  var filename = this.lastImage;

  var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : {'fileName': filename}
  };

  const fileTransfer: TransferObject = this.transfer.create();

  this.loading = this.loadingCtrl.create({
    content: 'Cargando...',
  });
  this.loading.present();

  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath, url, options).then(data => {
    this.loading.dismissAll()
    this.presentToast('Se ha subido la imagen.');
    this.lastImage = null;
  }, err => {
    this.loading.dismissAll()
    this.presentToast('Error al subir la imagen.');
  });
}

}
