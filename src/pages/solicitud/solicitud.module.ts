import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolicitudPage } from './solicitud';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    SolicitudPage,
  ],
  imports: [
    IonicPageModule.forChild(SolicitudPage),
    Ionic2RatingModule
  ],
  exports: [
    SolicitudPage
  ]
})
export class SolicitudPageModule {}
