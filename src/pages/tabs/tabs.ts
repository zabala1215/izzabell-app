import { Component, ElementRef, Renderer, ViewChild } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';

import { Events, Tabs } from 'ionic-angular';

import { InicioPage } from '../inicio/inicio';
import { AjustesPage } from '../ajustes/ajustes';
import { SolicitudesPage } from '../solicitudes/solicitudes';

declare var window;

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  @ViewChild('myTabs') tabRef: Tabs;
  mb: any;

  tab1Root = 'SolicitudesPage';
  tab2Root = 'InicioPage';
  tab3Root = 'AjustesPage';

  valueforngif = true;

  constructor(public navCtrl: NavController, private keyboard: Keyboard, private elementRef: ElementRef, private renderer: Renderer, private event: Events, private platform: Platform) {
    
  }

  ionViewDidLoad() {
    // let tabs = this.queryElement(this.elementRef.nativeElement, '.tabbar');
    // this.event.subscribe('hideTabs', () => {
    //   this.renderer.setElementStyle(tabs, 'display', 'none');
    //   let SelectTab = this.tabRef.getSelected()._elementRef.nativeElement;
    //   let content = this.queryElement(SelectTab, '.scroll-content');
    //   this.mb = content.style['margin-bottom'];
    //   this.renderer.setElementStyle(content, 'margin-bottom', '0')
    // });
    // this.event.subscribe('showTabs', () => {
    //   this.renderer.setElementStyle(tabs, 'display', '');
    //   let SelectTab = this.tabRef.getSelected()._elementRef.nativeElement;
    //   let content = this.queryElement(SelectTab, '.scroll-content');
    //   this.renderer.setElementStyle(content, 'margin-bottom', this.mb)
    // })
  }
  queryElement(elem: HTMLElement, q: string): HTMLElement {
    return <HTMLElement>elem.querySelector(q);
  }

  callIT(passedNumber){
    console.log('Entro a llamar en el celular');
    //You can add some logic here
     window.location = passedNumber;
  }

  sms(){
    //Enviar Mensaje de Whatsapp
    let NUMERO_TEL:string = "+50764272200";
    var mensaje = "Prueba de Whatsapp";
    var ubicacion = "Nose que es esto";
    window.location.href = "whatsapp://send?text="+mensaje+"&phone="+NUMERO_TEL+"&abid="+NUMERO_TEL;
    //window.location.href = "sms:+50755555555?body=" + messagetosend + "    I am at   "  + currentlocation;
  }

  openWp(){
    console.log('Entro a llamar al whatsapp');
    //You can add some logic here
     window.open("'whatsapp://send?text=hello world'", "_system");
  }

  // ionViewDidEnter(){
  //    this.keyboard.onKeyboardShow().subscribe(()=>{this.valueforngif=false})
  //    this.keyboard.onKeyboardHide().subscribe(()=>{this.valueforngif=true})
  // }

}
