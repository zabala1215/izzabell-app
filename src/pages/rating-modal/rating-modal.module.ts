import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RatingModalPage } from './rating-modal';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    RatingModalPage,
  ],
  imports: [
    IonicPageModule.forChild(RatingModalPage),
    Ionic2RatingModule
  ],
  exports: [
    RatingModalPage
  ]
})
export class RatingModalPageModule {}
