import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ViewController} from 'ionic-angular';
import { RatingServiceProvider } from '../../providers/rating-service/rating-service';


@IonicPage()
@Component({
  selector: 'page-rating-modal',
  templateUrl: 'rating-modal.html',
})
export class RatingModalPage {

  rating: any;
  ratingValor: String;
  comentario: any;
  
  //id de la solicitud
  id: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public RatingServiceProvider: RatingServiceProvider, public viewCtrl: ViewController) {
    this.rating = navParams.get('rating');
    this.id = navParams.get('id');
  }

  onModelChange(event){
    console.log(event);
    switch (event) {
      case 1:
        this.ratingValor = "Terrible";
        break;
      case 2:
        this.ratingValor = "Malo";
        break;
      case 3:
        this.ratingValor = "Regular";
        break;
      case 4:
        this.ratingValor = "Bien";
        break;
      case 5:
        this.ratingValor = "Excelente";
        break;
    }
  }

  calificar (){
    this.RatingServiceProvider.editRating('rating='+this.rating+'&comentario='+this.comentario+'&id='+this.id).then((result) => {
      console.log(result);
      this.viewCtrl.dismiss(this.rating);
    }, (err) => {
      console.log(err);
    });
  }

  ionViewDidLoad() {
    switch (this.rating) {
      case 1:
        this.ratingValor = "Terrible";
        break;
      case 2:
        this.ratingValor = "Malo";
        break;
      case 3:
        this.ratingValor = "Regular";
        break;
      case 4:
        this.ratingValor = "Bien";
        break;
      case 5:
        this.ratingValor = "Excelente";
        break;
    }
  }

  dismiss() {
    //No se escogio dirección
    this.viewCtrl.dismiss();
  }

}
