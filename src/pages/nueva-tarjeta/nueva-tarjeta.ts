import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PagosServiceProvider } from '../../providers/pagos-service/pagos-service';
import { CardIO } from '@ionic-native/card-io';

import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { CreditCardValidator } from 'angular-cc-library';


@IonicPage()
@Component({
  selector: 'page-nueva-tarjeta',
  templateUrl: 'nueva-tarjeta.html',
})
export class NuevaTarjetaPage implements OnInit {
  
  form: FormGroup;
  submitted: boolean = false;
  idUsuario: any;
  cc: CreditCardValidator;

  card: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public PagosServiceProvider: PagosServiceProvider, private _fb: FormBuilder, private cardIO: CardIO) {
    this.card = {};
    this.idUsuario = navParams.get('id');
    console.log('El id del usuario es:'+this.idUsuario);
  }

  ngOnInit() {
    this.form = this._fb.group({
      nombre: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      creditCard: ['', [<any>CreditCardValidator.validateCCNumber]],
      expirationDate: ['', [<any>CreditCardValidator.validateExpDate]],
      cvc: ['', [<any>Validators.required, <any>Validators.minLength(3), <any>Validators.maxLength(4)]] 
    });
  }

  onSubmit(form) {
    this.submitted = true;
    console.log(form);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NuevaTarjetaPage');
  }

  escanearTarjeta(){
    console.log('Entro aca');
    this.cardIO.canScan()
    .then(
      (res: boolean) => {
        if(res){
          let options = {
            requireExpiry: true,
            requireCVV: true,
            hideCardIOLogo: true,
            useCardIOLogo: false,
            requireCardholderName: true,
            supressManual: true
          };
          this.cardIO.scan(options).then((data) => {
            console.log('CONSOLEEEE   >>>>>> ',  data)
            this.card.number = data.cardNumber;
            this.card.exp_month = data.expiryMonth + ' / ' +data.expiryYear;
            this.card.cvc = data.cvv;
            this.card.holder_name = data.cardholderName;
          }, err => {
            console.log(err);
            // An error occurred
          });; 
        }
      }
    );
  }

  addTarjeta(card){

    this.PagosServiceProvider.addTarjeta('nombre='+card.holder_name+'&numero='+card.number+'&mes='+card.exp_month+'&ano='+card.exp_year+'&cvv='+card.cvc+'&user_id=54').then((result) => {
      console.log(result);
      this.navCtrl.pop();
    }, (err) => {
      console.log(err);
    });
  }

  save(){
    
    this.submitted = true;

    if(this.form.valid){
        this.PagosServiceProvider.addTarjeta('nombre='+this.form.value.nombre+'&numero='+this.form.value.creditCard+'&exp='+this.form.value.expirationDate+'&cvc='+this.form.value.cvc+'&user_id='+this.idUsuario).then((result) => {
        console.log(result);
        this.navCtrl.pop();
      }, (err) => {
        console.log(err);
      });
    } 
}

}
