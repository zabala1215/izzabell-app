import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NuevaTarjetaPage } from './nueva-tarjeta';
import { CreditCardDirectivesModule } from 'angular-cc-library';

@NgModule({
  declarations: [
    NuevaTarjetaPage,
  ],
  imports: [
    IonicPageModule.forChild(NuevaTarjetaPage),
    CreditCardDirectivesModule
  ],
  exports: [
    NuevaTarjetaPage
  ]
})
export class NuevaTarjetaPageModule {}
