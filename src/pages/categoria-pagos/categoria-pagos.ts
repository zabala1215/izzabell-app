import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { PagoServicioProvider } from '../../providers/pago-servicio/pago-servicio';


@IonicPage()
@Component({
  selector: 'page-categoria-pagos',
  templateUrl: 'categoria-pagos.html',
})
export class CategoriaPagosPage {

  searchQuery: string = '';
  categorias: any;
  itemsCargados: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public PagoServicioProvider: PagoServicioProvider) {
    this.getAllServicios();
  }

  initializeItems() {
    this.categorias = this.itemsCargados;
  }

  getAllServicios() {
  
    this.PagoServicioProvider.getAllServicios()
    .then(data => {
      this.categorias = data;
      this.itemsCargados = data;
      // this.carga = false;
      console.log(data);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriaServiciosPage');
  }

  cargarServicios(categoria) {
    console.log();
    this.navCtrl.push('PagosServicioPage', {
      items: categoria.items
    });
  }

  getItems(ev: any) {
    
    // Reset items back to all of the items
    this.initializeItems();
    

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.categorias = this.categorias.filter((categoria) => {
        return (categoria.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

}
