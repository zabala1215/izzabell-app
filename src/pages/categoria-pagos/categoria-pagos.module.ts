import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriaPagosPage } from './categoria-pagos';

@NgModule({
  declarations: [
    CategoriaPagosPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriaPagosPage)
  ],
  exports: [
    CategoriaPagosPage
  ]
})
export class CategoriaPagosPageModule {}