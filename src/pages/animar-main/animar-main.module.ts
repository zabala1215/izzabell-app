import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimarMainPage } from './animar-main';

@NgModule({
  declarations: [
    AnimarMainPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimarMainPage),
  ],
  exports: [
    AnimarMainPage
  ]
})
export class AnimarMainPageModule {}
