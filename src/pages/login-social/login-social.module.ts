import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginSocialPage } from './login-social';

@NgModule({
  declarations: [
    LoginSocialPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginSocialPage),
  ],
  exports: [
    LoginSocialPage
  ]
})
export class LoginSocialPageModule {}
