import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController } from 'ionic-angular';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { TabsPage } from '../tabs/tabs';
import { NativeStorage } from '@ionic-native/native-storage';


@IonicPage()
@Component({
  selector: 'page-login-social',
  templateUrl: 'login-social.html',
})
export class LoginSocialPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public Facebook: Facebook, public nativeStorage: NativeStorage) {

  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  doFbLogin(){
    // let permissions = new Array<string>();
    // let nav = this.nav;
    // //the permissions your facebook app needs from the user
    // permissions = ["public_profile"];

    let ns = this.nativeStorage;
    let nav = this.navCtrl;
    // this.Facebook.login(permissions)
    // .then(function(response){
    //   let userId = response.authResponse.userID;
    //   let params = new Array<string>();

    //   //Getting name and gender properties
    //   this.Facebook.api("/me?fields=name,gender", params)
    //   .then(function(user) {
    //     user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
    //     //now we have the users info, let's save it in the NativeStorage
    //     this.NativeStorage.setItem('user',
    //     {
    //       name: user.name,
    //       gender: user.gender,
    //       picture: user.picture
    //     })
    //     .then(function(){
    //       this.nav.push(TabsPage);
    //     }, function (error) {
    //       console.log(error);
    //     })
    //   })
    // }, function(error){
    //   console.log(error);
    // });

    this.Facebook.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => {
        let userId = res.authResponse.userID;
        let params = new Array<string>();
        console.log(userId);
        this.Facebook.api("/me?fields=name,email", params)
          .then(function(user) {
            user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
            //now we have the users info, let's save it in the NativeStorage
            console.log(user);
            ns.setItem('user',
            {
              idFacebook: userId,
              nombre: user.name,
              email: user.email,
              picture: user.picture
            })
            .then(function(){
              nav.setRoot(TabsPage);
            }, function (error) {
              console.log(error);
            })
          })
      })
      .catch(e => console.log('Error logging into Facebook', e));

      
    //   let params = new Array<string>();
  }

  //   this.Facebook.api("/me?fields=name,gender", params)
    //   .then(function(user) {
    //     user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
    //     //now we have the users info, let's save it in the NativeStorage
    //     this.NativeStorage.setItem('user',
    //     {
    //       name: user.name,
    //       gender: user.gender,
    //       picture: user.picture
    //     })
    //     .then(function(){
    //       this.nav.push(TabsPage);
    //     }, function (error) {
    //       console.log(error);
    //     })
    //   })

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginSocialPage');
  }

}
