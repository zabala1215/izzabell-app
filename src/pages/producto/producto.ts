import { Component, NgZone, ViewChild, trigger, transition, style, state, animate, group, keyframes } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-producto',
  templateUrl: 'producto.html'
})
export class ProductoPage {

  producto: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.producto = navParams.data.producto;
  }

  mas () {
    this.producto.cantidad++;
  }

  menos (producto) {
    if(this.producto.cantidad > 0) {
      this.producto.cantidad--;
    }  
  }

  agregar () {
    
  } 

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuperPage');
  }

}
