import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading } from 'ionic-angular';
import { trigger,state, style, transition, animate } from '@angular/animations';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
// import { ArrayFilterPipe } from "../../app/pipes/solicitudes-filter.pipe";
import { SolicitudserviceProvider } from '../../providers/solicitudservice/solicitudservice';
import { NativeStorage } from '@ionic-native/native-storage';


@IonicPage()
@Component({
  selector: 'page-solicitudes',
  templateUrl: 'solicitudes.html',
  animations: [
    trigger('itemState', [
      state('in', style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      state('out', style({
        opacity: 0,
        transform: 'translateX(150%)'
      })),
      transition('in => out', animate('200ms ease-in'))
    ])
  ]
})
export class SolicitudesPage {
  
  estado: string= "0";
  loading: Loading;
  solicitudes: any;

  state: any;
  carga: boolean = true;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public SolicitudserviceProvider: SolicitudserviceProvider, private nativeStorage: NativeStorage, private loadingCtrl: LoadingController) {
    // this.getAllSolicitudes('1');
    // this.solicitudes = {};
  }

  getAllSolicitudes(id) {
    
    let me = this;
    this.SolicitudserviceProvider.getSolicitudesUsuario(id)
    .then(data => {
      me.solicitudes = data;
      this.carga = false;
      // this.loading.dismiss();
      console.log(data);
    });
  }

  borrarSolicitud(solicitud) {
    console.log('Borrar la siguiente solicitud:', solicitud);
    this.SolicitudserviceProvider.deleteSolicitud(solicitud.id)
    .then(data => {
      var index = this.solicitudes.indexOf(solicitud);
      solicitud.state = 'out';
      this.solicitudes[index].state = 'out';
     
      this.loading.dismiss();
      console.log(data);
    });
  }

  hack(val) {
    return Array.from(val);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  goToSolicitudPage(event, solicitud){
  	this.navCtrl.push('SolicitudPage', {
      solicitud: solicitud
    });
  }

  ionViewWillEnter() {
    // this.showLoading();
    this.carga = true;
    console.log('Entrando a la vista...');
    return this.nativeStorage.getItem('user')
    .then(
      data => {
        return this.getAllSolicitudes(data.id)
      },
      error => {
        console.error(error)
        this.loading.dismiss();
      }
    );
  }

  ionViewDidLeave(){
    console.log('entroo??/');
    this.solicitudes = {};
    this.state = '';
  }

}
