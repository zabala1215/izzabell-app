import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolicitudesPage } from './solicitudes';
import { PipesModule } from '../../app/pipes/pipes.module';

@NgModule({
  declarations: [
    SolicitudesPage,
  ],
  imports: [
    IonicPageModule.forChild(SolicitudesPage),
    PipesModule
  ],
  exports: [
    SolicitudesPage
  ]
})
export class SolicitudesPageModule {}
