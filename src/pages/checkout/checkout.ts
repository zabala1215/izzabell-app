import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, ModalController, Platform, Loading } from 'ionic-angular';

import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from "@ionic-native/file-path";
import { File } from '@ionic-native/file';
import { SolicitudserviceProvider } from '../../providers/solicitudservice/solicitudservice';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {

  loading: Loading;
  
  solicitud: any;
  direccionSolicitud: any;
  direccionActual: any;
  adjunto: any;

  metodoPago: any;
  distancia: any; //Distancia entre dos puntos

  //tipo de solicitud
  tipo: any;

  total: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingController: LoadingController, public solicitudService: SolicitudserviceProvider, public modalCtrl: ModalController, private transfer: Transfer, private file: File, private filePath: FilePath, public platform: Platform, private loadingCtrl: LoadingController) {

    this.tipo = navParams.data.tipo;
    switch (this.tipo) {
      //PARA LO QUE SEA
      case 5:
        console.log("tipo lo que sea");
        this.solicitud = navParams.data.solicitud;
        this.adjunto = navParams.data.adjunto;
        this.direccionSolicitud = navParams.data.direccion;
        this.direccionActual = navParams.data.direccionActual;

        this.distancia = this.getDistanceFromLatLonInKm(this.direccionActual.latitud, this.direccionActual.longitud, this.direccionSolicitud.latitud, this.direccionSolicitud.longitud);

        console.log("Adjunto: ", this.adjunto);
        console.log("Distancia: ", this.distancia);
        break;
      //PARA SOLICITUD DE CAJERO
      case 6:
        console.log("tipo cajero");
        this.total = navParams.data.total;
        this.solicitud = navParams.data.solicitud;
        this.direccionActual = navParams.data.direccionActual;
        console.log(this.direccionActual);
        break;
    
      default:
        break;
    }
    

  }

  generarSolicitud(metodo) {
    let nav = this.navCtrl;
    let me = this;

    let loading = this.loadingController.create({
      content: "Enviando solicitud"
    });
    loading.present();

    //SI EL METODO DE PAGO ES EFECTIVO
    if(metodo == 0) {
      this.metodoPago = 0;
      me.solicitudService.addSolicitud2(this.solicitud, this.adjunto, this.direccionSolicitud.formatted_address, this.direccionActual, this.metodoPago)
      .then(respuesta => {
        loading.dismiss();
        if(respuesta != false) {
          //Respuesta correcta
          console.log('datos guardados', respuesta);
          if(me.adjunto){
            this.uploadImage();
          }
          me.verSolicitud(respuesta['solicitud']);
        } else {
          //Error
          console.log("Respuesta false");
        
        }
      }, error => {
        loading.dismiss();
        //Ocurrio un error
      
      });
    }else{
      if(metodo == 1) {
        if(this.tipo == 5){
          me.solicitudService.addSolicitud2(this.solicitud, this.adjunto, this.direccionSolicitud.formatted_address, this.direccionActual, this.metodoPago)
          .then(respuesta => {
            loading.dismiss();
            if(respuesta != false) {
              //Respuesta correcta
              console.log('datos guardados', respuesta);
              this.uploadImage();
              me.verSolicitud(respuesta['solicitud']);
            } else {
              //Error
              console.log("Respuesta false");
            
            }
          }, error => {
            loading.dismiss();
            //Ocurrio un error
          
          });
        }else if(this.tipo == 6){
          console.log('cajero');
          me.solicitudService.addSolicitudCajero(this.solicitud, this.direccionActual.direccion, this.metodoPago, this.total)
          .then(respuesta => {
            loading.dismiss();
            if(respuesta != false) {
              //Respuesta correcta
              console.log('datos guardados', respuesta);
              me.verSolicitud(respuesta['solicitud']);
            } else {
              //Error
              console.log("Respuesta false");
            
            }
          }, error => {
            loading.dismiss();
            //Ocurrio un error
          
          });
        }
      }
    }
    //Añadimos la solicitud
    
  }

  seleccionarTarjeta() {
    console.log('Direccion');
    let modal = this.modalCtrl.create('BilleteraPage', {tipoVista: 1});
    modal.onDidDismiss(data => {
      if(data){
        console.log(data);
        this.metodoPago = data.idTarjeta;
        this.generarSolicitud(1);
      }
    });
    modal.present();
  }

  verSolicitud(solicitud) {
    console.log('Ver solicitud');
    this.navCtrl.popToRoot().then(data => {
      let modal = this.modalCtrl.create('SolicitudEnviadaPage', {solicitud: solicitud});
      modal.onDidDismiss(data => {
        console.log('Dissmised');
      });
      modal.present();
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckoutPage');
  }

  // Always get the accurate path to your apps folder
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + img;
  }
}

  public uploadImage() {
  // Destination URL
  var url = "http://www.izzabell.com/subirImg.php";

  // File for Upload
  var targetPath = this.pathForImage(this.adjunto);

  // File name only
  var filename = this.adjunto;

  var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : {'fileName': filename}
  };

  const fileTransfer: TransferObject = this.transfer.create();

  this.loading = this.loadingCtrl.create({
    content: 'Cargando...',
  });
  this.loading.present();

  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath, url, options).then(data => {
    this.loading.dismissAll()
    this.adjunto = null;
    console.log('Se ha subido la imagen correctamente');
  }, err => {
    this.loading.dismissAll();
    console.log('Error al subir la imagen');
  });
}

  /**
   * CALCULAR DISTANCIA 
   */

  getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1); 
    var a = 
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) * 
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ; 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI/180)
  }

}
