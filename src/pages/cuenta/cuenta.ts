import { Component } from '@angular/core';
import { IonicPage, App, Events, NavController, NavParams, ModalController, LoadingController, Loading } from 'ionic-angular';
import { UsuarioServiceProvider } from '../../providers/usuario-service/usuario-service';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { NativeStorage } from '@ionic-native/native-storage';



@IonicPage()
@Component({
  selector: 'page-cuenta',
  templateUrl: 'cuenta.html',
})
export class CuentaPage {

  loading : Loading;
  usuario: any;
  idUsuario: any;
  nombre_mostrar: string = "Usuario";

  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, public UsuarioServiceProvider: UsuarioServiceProvider, private nativeStorage: NativeStorage, private loadingCtrl: LoadingController, private auth: AuthServiceProvider, private app: App, public events: Events) {
    this.usuario = { direcciones_casa: '', direcciones_trabajo: '', direcciones_otro: ''};
  }

  ionViewWillEnter() {
    //Cargamos los datos del usuario localmente
    this.nativeStorage.getItem('user')
      .then(
        data => {
          this.usuario = data;
          if(this.usuario.nombre == null || this.usuario.nombre == "") {
            this.nombre_mostrar = this.usuario.email;
          } else {
            this.nombre_mostrar = this.usuario.nombre;
          }
          console.log("Usuario recuperado: "+JSON.stringify(data));
        },
        error => {
          console.error(error);
          this.loading.dismiss();
        }
      );
  }

  showAddressModal () {
    //Abrimos modal de ubicacion
    let modal = this.modalCtrl.create('AutocompletePage');
    modal.onDidDismiss(data => {
      if(data){
        console.log(data);
        //Al recibir dirección, la guardamos en la web y localmente
        this.showLoading();
        this.UsuarioServiceProvider.editDireccion(data.formatted_address, data.latitud, data.longitud, this.usuario.id, "casa").then((result) => {
          console.log("Se guardo la dirección de casa correctamente");
          this.usuario.direccion_casa = data.formatted_address;
          this.loading.dismissAll();
          console.log("--------------------Recuperacion de datos C-------------------------");
          this.nativeStorage.getItem('user')
            .then(
              data => console.log(data),
              error => console.error(error)
            );
        }, (err) => {
          console.log(err);
        });
      }
    });
    modal.present();
  }

  showAddressModal2 () {
    //Abrimos modal de ubicacion
    let modal = this.modalCtrl.create('AutocompletePage');
    modal.onDidDismiss(data => {
      if(data){
        //Al recibir dirección, la guardamos en la web y localmente
        this.showLoading();
        this.UsuarioServiceProvider.editDireccion(data.formatted_address, data.latitud, data.longitud, this.usuario.id, "trabajo").then((result) => {
          console.log("Se guardo la dirección de trabajo correctamente");
          this.usuario.direccion_trabajo = data.formatted_address;
          this.loading.dismissAll();
          console.log("--------------------Recuperacion de datos T-------------------------");
          this.nativeStorage.getItem('user')
            .then(
              data => console.log(data),
              error => console.error(error)
            );
        }, (err) => {
          console.log(err);
        });
      }
    });
    modal.present();
  }

  showAddressModal3 () {
    //Abrimos modal de ubicacion
    let modal = this.modalCtrl.create('AutocompletePage');
    modal.onDidDismiss(data => {
      if(data){
        //Al recibir dirección, la guardamos en la web y localmente
        this.showLoading();
        this.UsuarioServiceProvider.editDireccion(data.formatted_address, data.latitud, data.longitud, this.usuario.id, "otro").then((result) => {
          console.log("Se guardo la dirección otro correctamente");
          this.usuario.direccion_otro = data.formatted_address;
          this.loading.dismissAll();
          console.log("--------------------Recuperacion de datos O-------------------------");
          this.nativeStorage.getItem('user')
            .then(
              data => console.log(data),
              error => console.error(error)
            );
        }, (err) => {
          console.log(err);
        });
      }
    });
    modal.present();
  }

  public logout() {
    console.log('Cerrando sesion...');
    //DESTRUIR LOS DATOS DEL USUARIO EN NATIVE STORAGE
    this.nativeStorage.remove('user');

    //MANDAR EVENTO AL COMPONENTE PARA QUE SE ENCARGUE DE ESTABLECER EL ROOT PRINCIPAL
    this.events.publish('user:logout');
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CuentaPage');
  }

  goToEditarCuentaPage(){
  	this.navCtrl.push( 'EditarCuentaPage', {usuario: this.usuario} );
  }

}
