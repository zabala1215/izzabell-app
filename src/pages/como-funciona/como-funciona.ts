import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-como-funciona',
  templateUrl: 'como-funciona.html',
})
export class ComoFuncionaPage {

  abrir: any = null; //Variable para controlar que view se muestra

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  abrirList(codigo) {
    //Si esta abierto, cerramos. Si no, asignamos id y abrimos
    if(this.abrir == codigo) {
      //Cerrar
      this.abrir = null;
    } else {
      //Abrir
      this.abrir = codigo;
    }
  }

}
