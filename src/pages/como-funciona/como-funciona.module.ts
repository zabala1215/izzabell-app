import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComoFuncionaPage } from './como-funciona';

@NgModule({
  declarations: [
    ComoFuncionaPage,
  ],
  imports: [
    IonicPageModule.forChild(ComoFuncionaPage),
  ],
  exports: [
    ComoFuncionaPage
  ]
})
export class ComoFuncionaPageModule {}
