import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CajeroPage } from './cajero';


@NgModule({
  declarations: [
    CajeroPage,
  ],
  imports: [
    IonicPageModule.forChild(CajeroPage)
  ],
  exports: [
    CajeroPage
  ]
})
export class CajeroPageModule {}
