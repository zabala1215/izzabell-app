import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

//Nativo
import { Geolocation } from '@ionic-native/geolocation';

//Providers
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

//Modelos
import { SolicitudCajero } from '../../custom_elems/modelos/solicitud_cajero';
import { Usuario } from '../../custom_elems/modelos/usuario';
import { Ubicacion } from '../../custom_elems/modelos/ubicacion';

@IonicPage()
@Component({
  selector: 'page-cajero',
  templateUrl: 'cajero.html'
})
export class CajeroPage {

  usuario: Usuario; //Datos del usuario
  solicitud: SolicitudCajero;; //Datos de la solicitud
  ubicacion: Ubicacion; // Ubicacion

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthServiceProvider, private geolocation: Geolocation) {
    let self = this;
    //Obtenemos los datos del usuario
    self.auth.usuarioLogueado().then((usuario) => {
      console.log("UBICACION LIST -> Obtuvimos usuario "+JSON.stringify(usuario));
      self.usuario = usuario;
    });

    //Obtenemos la posicion y asignamos
    geolocation.watchPosition({ enableHighAccuracy: true }).subscribe((respuesta) => {
      self.ubicacion = new Ubicacion(respuesta.coords.latitude, respuesta.coords.longitude, respuesta.coords.altitude, respuesta.coords.accuracy);
    }, error => console.log("Error obteniendo su ubicación"));
  }

  //Click Flecha
  clickBack() {
    this.navCtrl.pop({direction: 'back'});
  }

  //Click en algun monto
  clickMonto(monto) {
    let self = this;

    //Verificamos si es numerico u otro
    if(monto == "otro") {

    } else {
      //Creamos la solicitud
      //self.solicitud = new SolicitudCajero(self.usuario.id, monto, self.ubicacion);
      self.solicitud = new SolicitudCajero(65, monto, self.ubicacion);
      console.log("ENVIO "+JSON.stringify(self.solicitud));
      self.navCtrl.push("UbicacionPage", { tipo: "cajero", solicitud: self.solicitud });
    }
  }

}
