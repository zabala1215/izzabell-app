import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import {IonicPage, LoadingController, Loading, AlertController, Alert} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-afiliate-form',
  templateUrl: 'afiliate-form.html',
})
export class AfiliateFormPage {

  //variables
  afiliado: any; //Arreglo de datos a enviar
  form_afiliate: FormGroup; //Formulario para validacion
  formError: boolean = false;
  correoValido: boolean = true;
  loadingDialog: Loading; //Dialogo de Carga
  responseDialog: Alert; //Alerta al recibir respuesta de servidor


  constructor(public http: Http, public formBuilder: FormBuilder, public loadingCtrl: LoadingController, public alertCtrl: AlertController) {

    this.afiliado = [];  //Inicializamos el arreglo para que no de error

    //FormGroup para validar los datos
    this.form_afiliate = formBuilder.group({
      nombre: ['', Validators.compose([Validators.minLength(1), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      apellido: ['', Validators.compose([Validators.minLength(1), Validators.pattern('[a-zA-Z]*'), Validators.required])],
      profesion: ['', Validators.compose([Validators.minLength(1), Validators.required])],
      empresa: ['', Validators.compose([Validators.minLength(1), Validators.required])],
      correo: ['', Validators.compose([Validators.minLength(1), Validators.email, Validators.required])],
      telefono: ['', Validators.compose([Validators.minLength(1), Validators.pattern('[0-9+-/. ]*'), Validators.required])],
      celular: ['', Validators.compose([Validators.minLength(1), Validators.pattern('[0-9+-/. ]*'), Validators.required])]
    });

    //Creamos dialogo de cargando
    this.loadingDialog = this.loadingCtrl.create({
      content: "Por favor espere. Enviando datos..."
    });

  }

  //Método para enviar datos al servidor
  enviarDatos() {

    //Verificamos si el formulario es valido
    if(!this.form_afiliate.valid) {
      this.formError = true;
    } else {
      //No hay error en formulario

      //Creamos header
      var cabecera = new Headers({ 'Content-Type' : 'application/x-www-form-urlencoded' });
      let opciones = new RequestOptions({ headers : cabecera}); //Asignamos la cabecera

      //Datos a enviar
      let parametros = "nombre="+this.afiliado.nombre+"&apellido="+this.afiliado.apellido+"&profesion="+this.afiliado.profesion+"&empresa="+this.afiliado.empresa+"&correo="+this.afiliado.correo+"&telefono="+this.afiliado.telefono+"&celular="+this.afiliado.celular;

      //Creamos dialogo de respuesta


      //Mostramos loading antes de realizar proceso
      this.loadingDialog.present();
      this.http.post("http://www.izzabell.com/api-kingbakery/afiliados", parametros, opciones)
        .subscribe(data => {
          //Quitamos loading y mostramos alerta con respuesta
          this.loadingDialog.dismiss();
          this.responseDialog = this.alertCtrl.create({
            title: '¡Gracias por afiliarte!',
            subTitle: "Proximamente nos pondremos en contacto contigo.",
            buttons: ['Aceptar']
          });
          this.responseDialog.present();
        }, error => {
          //Quitamos loading y mostramos mensaje de error
          this.loadingDialog.dismiss();
          this.responseDialog = this.alertCtrl.create({
            title: '¡Lo sentimos!',
            subTitle: "Ha ocurrido un error inesperado. Vuelve a intentarlo, o contactanos para asistirte.",
            buttons: ['Aceptar']
          });
          this.responseDialog.present();
        });

    }
  }

}
