import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AfiliateFormPage } from './afiliate-form';

@NgModule({
  declarations: [
    AfiliateFormPage,
  ],
  imports: [
    IonicPageModule.forChild(AfiliateFormPage),
  ],
  exports: [
    AfiliateFormPage
  ]
})
export class AfiliateFormPageModule {}
