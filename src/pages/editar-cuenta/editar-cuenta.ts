import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Loading, ToastController, Platform, ActionSheetController } from 'ionic-angular';
import { EditarValorPage } from '../editar-valor/editar-valor';
import { UsuarioServiceProvider } from '../../providers/usuario-service/usuario-service';

import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-editar-cuenta',
  templateUrl: 'editar-cuenta.html',
})
export class EditarCuentaPage {

  loading: Loading;
  lastImage: string = null;
  usuario;

  constructor(public navCtrl: NavController, public navParams: NavParams, private loadingCtrl: LoadingController, public UsuarioServiceProvider: UsuarioServiceProvider, private toastCtrl: ToastController, private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public platform: Platform, public actionSheetCtrl: ActionSheetController) {
    this.usuario = navParams.get('usuario');
  }

  ionViewDidLoad() {
    console.log(this.usuario);
  }

  goToEditarValorPage(pos, valor){
  	this.navCtrl.push( EditarValorPage, {pos: pos, valor: valor, usuario: this.usuario} );
  }

  guardarValores(){
    if(this.lastImage){
      this.usuario.picture = 'http://www.izzabell.com/fotos/'+this.lastImage;
    }
    this.UsuarioServiceProvider.editUsuario(this.usuario).then((result) => {
      console.log(result);
      // this.navCtrl.pop();
      this.uploadImage();
      this.presentToast();
    }, (err) => {
      console.log(err);
    });

    // console.log(this.usuario);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Datos del usuario guardados',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  //SUBIR UNA NUEVA FOTO DE PERFIL

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Selecciona una foto',
      buttons: [
        {
          text: 'Desde la galería',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Usar cámara',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Eliminar Foto',
          role: 'destructive',
          handler: () => {
            this.borrarImg();
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      allowEdit: true,
      targetHeight: 500,
      targetWidth: 500
    };
  
    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToastImage('Error al seleccionar la imagen.');
    });
  }

  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
    n = d.getTime(),
    newFileName =  n + ".jpg";
    return newFileName;
  }
  
  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
      console.log(this.lastImage);
      this.uploadImage();
    }, error => {
      this.presentToastImage('Error al almacenar la imagen.');
    });
  }

  private presentToastImage(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
  
  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }

  borrarImg() {
    this.loading = this.loadingCtrl.create({
      content: 'Cargando...',
    });
    this.loading.present();

    this.UsuarioServiceProvider.editColUsuario("http://www.izzabell.com/fotos/user.svg", 'columna=picture&item=http://www.izzabell.com/fotos/user.svg&usuario='+this.usuario.id).then((result) => {
        console.log(result);
        // this.navCtrl.pop();
        this.loading.dismissAll()
        this.presentToastImage('Imagen cambiada correctamente');
      }, (err) => {
        console.log(err);
      });
  }

  public uploadImage() {
    // Destination URL
    var url = "http://www.izzabell.com/subirImg.php";
  
    // File for Upload
    var targetPath = this.pathForImage(this.lastImage);
  
    // File name only
    var filename = this.lastImage;
  
    var options = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': filename}
    };
  
    const fileTransfer: TransferObject = this.transfer.create();
  
    this.loading = this.loadingCtrl.create({
      content: 'Cargando...',
    });
    this.loading.present();
  
    // Use the FileTransfer to upload the image
    fileTransfer.upload(targetPath, url, options).then(data => {
      console.log(data);
      let newImage = 'http://www.izzabell.com/fotos/'+this.lastImage;
      this.UsuarioServiceProvider.editColUsuario(newImage, 'columna=picture&item='+newImage+'&usuario='+this.usuario.id).then((result) => {
        console.log(result);
        // this.navCtrl.pop();
        this.loading.dismissAll()
        this.presentToastImage('Imagen cambiada correctamente');
      }, (err) => {
        console.log(err);
      });
    }, err => {
      this.loading.dismissAll()
    });
  }
}
