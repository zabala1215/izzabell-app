import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriaServiciosPage } from './categoria-servicios';

@NgModule({
  declarations: [
    CategoriaServiciosPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriaServiciosPage)
  ],
  exports: [
    CategoriaServiciosPage
  ]
})
export class CategoriaServiciosPageModule {}