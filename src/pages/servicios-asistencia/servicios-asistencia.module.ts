import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiciosAsistenciaPage } from './servicios-asistencia';

@NgModule({
  declarations: [
    ServiciosAsistenciaPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiciosAsistenciaPage)
  ],
  exports: [
    ServiciosAsistenciaPage
  ]
})
export class ServiciosAsistenciaPageModule {}