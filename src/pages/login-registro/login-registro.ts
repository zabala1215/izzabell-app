import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TabsPage } from '../tabs/tabs';
import { SelectView } from '../../custom_elems/select_view/select_view';
import { Http, Headers, RequestOptions } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-login-registro',
  templateUrl: 'login-registro.html',
})
export class LoginRegistroPage {
  @ViewChild('input') input;

  //Datos de login
  credenciales: any = {telefono: null, email: null, nombre: null, picture: null, idGoogle: null, idFacebook: null};
  num_tel = "";

  //Datos recibidos
  tipo: string;
  datos: any;

  loading: any;
  alert: any;
  selV: SelectView = new SelectView();

  paises: any; //Arreglo de paises
  public select_state: string = "cerrado";

  constructor(public http: Http, public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private loadingCtrl: LoadingController, public alertCtrl: AlertController) {
    this.tipo = navParams.get('tipo');
    this.credenciales = navParams.get('usuario');
    console.log("Tipo "+this.tipo);
    console.log("Datos "+JSON.stringify(this.datos));

    //Creamos vista de Loading
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });

    //Llenamos el arreglo de paises
    this.paises = [
      {nombre : 'panama', codigo : '+507'},
      {nombre : 'venezuela', codigo : '+58'}
    ];

  }

  closeModal() {
    this.navCtrl.pop({direction: 'back'});
  }

  //Registrar
  submitRegistro() {
    let nav = this.navCtrl; //Evitamos errores
    let me = this;

    console.log("Empezamos registro...");
    this.loading.present(); //Mostramos Loading

    //Si es de tipo telefono verificamos y registramos
    if(this.tipo == "tel") {
      this.credenciales.telefono = this.paises[this.selV.sel_opt].codigo+this.num_tel; //Asignamos codigo de pais y numero

      let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
      let options = new RequestOptions({ headers: headers });
      //Enviamos peticion SMS
      this.http.post("https://api.nexmo.com/verify/json?api_key=d5826d26&api_secret=863df48605f90546&number=507"+this.num_tel+"&brand=IzzabellApp", null,options)
      .map(res => res.json())  
      .subscribe(res => {
        me.loading.dismiss(); //Cerramos view
        console.log("RES: "+JSON.stringify(res));
        //Verificamos el estado del SMS
        if(res.status == 10 || res.status == 0) {
          console.log("Status 0 o 10");
          nav.push('VerificarNumeroPage', {
            request_id: res.request_id,
            siguiente: 'tel',
            usuario: me.credenciales,
            sms_status: res.status
          });
        }
      }, error => {
        console.log("ERROR SMS: "+error);
      });
    } else {
      //Procedemos a registrar
      this.auth.registro(this.credenciales).subscribe(respuesta => {
        this.loading.dismiss(); //Cerramos view
        console.log("Respuesta: "+JSON.stringify(respuesta));

        if(respuesta == true) {
          //Registro correcto
          console.log("Registro correcto");
          nav.setRoot(TabsPage);
        } else if(respuesta == false) {
          //Error durante registro
          console.log("Error durante registro");
          this.errorView("Error", "Ha ocurrido un error inesperado durante el registro");
        } else {
          //Otro error
          this.errorView("Error",respuesta);
        }
      });
    }
  }

  errorView(tit: string, sub: string) {
    this.alert = this.alertCtrl.create({
      title: tit,
      subTitle: sub,
      buttons: ['Aceptar']
    });
    this.alert.present();
  }

}
