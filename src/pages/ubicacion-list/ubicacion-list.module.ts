import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UbicacionListPage } from './ubicacion-list';

@NgModule({
  declarations: [
    UbicacionListPage,
  ],
  imports: [
    IonicPageModule.forChild(UbicacionListPage),
  ],
  exports: [
    UbicacionListPage
  ]
})
export class UbicacionListPageModule {}
