import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//Nativo
import { StatusBar } from '@ionic-native/status-bar';
import { NativeStorage } from '@ionic-native/native-storage';

//Modelos
import { Ubicacion } from '../../custom_elems/modelos/ubicacion';
import { SolicitudCajero } from '../../custom_elems/modelos/solicitud_cajero';
import { SolicitudMensaje } from '../../custom_elems/modelos/solicitud_mensaje';

//Providers
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';


@IonicPage()
@Component({
  selector: 'page-ubicacion-list',
  templateUrl: 'ubicacion-list.html',
})
export class UbicacionListPage {

  ubicaciones = []; //Lista de ubicaciones

  //Recibimos de vista
  solicitud: SolicitudMensaje | SolicitudCajero; //Datos de solicitud
  tipo: string; //Tipo

  ubicacionSeleccionada: Ubicacion = null; //Ubicacion seleccionada

  constructor(private navCtrl: NavController, private navParams: NavParams, private statusbar: StatusBar, private auth: AuthServiceProvider) {
    //Arreglamos estilos
    statusbar.overlaysWebView(true);
    statusbar.styleBlackTranslucent();

    //Datos recibidos de vista
    this.tipo = this.navParams.get("tipo");
    this.solicitud = this.navParams.get("solicitud");
  }

  //Vista cargo
  ionViewDidLoad() {
    console.log("DID LOAD!!!");
    this.getUbicaciones();
  }

  //Click Flecha
  clickBack() {
    this.navCtrl.pop({direction: 'back'});
  }

  //Click en Ubicacion
  clickUbicacion(ubicacion: Ubicacion) {
    this.ubicacionSeleccionada = ubicacion;
    this.solicitud.ubicacion = ubicacion
  }

  //Click en AQUÍ ESTOY
  setUbicacion() {
    let self = this;
    console.log("UBICACION LISTA -> Procedemos a enviar a metodos "+JSON.stringify(self.solicitud));
    //self.navCtrl.push("MensajeriaPagoPage", { solicitud: self.solicitud });
  }

  //Obtenemos ubicaciones
  getUbicaciones() {
    let self = this;
    //Obtenemos Usuario
    self.auth.usuarioLogueado().then((usuario) => {
      console.log("UBICACION LIST -> Obtuvimos usuario "+JSON.stringify(usuario));
      //Asignamos a ubicaciones
      self.ubicaciones.push({tipo: 'Casa', ubicacion: usuario.dirCasa});
      self.ubicaciones.push({tipo: 'Trabajo', ubicacion: usuario.dirTrabajo});
      for(let otro of usuario.dirOtros) {
        self.ubicaciones.push({tipo: 'Otro', ubicacion: otro});
      }
    });
  }
}
