import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';

@IonicPage()
@Component({
  selector: 'page-paypal',
  templateUrl: 'paypal.html',
})
export class PaypalPage {

  payment: PayPalPayment = new PayPalPayment('0.89', 'USD', 'TV', 'Pago membresía');
	currencies = ['EUR', 'USD'];
	payPalEnvironment: string = 'AdHuCee_tKpQ-hcjIk20nRNRSxX_ge-xfq2qU6FMdsLNMgRKYms50-aeW2_KUeXqoDcCchRmaxt53Xsq';
  idUsuario: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private payPal: PayPal) {
    this.idUsuario = navParams.get('id');
    this.payPal.renderFuturePaymentUI().then(() => {
      console.log('Entro?');
    });
  }

  makePayment() {

		this.payPal.init({
      PayPalEnvironmentProduction: '',
      PayPalEnvironmentSandbox: 'AdHuCee_tKpQ-hcjIk20nRNRSxX_ge-xfq2qU6FMdsLNMgRKYms50-aeW2_KUeXqoDcCchRmaxt53Xsq'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        acceptCreditCards: false
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        let payment = new PayPalPayment('3.33', 'USD', 'Description', 'sale');
        this.payPal.renderFuturePaymentUI().then(() => {
          // Successfully paid
          console.log('Pago exitoso');
          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }
        }, () => {
          // Error or render dialog closed without being successful
        });
      }, () => {
        // Error in configuration
      });
    }, () => {
      // Error in initialization, maybe PayPal isn't supported or something else
    });
	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaypalPage');
  }

}
