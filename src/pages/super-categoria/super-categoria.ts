import { Component, NgZone, ViewChild, trigger, transition, style, state, animate, group, keyframes } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-super-categoria',
  templateUrl: 'super-categoria.html',
  animations: [
    trigger('fade', [
      state('visible', style({
        opacity: 1
      })),
      state('invisible', style({
        opacity: 0,
        display: 'none'
      })),
      transition('invisible <=> visible', animate('200ms linear'))
    ]),
  ]
})
export class SuperCategoriaPage {

  fadeState: String = 'invisible';
  productos: Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.productos = [
      {
        id: 1,
        descripcion: 'Airwick Scented Oils 1 Warmer + Refills',
        imagen: 'assets/images/productos/airwick.png',
        precio: '16.38',
        precioDescuento: '15.00',
        cantidad: 0,
        add: 'invisible'
      },
      {
        id: 2,
        descripcion: 'Airwick Scented Oils 1 Warmer + Refills',
        imagen: 'assets/images/productos/airwick.png',
        precio: '16.38',
        precioDescuento: '15.00',
        cantidad: 0,
        add: 'invisible'
      },
      {
        id: 3,
        descripcion: 'Airwick Scented Oils 1 Warmer + Refills',
        imagen: 'assets/images/productos/airwick.png',
        precio: '16.38',
        precioDescuento: '15.00',
        cantidad: 0,
        add: 'invisible'
      },
      {
        id: 4,
        descripcion: 'Airwick Scented Oils 1 Warmer + Refills',
        imagen: 'assets/images/productos/airwick.png',
        precio: '16.38',
        precioDescuento: '15.00',
        cantidad: 0,
        add: 'invisible'
      },
      {
        id: 5,
        descripcion: 'Airwick Scented Oils 1 Warmer + Refills',
        imagen: 'assets/images/productos/airwick.png',
        precio: '16.38',
        precioDescuento: '15.00',
        cantidad: 0,
        add: 'invisible'
      },
      {
        id: 6,
        descripcion: 'Airwick Scented Oils 1 Warmer + Refills',
        imagen: 'assets/images/productos/airwick.png',
        precio: '16.38',
        precioDescuento: '15.00',
        cantidad: 0,
        add: 'invisible'
      }
    ]
    
  }

  toggleFade() {
    this.fadeState = (this.fadeState == 'visible') ? 'invisible' : 'visible';    
  }

  add (producto){
    var index = this.productos.indexOf(producto);
    this.productos[index].add = 'visible';
    this.productos[index].cantidad++;
    // this.toggleFade();
  }

  mas (producto) {
    var index = this.productos.indexOf(producto);
    this.productos[index].cantidad++;
  }

  menos (producto) {
    var index = this.productos.indexOf(producto);
    if(this.productos[index].cantidad > 0) {
      this.productos[index].cantidad--;
    }  
  }

  cargarProducto (producto) {
    this.navCtrl.push('ProductoPage', {producto: producto});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuperPage');
  }

}
