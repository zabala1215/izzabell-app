import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuperCategoriaPage } from './super-categoria';

@NgModule({
  declarations: [
    SuperCategoriaPage,
  ],
  imports: [
    IonicPageModule.forChild(SuperCategoriaPage)
  ],
  exports: [
    SuperCategoriaPage
  ]
})
export class SuperCategoriaPageModule {}
