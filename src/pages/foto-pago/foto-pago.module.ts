import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FotoPagoPage } from './foto-pago';

@NgModule({
  declarations: [
    FotoPagoPage,
  ],
  imports: [
    IonicPageModule.forChild(FotoPagoPage),
  ],
  exports: [
    FotoPagoPage
  ]
})
export class FotoPagoPageModule {}
