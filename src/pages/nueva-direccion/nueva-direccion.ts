import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import {AutocompletePage} from '../autocomplete/autocomplete';

/**
 * Generated class for the NuevaDireccionPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-nueva-direccion',
  templateUrl: 'nueva-direccion.html',
})
export class NuevaDireccionPage {
  address;
 
  constructor(private navCtrl: NavController, private modalCtrl: ModalController) {
    this.address = {
      place: ''
    };
  }
  
  showAddressModal () {
    let modal = this.modalCtrl.create(AutocompletePage);
    let me = this;
    modal.onDidDismiss(data => {
      this.address.place = data;
    });
    modal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NuevaDireccionPage');
  }

}
