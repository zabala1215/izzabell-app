import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AyudaTerminosPage } from './ayuda-terminos';

@NgModule({
  declarations: [
    AyudaTerminosPage,
  ],
  imports: [
    IonicPageModule.forChild(AyudaTerminosPage),
  ],
  exports: [
    AyudaTerminosPage
  ]
})
export class AyudaTerminosPageModule {}
