import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CategoriaAsistenciaPage } from './categoria-asistencia';

@NgModule({
  declarations: [
    CategoriaAsistenciaPage,
  ],
  imports: [
    IonicPageModule.forChild(CategoriaAsistenciaPage)
  ],
  exports: [
    CategoriaAsistenciaPage
  ]
})
export class CategoriaAsistenciaPageModule {}