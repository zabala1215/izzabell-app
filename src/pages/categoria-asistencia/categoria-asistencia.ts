import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { EnLaCalleProvider } from '../../providers/en-la-calle/en-la-calle';


@IonicPage()
@Component({
  selector: 'page-categoria-asistencia',
  templateUrl: 'categoria-asistencia.html',
})
export class CategoriaAsistenciaPage {

  searchQuery: string = '';
  categorias: any;
  itemsCargados: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public ServiciosProvider: EnLaCalleProvider) {
    this.getAllServicios();
  }

  initializeItems() {
    this.categorias = this.itemsCargados;
  }

  getAllServicios() {
  
    this.ServiciosProvider.getAllServicios()
    .then(data => {
      this.categorias = data;
      this.itemsCargados = data;
      // this.carga = false;
      console.log(data);
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoriaServiciosPage');
  }

  cargarServicios(categoria) {
    console.log();
    this.navCtrl.push('ServiciosAsistenciaPage', {
      items: categoria.items
    });
  }

  getItems(ev: any) {
    
    // Reset items back to all of the items
    this.initializeItems();
    

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.categorias = this.categorias.filter((categoria) => {
        return (categoria.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

}
