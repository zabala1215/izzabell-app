import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, PopoverController } from 'ionic-angular';
import { trigger,state, style, transition, animate } from '@angular/animations';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
// import { ArrayFilterPipe } from "../../app/pipes/solicitudes-filter.pipe";
import { SolicitudserviceProvider } from '../../providers/solicitudservice/solicitudservice';
import { ServiciosProvider } from '../../providers/servicios/servicios';
import { PagoServicioProvider } from '../../providers/pago-servicio/pago-servicio';

import { NativeStorage } from '@ionic-native/native-storage';

@IonicPage()
@Component({
  selector: 'page-solicitudes',
  templateUrl: 'solicitudes.html',
  animations: [
    trigger('itemState', [
      state('in', style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      state('out', style({
        opacity: 0,
        transform: 'translateX(150%)'
      })),
      transition('in => out', animate('200ms ease-in'))
    ])
  ]
})
export class SolicitudesPage {
  
  estado: string= "1";
  loading: Loading;
  
  //DECLARAMOS LAS SOLICITUDES
  solicitudes: any;
  pagos: any;
  servicios: any;

  //ESTADO PARA LA ANIMACIÓN DE BORRAR
  state: any;
  //ESTADO PARA LA CARGA DE LAS SOLICITUDES
  carga: boolean = true;

  estado_sol = '0';

  //usuario
  usuario: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public SolicitudserviceProvider: SolicitudserviceProvider, public serviciosProvider: ServiciosProvider, public pagoServicioProvider: PagoServicioProvider, private nativeStorage: NativeStorage, private loadingCtrl: LoadingController, public popoverCtrl: PopoverController) {
    
    console.log('Cargando datos del usuario...');
    nativeStorage.getItem('user')
    .then(
      data => {
        this.usuario = data;
      },
      error => {
        console.error(error)
        this.loading.dismiss();
      }
    );

    // this.usuario = {
    //   id: 67
    // }

  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create('PopoverSolicitudesPage', {
      estado_sol : this.estado_sol
    });
    popover.onDidDismiss(data => {
      if(data){
        console.log(data);
        this.estado_sol = data;
      }
    });
    popover.present({
      ev: myEvent
    });
  }

  getAllSolicitudes(id) {
    
    let me = this;
    // this.SolicitudserviceProvider.getSolicitudesUsuario(id)
    // .then(data => {
    //   me.solicitudes = data;
    //   this.carga = false;
    //   // this.loading.dismiss();
    //   console.log(data);
    // });
    // this.serviciosProvider.getServiciosUsuario(id)
    // .then(data => {
    //   me.solicitudes = data;
    //   this.carga = false;
    //   // this.loading.dismiss();
    //   console.log(data);
    // });
    // this.pagoServicioProvider.getPagosUsuario(id)
    // .then(data => {
    //   me.solicitudes = data;
    //   this.carga = false;
    //   // this.loading.dismiss();
    //   console.log(data);
    // });
  }

  cargarSolicitudes() {
    
    console.log('Cargando solicitudes');
    let me = this;
    this.SolicitudserviceProvider.getSolicitudesUsuario(this.usuario.id)
    .then(data => {
      me.solicitudes = data;
      this.carga = false;
      console.log(data);
    });
  }

  borrarSolicitud(solicitud) {
    console.log('Borrar la siguiente solicitud:', solicitud);
    this.SolicitudserviceProvider.deleteSolicitud(solicitud.id)
    .then(data => {
      var index = this.solicitudes.indexOf(solicitud);
      solicitud.state = 'out';
      this.solicitudes[index].state = 'out';
     
      // this.loading.dismiss();
      console.log(data);
    });
  }

  hack(val) {
    return Array.from(val);
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  goToSolicitudPage(event, solicitud){
  	this.navCtrl.push('SolicitudPage', {
      solicitud: solicitud
    });
  }

  ionViewWillEnter() {
    // switch (this.estado) {
    //   case '0':
    //     this.cargarServicios();
    //     break;
    //   case '1':
    //     this.cargarSolicitudes();
    //     break;
    //   case '2':
    //     this.cargarPagos();
    //     break;
    
    //   default:
    //     this.cargarSolicitudes();
    //     break;
    // }

    this.cargarSolicitudes();
  }

}
