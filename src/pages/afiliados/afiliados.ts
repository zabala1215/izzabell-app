import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-afiliados',
  templateUrl: 'afiliados.html',
})
export class AfiliadosPage {

  pag_Afiliate_Form = 'AfiliateFormPage';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AfiliadosPage');
  }

}
