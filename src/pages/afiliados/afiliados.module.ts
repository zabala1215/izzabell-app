import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AfiliadosPage } from './afiliados';

@NgModule({
  declarations: [
    AfiliadosPage,
  ],
  imports: [
    IonicPageModule.forChild(AfiliadosPage),
  ],
  exports: [
    AfiliadosPage
  ]
})
export class AfiliadosPageModule {}
