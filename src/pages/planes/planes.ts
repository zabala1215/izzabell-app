import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ToastController } from 'ionic-angular';
import {UsuarioServiceProvider} from '../../providers/usuario-service/usuario-service';
import { NativeStorage } from '@ionic-native/native-storage';



/**
 * Generated class for the PlanesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-planes',
  templateUrl: 'planes.html',
})
export class PlanesPage {

  usuario: any;
  plan: number;
  idUsuario: any;
  loading: Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, public UsuarioServiceProvider: UsuarioServiceProvider, private loadingCtrl: LoadingController, private nativeStorage: NativeStorage, private toastCtrl: ToastController) {
    this.usuario = {tipo_plan : 0};
  }

  ionViewWillEnter() {
    //Obtenemos los datos del usuario
    this.nativeStorage.getItem('user')
      .then(
          data => {
            this.usuario = data;
            console.log("---Recuperado");
            console.log(JSON.stringify(this.usuario));
          },
          error => {
            console.log("Error "+error);
          }
      );
  }

  editarPlan(){
    this.UsuarioServiceProvider.editPlan(this.usuario.tipo_plan, this.usuario.id).then((result) => {
      this.presentToast();
      console.log(result);
    }, (err) => {
      console.log(err);
    });
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Se ha cambiado el plan con éxito',
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

}
