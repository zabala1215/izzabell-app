import { Component, ViewChild, trigger, transition, style, state, animate, keyframes } from '@angular/core';
import { IonicPage, NavController, NavParams , Slides} from 'ionic-angular';
// import { LoginPage } from '../../pages/login/login';

@IonicPage()
@Component({
  selector: 'page-inicio-slides',
  templateUrl: 'inicio-slides.html',
  animations: [

    trigger('bounce', [
      state('*', style({
        transform: 'translateX(0)'
      })),
      transition('* => rightSwipe', animate('700ms ease-out', keyframes([
        style({transform: 'translateX(0)', offset: 0}),
        style({transform: 'translateX(-65px)', offset: .3}),
        style({transform: 'translateX(0)', offset: 1})
      ]))),
      transition('* => leftSwipe', animate('700ms ease-out', keyframes([
        style({transform: 'translateX(0)', offset: 0}),
        style({transform: 'translateX(65px)', offset: .3}),
        style({transform: 'translateX(0)', offset: 1})
      ])))
    ])
  ]
})
export class InicioSlidesPage {

  @ViewChild(Slides) slides: Slides;

  skipMsg: string = "Skip";
  state: string = 'x';

  color: string = "#65CAE3";
  ultima = true;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  irALogin(){
    this.navCtrl.setRoot('Login2Page');
  }

  next(){
    this.slides.slideNext();
  }

  skip() {
    this.slides.slideTo(4, 500);
  }

  slideChanged() {

    let currentIndex = this.slides.getActiveIndex();
    this.ultima = true;

    if(currentIndex % 2 == 0) {
      this.color = '#65CAE3';
    }else{
      this.color = 'white';
    }

    if (this.slides.isEnd())
      this.ultima = false;
  }

  slideMoved() {
    if (this.slides.getActiveIndex() >= this.slides.getPreviousIndex())
      this.state = 'rightSwipe';
    else
      this.state = 'leftSwipe';
  }

  animationDone() {
    this.state = 'x';
  }

}
