import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InicioSlidesPage } from './inicio-slides';

@NgModule({
  declarations: [
    InicioSlidesPage,
  ],
  imports: [
    IonicPageModule.forChild(InicioSlidesPage),
  ],
  exports: [
    InicioSlidesPage
  ]
})
export class InicioSlidesPageModule {}
