import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ModalController, ActionSheetController, Platform, LoadingController, ToastController, AlertController, Loading } from 'ionic-angular';
import { Camera } from "@ionic-native/camera";
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from "@ionic-native/file-path";
import { File } from '@ionic-native/file';
import { NativeStorage } from "@ionic-native/native-storage";



declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-lo-que-quieras',
  templateUrl: 'lo-que-quieras.html',
})
export class LoQueQuierasPage {

  solicitud: any;
  loading: Loading;
  adjunto: any;
  query: any;
  lastImage: string = null;
  resultado: any;
  cargaComentarios: boolean = true;
  comentarios: any;

  direccionActual: any;
  direccionSelected: any;
  usuario: any;
  direcciones: any;
  origen: any;
  destino: any;


  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public platform: Platform, private loadingCtrl: LoadingController, private toastCtrl: ToastController, private alertCtrl: AlertController, public nativeStorage: NativeStorage) {
    this.origen = {formatted_address: ''};
    this.destino = {formatted_address: ''};
    this.direcciones = [];
     this.nativeStorage.getItem('user')
      .then(
        data => {
          this.usuario = data;
          if(this.usuario.direccion_casa != null) 
            this.direcciones.push({direccion: this.usuario.direccion_casa, latitud: this.usuario.casa_lat, longitud: this.usuario.casa_long});
          if(this.usuario.direccion_trabajo != null) 
            this.direcciones.push({direccion: this.usuario.direccion_trabajo, latitud: this.usuario.trabajo_lat, longitud: this.usuario.trabajo_long});
          if(this.usuario.direccion_otro != null) 
            this.direcciones.push({direccion: this.usuario.direccion_otro, latitud: this.usuario.otro_lat, longitud: this.usuario.otro_long});

          this.direccionActual = this.direcciones[0].direccion;
          this.direccionSelected = this.direcciones[0];

          console.log('direcciones', this.direcciones);
            
          console.log("Usuario recuperado: "+JSON.stringify(data));
        },
        error => {
          console.error(error);
          this.loading.dismiss();
        }
      );
  }

  cambio(selectedValue: any) {
    console.log(selectedValue);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoQueQuierasPage');

  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  direccion1() {
    console.log('Direccion');
    let modal = this.modalCtrl.create('AutocompletePage');
    modal.onDidDismiss(data => {
      if(data){
        this.origen = data;
        console.log(data);
        // this.navCtrl.push('CheckoutPage', {
        //   tipo: 5,
        //   solicitud: this.solicitud,
        //   adjunto: this.lastImage,
        //   direccion: data,
        //   direccionActual: this.direccionSelected
        // });
      }
    });
    modal.present();
  }

  direccion2() {
    console.log('Direccion');
    let modal = this.modalCtrl.create('AutocompletePage');
    modal.onDidDismiss(data => {
      if(data){
        console.log(data);
        this.destino = data;
      }
    });
    modal.present();
  }

  continuar() {
    this.navCtrl.push('CheckoutPage', {
      tipo: 5,
      solicitud: this.solicitud,
      adjunto: this.lastImage,
      direccion: this.destino,
      direccionActual: this.origen
    });
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Enviar Archivo',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Agregar Documento',
          icon: 'document',
          handler: () => {
            //Adjuntar documento
          }
        },
        {
          text: 'Imagen de la galería',
          icon: 'image',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Tomar foto',
          icon: 'camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
  // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
}

// Create a new name for the image
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}

// Copy the image to a local folder
private copyFileToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.lastImage = newFileName;
  }, error => {
    this.presentToast('Ha ocurrido un error mientras se almacenaba el archivo.');
  });
}

// Always get the accurate path to your apps folder
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + img;
  }
}

public uploadImage() {
  // Destination URL
  var url = "http://www.izzabell.com/subirImg.php";

  // File for Upload
  var targetPath = this.pathForImage(this.lastImage);

  // File name only
  var filename = this.lastImage;

  var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : {'fileName': filename}
  };

  const fileTransfer: TransferObject = this.transfer.create();

  this.loading = this.loadingCtrl.create({
    content: 'Cargando...',
  });
  this.loading.present();

  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath, url, options).then(data => {
    this.loading.dismissAll()
    this.presentToast('Se ha subido la imagen.');
    this.lastImage = null;
  }, err => {
    this.loading.dismissAll()
    this.presentToast('Error al subir la imagen.');
  });
}

  none() {
    console.log('valor:', this.solicitud && this.origen.formatted_address != '' && this.destino.formatted_address != '');
    return !(this.solicitud && this.origen.formatted_address != '' && this.destino.formatted_address != '')
  }

}
