import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoQueQuierasPage } from './lo-que-quieras';

@NgModule({
  declarations: [
    LoQueQuierasPage,
  ],
  imports: [
    IonicPageModule.forChild(LoQueQuierasPage),
  ],
  exports: [
    LoQueQuierasPage
  ]
})
export class LoQueQuierasPageModule {}
