import { Component, NgZone, ViewChild, ElementRef, OnInit, trigger, transition, style, state, animate, group  } from '@angular/core';
import { Platform, IonicPage, NavController, NavParams } from 'ionic-angular';
import { ViewController, ModalController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';


declare var google: any;

@IonicPage()
@Component({
  selector: 'page-autocomplete',
  templateUrl: 'autocomplete.html',
  animations: [
    trigger('flyInOut', [
      state('in', style({
        transform: 'translate(0, 0)'
      })),
      state('out', style({
        transform: 'translate(0, 300%)'
      })),
      transition('in => out', animate('300ms ease-in')),
      transition('out => in', animate('300ms ease-out'))
    ])
  ]
})
export class AutocompletePage{

  autocompleteItems: any;
  autocomplete: any;
  acService:any;
  placesService: any;

  public pan: number = 0;

  flyInOutState = "out";
  ocultar: boolean = false;

  @ViewChild('map') mapElement: ElementRef;
  map: any;

  address:any = {
      place: '',
      set: false,
  };
  
  markers = [];
  placedetails: any;

  center: any;
  place_id: any;
  data:  any;

  direccion: any;
  ubicacionActiva: boolean;

  constructor (public modalCtrl: ModalController, public viewCtrl: ViewController, private zone: NgZone, public nav: NavController, public http: Http, private geolocation: Geolocation, private diagnostic: Diagnostic, public alertCtrl: AlertController, public platform: Platform) {
      
    document.addEventListener('resume', () => {
       console.log('APLICACION EN RESUMEN');
       this.diagnostic.isLocationEnabled().then(
        (isAvailable) => {
          if(isAvailable){
            console.log('Se ha activado la ubicacion por gps');
            this.ubicacionActiva = true;
            this.initMap(this.ubicacionActiva);
          }
        }).catch( (e) => {
          console.log(e);
        });
    });
  }

  ngOnInit() {
    this.initPlacedetails();
    this.acService = new google.maps.places.AutocompleteService();        
    this.autocompleteItems = [];
    this.autocomplete = {
        query: ''
    };      
     
    
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter');
    this.diagnostic.isLocationEnabled().then(
      (isAvailable) => {
        if(!isAvailable){
          this.showConfirm();
          this.ubicacionActiva = false;
        }else{
          this.ubicacionActiva = true;
          this.initMap(this.ubicacionActiva);
        }
    }).catch( (e) => {
      console.log(e);
    });
  }

  showConfirm() {
    let confirm = this.alertCtrl.create({
      message: 'Izzabell desea conocer tu ubicación para ofrecerte una mejor experiencia',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Disagree clicked');
            this.ubicacionActiva = false;
            this.initMap(this.ubicacionActiva);
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Agree clicked');
            this.ubicacionActiva = true;
            this.diagnostic.switchToLocationSettings();
            
          }
        }
      ]
    });
    confirm.present();
  }

  dismiss() {
    console.log('dismiss');
    //No se escogio dirección
    this.viewCtrl.dismiss();
  }

  toggleFlyOut(){
    this.flyInOutState = 'out';
    this.ocultar = false;
  }

  toggleFlyIn(){
    this.flyInOutState = 'in';
    this.ocultar = true;
  }

  chooseItem(item: any) {
  
    console.log(item);
    this.data = item.place_id;
    console.log('DATA', this.data);
    this.address.place = item.description;
    this.place_id = item.place_id;

    this.getPlaceDetail(this.place_id);
    this.toggleFlyOut();
  }

  public cambiarState(event) : void{
    this.toggleFlyIn();
    // event.inputElement.select();
    // event.inputElement.select();
  }

  cancelar(){
    this.toggleFlyOut();
  }

  updateSearch() {
    

    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    let me = this;
    this.acService.getPlacePredictions({ input: this.autocomplete.query, componentRestrictions: {country: 'PA'} }, function (predictions, status) {
      me.autocompleteItems = [];
      me.zone.run(function () {
        predictions.forEach(function (prediction) {
          console.log(prediction);
          me.autocompleteItems.push(prediction);
        });
      });
    });
  }  

  ionViewDidLoad() {
    console.log('ionViewDidLoad AutocompletePage');
  }

  /**
   * PARA CARGAR EL MAPA
   */

      
    private reset() {
        this.initPlacedetails();
        this.address.place = '';
        this.address.set = false;
    }

    private getPlaceDetail(place_id:string):void {


        console.log('Entro correctamente');
        var self = this;
        var request = {
            placeId: place_id
        };
        this.placesService = new google.maps.places.PlacesService(this.map);
        this.placesService.getDetails(request, callback);
        function callback(place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                console.log('page > getPlaceDetail > place > ', place);
                // set full address
                self.placedetails.address = place.formatted_address;
                self.placedetails.lat = place.geometry.location.lat();
                self.placedetails.lng = place.geometry.location.lng();
                for (var i = 0; i < place.address_components.length; i++) {
                    let addressType = place.address_components[i].types[0];
                    let values = {
                        short_name: place.address_components[i]['short_name'],
                        long_name: place.address_components[i]['long_name']
                    }
                    if(self.placedetails.components[addressType]) {
                        self.placedetails.components[addressType].set = true;
                        self.placedetails.components[addressType].short = place.address_components[i]['short_name'];
                        self.placedetails.components[addressType].long = place.address_components[i]['long_name'];
                    }                                     
                }                  
                // set place in map
                self.map.setCenter(place.geometry.location);
                self.createMapMarker(place);
                // populate
                self.address.set = true;
                console.log('page > getPlaceDetail > details > ', self.placedetails);
            }else{
                console.log('page > getPlaceDetail > status > ', status);
            }
        }
    }

    panEvent(e) {
      this.center = this.map.getCenter(); 
      this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+this.center.lat()+', '+this.center.lng()+'&key=AIzaSyDlZDcx723rlGAJBuoqVZYvJnEm5-s7xnU').map(res => res.json()).subscribe(data => {
        console.log(data.results[0].formatted_address);
        this.autocomplete.query = data.results[0].formatted_address;
      });
    }

    getUbicacion(){
      this.center = this.map.getCenter(); 
      this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng='+this.center.lat()+', '+this.center.lng()+'&key=AIzaSyDlZDcx723rlGAJBuoqVZYvJnEm5-s7xnU').map(res => res.json()).subscribe(data => {
        console.log(data.results);
        let item = {
          latitud: this.center.lat(),
          longitud: this.center.lng(),
          formatted_address: data.results[0].formatted_address,
          address: this.address.place
        }
        this.viewCtrl.dismiss(item);
      });
      
    }
     
    private initMap(gpsActivo) {
      console.log('CARGANDO MAPA CON', gpsActivo);
      if(gpsActivo) {

        //CUANDO EL USUARIO HA ENCENDIDO EL GPS
        console.log('GPS ACTIVO');
        this.geolocation.getCurrentPosition().then((position) => {
          console.log('EVENTO SE DISPARA');
          let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
          console.log(latLng);
          var point = {lat: position.coords.latitude, lng: position.coords.longitude}; 
          let divMap = (<HTMLInputElement>document.getElementById('map'));
          this.map = new google.maps.Map(divMap, {
              center: point,
              zoom: 18,
              disableDefaultUI: true,
              draggable: true,
              zoomControl: true
          });
        
          this.createMarker(latLng);
        }, (err) => {
          console.log(err);
        });
         
      }else{

        //CUANDO EL USUARIO NO HA ENCENDIDO EL GPS
        var point = {lat: 8.985377, lng: -79.525062}; 
        let divMap = (<HTMLInputElement>document.getElementById('map'));
        this.map = new google.maps.Map(divMap, {
            center: point,
            zoom: 18,
            disableDefaultUI: true,
            draggable: true,
            zoomControl: true
        });
      }
    }

    private createMarker(position:any):void {
      console.log('Crear marcador de inicio', position);
      var image = 'http://izzabell.com/714.gif';


      CustomMarker.prototype = new google.maps.OverlayView();

	    function CustomMarker(opts) {
	        this.setValues(opts);
	    }

      CustomMarker.prototype.draw = function() {
        var div = this.div;
        if (!div) {
          console.log('AQUI SE REPITE');
          div = this.div = document.createElement('div');
          var pinEffect = document.createElement('div');
          pinEffect.className = "pin-effect";
          var pin = document.createElement('div');
          pin.className = "pin";

          div.appendChild(pin);
          div.appendChild(pinEffect);

          div.style.position = 'absolute';
          div.style.cursor = 'pointer';

          var panes = this.getPanes();
          panes.overlayImage.appendChild(div);

        }

        var point = this.getProjection().fromLatLngToDivPixel(position);
        console.log('POSITION: ', position);
        console.log('POINT: ', point);
            if (point) {
                div.style.left = point.x + 'px';
                div.style.top = point.y + 'px';
            }
        
      };

      var marker = new CustomMarker({
          position: position,
          map: this.map,
      });

    }
    

    private createMapMarker(place:any):void {
      
      var placeLoc = place.geometry.location;
      CustomMarker.prototype = new google.maps.OverlayView();

	    function CustomMarker(opts) {
	        this.setValues(opts);
	    }

      CustomMarker.prototype.draw = function() {
        var div = this.div;
        if (!div) {
          console.log('AQUI SE REPITE');
          div = this.div = document.createElement('div');
          var pinEffect = document.createElement('div');
          pinEffect.className = "pin-effect";
          var pin = document.createElement('div');
          pin.className = "pin";

          div.appendChild(pin);
          div.appendChild(pinEffect);

          div.style.position = 'absolute';
          div.style.cursor = 'pointer';

          var panes = this.getPanes();
          panes.overlayImage.appendChild(div);

        }

        var point = this.getProjection().fromLatLngToDivPixel(placeLoc);
        
            if (point) {
                div.style.left = point.x + 'px';
                div.style.top = point.y + 'px';
            }
        
      };

      var marker = new CustomMarker({
          position: placeLoc,
          map: this.map,
      });
  
    }
    

    private initPlacedetails() {
        this.placedetails = {
            address: '',
            lat: '',
            lng: '',
            components: {
                route: { set: false, short:'', long:'' },                           // calle 
                street_number: { set: false, short:'', long:'' },                   // numero
                sublocality_level_1: { set: false, short:'', long:'' },             // barrio
                locality: { set: false, short:'', long:'' },                        // localidad, ciudad
                administrative_area_level_2: { set: false, short:'', long:'' },     // zona/comuna/partido 
                administrative_area_level_1: { set: false, short:'', long:'' },     // estado/provincia 
                country: { set: false, short:'', long:'' },                         // pais
                postal_code: { set: false, short:'', long:'' },                     // codigo postal
                postal_code_suffix: { set: false, short:'', long:'' },              // codigo postal - sufijo
            }    
        };        
    }

}
