import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServicioAsistenciaPage } from './servicio-asistencia';

@NgModule({
  declarations: [
    ServicioAsistenciaPage,
  ],
  imports: [
    IonicPageModule.forChild(ServicioAsistenciaPage)
  ],
  exports: [
    ServicioAsistenciaPage
  ]
})
export class ServicioAsistenciaPageModule {}