import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MensajeriaUbicacionListPage } from './mensajeria-ubicacion-list';

@NgModule({
  declarations: [
    MensajeriaUbicacionListPage,
  ],
  imports: [
    IonicPageModule.forChild(MensajeriaUbicacionListPage),
  ],
  exports: [
    MensajeriaUbicacionListPage
  ]
})
export class MensajeriaUbicacionListPageModule {}
