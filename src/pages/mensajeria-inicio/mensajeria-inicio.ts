import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

//Modelos
import { SolicitudMensaje } from '../../custom_elems/solicitud_mensaje/solicitud_mensaje';
import { Ubicacion } from '../../custom_elems/ubicacion/ubicacion';

//Nativo 
import { Geolocation } from '@ionic-native/geolocation';
import { NativeStorage } from '@ionic-native/native-storage';

@IonicPage()
@Component({
  selector: 'page-mensajeria-inicio',
  templateUrl: 'mensajeria-inicio.html',
})
export class MensajeriaInicioPage {
  @ViewChild('text') textInput;

  ubicacion: Ubicacion = null; //Ubicacion del usuario
  solicitud: SolicitudMensaje = null; //Solicitud del mensaje
  texto: string = null;
  id: number; //Id de usuario

  constructor(private navCtrl: NavController, private geolocation: Geolocation, private nativeStorage: NativeStorage) {
    let me = this; //Evitar errores

    //Obtenemos los datos del usuario
    me.nativeStorage.getItem('user')
    .then(
      data => {
        me.id = data.id; //Asignamos la id del usuario
      },
      error => console.error(error)
    );

    //Obtenemos la posicion y asignamos
    me.geolocation.watchPosition({ enableHighAccuracy: true }).subscribe((respuesta) => {
      me.ubicacion = new Ubicacion(respuesta.coords.latitude, respuesta.coords.longitude, respuesta.coords.altitude, respuesta.coords.accuracy);
    }, error => console.log("Error obteniendo su ubicación"));

  }

  //Click Flecha
  clickBack() {
    this.navCtrl.pop({direction: 'back'});
  }
  
  //Al hacer click fuera del input
  blur() {
    this.textInput.blur();
  }

  //Obtener el texto del textarea *** BUG IONIC Y ANGULAR ***
  getTextoTextArea(evento) {
    this.texto = evento;
    return true;
  }

  //Click en Realizar
  clickRealizar() {
    let me = this;
    //Creamos la solicitud
    me.solicitud = new SolicitudMensaje(me.texto, me.id, me.ubicacion);
    console.log("Procedemos a enviar a ubicacion "+JSON.stringify(me.solicitud));

    me.navCtrl.push("MensajeriaUbicacionPage", { solicitud: me.solicitud });
  }

}
