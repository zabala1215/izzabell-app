import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MensajeriaInicioPage } from './mensajeria-inicio';

@NgModule({
  declarations: [
    MensajeriaInicioPage,
  ],
  imports: [
    IonicPageModule.forChild(MensajeriaInicioPage),
  ],
  exports: [
    MensajeriaInicioPage
  ]
})
export class MensajeriaInicioPageModule {}
