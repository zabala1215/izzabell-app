import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-super',
  templateUrl: 'super.html',
})
export class SuperPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SuperPage');
  }

  categoria () {
    this.navCtrl.push('SuperCategoriaPage');
  }

}
