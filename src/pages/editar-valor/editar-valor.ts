import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UsuarioServiceProvider } from '../../providers/usuario-service/usuario-service';

@IonicPage()
@Component({
  selector: 'page-editar-valor',
  templateUrl: 'editar-valor.html',
})
export class EditarValorPage {

  valor;
  columna = ['nombre', 'telefono', 'email'];
  pos;
  usuario;

  constructor(public navCtrl: NavController, public navParams: NavParams, public UsuarioServiceProvider: UsuarioServiceProvider) {
    this.pos = navParams.get('pos');
    this.valor = navParams.get('valor');
    this.usuario =navParams.get('usuario');

    console.log(this.columna);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditarValorPage');
  }

  editarValor(){
    // this.UsuarioServiceProvider.editColUsuario('item='+this.valor+'&columna='+this.columna[this.pos]+'&usuario='+this.usuario.id).then((result) => {
    //   console.log(result);
    //   this.navCtrl.pop();
    // }, (err) => {
    //   console.log(err);
    // });
  }

}
