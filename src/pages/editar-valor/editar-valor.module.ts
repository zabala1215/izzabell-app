import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditarValorPage } from './editar-valor';

@NgModule({
  declarations: [
    EditarValorPage,
  ],
  imports: [
    IonicPageModule.forChild(EditarValorPage),
  ],
  exports: [
    EditarValorPage
  ]
})
export class EditarValorPageModule {}
