import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-verificar-numero',
  templateUrl: 'verificar-numero.html',
})
export class VerificarNumeroPage {
  @ViewChild('cod1') cod1;
  @ViewChild('cod2') cod2;
  @ViewChild('cod3') cod3;
  @ViewChild('cod4') cod4;

  //Datos obtenidos
  request_code: string = "";
  sms_status: number = 0;
  siguiente: string = "";
  usuario: any;

  codigo = [null,null,null,null] //Código introducido
  loadingView: any;

  constructor(public http: Http, public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public alertCtrl: AlertController, private auth: AuthServiceProvider) {
    //Obtenemos los datos enviados al crear vista
    this.request_code = navParams.get('request_id');
    this.sms_status = navParams.get('sms_status');
    this.siguiente = navParams.get('siguiente');
    this.usuario = navParams.get('usuario');

    this.loadingView = this.loadingCtrl.create({
      content: "Verificando..."
    });

    console.log("REQUEST CODE: "+this.request_code);
    console.log("Inicio ...");
  }

  //Verificar el usuario
  verificar() {
    //Mostramos la vista de carga
    this.loadingView.present();

    let nav = this.navCtrl;
    let me = this;

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    let options = new RequestOptions({ headers: headers });
    console.log("CODIGO: "+this.codigo[0]+this.codigo[1]+this.codigo[2]+this.codigo[3]);
    this.http.post("https://api.nexmo.com/verify/check/json?api_key=d5826d26&api_secret=863df48605f90546&request_id="+this.request_code+"&code="+this.codigo[0]+this.codigo[1]+this.codigo[2]+this.codigo[3], null, options)
    .map(res => res.json())
    .subscribe(res => {
      this.loadingView.dismiss();
      console.log("RESPUESTA: "+JSON.stringify(res));
      switch(res.status) {
        case "0":
          //Verificación correcta, procedemos
          console.log("VERIFICACION CORRECTA");
          me.nextView(me.siguiente,me.usuario);
        break;
        case "6":
          //Ya se valido o no existe request_id
          console.log("YA SE HA VALIDADO")
          me.nextView(me.siguiente,me.usuario);
        break;
        case "16":
          //Codigo incorrecto
          console.log("CODIGO INCORRECTO");
          let errorAlert = this.crearAlerta('Error', 'Codigo de verificación incorrecto', ['Aceptar']);
        break;
      }
    }, error => {
      this.loadingView.dismiss();
      console.log("Error Verificar "+error);
    });
  }

  //Enviar a lo que procede
  nextView(tipo:string, usuario) {
    let nav = this.navCtrl;
    let me = this;

    switch(tipo) {
      case "email":
        //E-Mail
        nav.push("LoginRegistroPage", {tipo: 'email', usuario: usuario});
      break;
      case "tel":
        //Procedemos a registrar
        me.auth.registro(usuario).subscribe(respuesta => {
          console.log("Respuesta: "+JSON.stringify(respuesta));

          if(respuesta == true) {
            //Registro correcto
            console.log("Registro correcto");
            nav.setRoot(TabsPage);
          } else if(respuesta == false) {
            //Error durante registro
            console.log("Error durante registro");
            me.crearAlerta("Error", "Ha ocurrido un error inesperado durante el registro", ['Aceptar']);
          } else {
            //Otro error
            me.crearAlerta("Error",respuesta, ['Aceptar']);
          }
        });
      break;
    }
  }

  //Crear alerta
  crearAlerta(titulo: string, contenido: string, botones) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: contenido,
      buttons: botones
    });
    return alert;
  }

  //Verificamos cual input sigue
  nextInput(numero) {
    let me = this;
    console.log("NEXT INPUT: "+numero);

    if(numero == 1 || numero == "1") {
      console.log("Numero 1");
      if(me.codigo[0] != null && me.codigo[0] != "" && me.codigo[0] != " " && me.codigo[0] >= 0 && me.codigo[0] <= 9) {
        console.log("Length mayor a 1");
        me.cod2.setFocus();
      }
    } else if(numero == 2 || numero == "2") {
      console.log("Numero 2");
      if(me.codigo[1] != null && me.codigo[1] != "" && me.codigo[1] != " " && me.codigo[1] >= 0 && me.codigo[1] <= 9) {
        console.log("Length mayor a 1");
        me.cod3.setFocus();
      }
    } else if(numero == 3 || numero == "3") {
      console.log("Numero 3");
      if(me.codigo[2] != null && me.codigo[2] != "" && me.codigo[2] != " " && me.codigo[2] >= 0 && me.codigo[2] <= 9) {
        console.log("Length mayor a 1");
        me.cod4.setFocus();
      }
    }
  }

  closeModal() {
    this.navCtrl.pop({direction: 'back'});
  }

}
