import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerificarNumeroPage } from './verificar-numero';

@NgModule({
  declarations: [
    VerificarNumeroPage,
  ],
  imports: [
    IonicPageModule.forChild(VerificarNumeroPage),
  ],
  exports: [
    VerificarNumeroPage
  ]
})
export class VerificarNumeroPageModule {}
