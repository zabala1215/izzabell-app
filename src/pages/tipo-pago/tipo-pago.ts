import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Headers, RequestOptions } from '@angular/http';



@IonicPage()
@Component({
  selector: 'page-tipo-pago',
  templateUrl: 'tipo-pago.html',
})
export class TipoPagoPage {

  idUsuario: any;
  code: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private payPal: PayPal, public http: Http) {
    this.idUsuario = navParams.get('id');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TipoPagoPage');
  }

  goToAddTarjetaPage(){
    this.navCtrl.push( 'NuevaTarjetaPage', {id: this.idUsuario} );
  }

  procesarPago(){
    this.payPal.init({
      PayPalEnvironmentProduction: '',
      PayPalEnvironmentSandbox: 'AdHuCee_tKpQ-hcjIk20nRNRSxX_ge-xfq2qU6FMdsLNMgRKYms50-aeW2_KUeXqoDcCchRmaxt53Xsq'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        // let payment = new PayPalPayment('3.33', 'USD', 'Description', 'sale');
        this.payPal.clientMetadataID().then((data) => {
          // Successfully paid
          console.log(data);
          // Example sandbox response
          //
          // {
          //   "client": {
          //     "environment": "sandbox",
          //     "product_name": "PayPal iOS SDK",
          //     "paypal_sdk_version": "2.16.0",
          //     "platform": "iOS"
          //   },
          //   "response_type": "payment",
          //   "response": {
          //     "id": "PAY-1AB23456CD789012EF34GHIJ",
          //     "state": "approved",
          //     "create_time": "2016-10-03T13:33:33Z",
          //     "intent": "sale"
          //   }
          // }
        }, () => {
          // Error or render dialog closed without being successful
        });
      }, () => {
        // Error in configuration
      });
    }, () => {
      // Error in initialization, maybe PayPal isn't supported or something else
    });
  }

  goToPayPalPage(){
    this.payPal.init({
      PayPalEnvironmentProduction: '',
      PayPalEnvironmentSandbox: 'AdHuCee_tKpQ-hcjIk20nRNRSxX_ge-xfq2qU6FMdsLNMgRKYms50-aeW2_KUeXqoDcCchRmaxt53Xsq'
    }).then(() => {
      // Environments: PayPalEnvironmentNoNetwork, PayPalEnvironmentSandbox, PayPalEnvironmentProduction
      this.payPal.prepareToRender('PayPalEnvironmentSandbox', new PayPalConfiguration({
        merchantName: "Izzabell App", 
        merchantPrivacyPolicyURL: "https://mytestshop.com/policy",
        merchantUserAgreementURL: "https://mytestshop.com/agreement"
        // Only needed if you get an "Internal Service Error" after PayPal login!
        //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
      })).then(() => {
        // let payment = new PayPalPayment('3.33', 'USD', 'Description', 'sale');
        this.payPal.renderFuturePaymentUI().then((data) => {
          // Successfully paid
          console.log('DATA', data);
          this.code = data.response.code;
          
          let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
          let options = new RequestOptions({ headers: headers });

          console.log('La data de la solicitud a agregar es: ', data);
          return new Promise((resolve, reject) => {
            this.http.post('http://www.izzabell.com/api-kingbakery/autorize', 'code='+data.response.code, options)
              .map(res => res.json())
              .subscribe(res => {
                console.log(res);
              }, (err) => {
                console.log(err);
              });
          });
        }, (Error) => {
          console.log('Error ', Error);
        });
      }, () => {
        // Error in configuration
      });
    }, () => {
      // Error in initialization, maybe PayPal isn't supported or something else
    });
  }
}
