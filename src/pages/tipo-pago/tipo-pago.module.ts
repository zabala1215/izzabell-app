import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TipoPagoPage } from './tipo-pago';

@NgModule({
  declarations: [
    TipoPagoPage,
  ],
  imports: [
    IonicPageModule.forChild(TipoPagoPage),
  ],
  exports: [
    TipoPagoPage
  ]
})
export class TipoPagoPageModule {}
