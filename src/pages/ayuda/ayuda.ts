import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-ayuda',
  templateUrl: 'ayuda.html',
})
export class AyudaPage {

  politicasPage: 'AyudaPoliticasPage';
  terminosPage: 'AyudaTerminosPage';

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AyudaPage');
  }

  abrirTerminos(){
    this.navCtrl.push('AyudaTerminosPage');
  }

  correo(){
    window.location.href = "mailto:izzabellapp@gmail.com";
  }

  abrirPoliticas(){
    this.navCtrl.push('AyudaPoliticasPage');
  }

}
