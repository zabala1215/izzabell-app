import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AyudaDudasPage } from './ayuda-dudas';

@NgModule({
  declarations: [
    AyudaDudasPage,
  ],
  imports: [
    IonicPageModule.forChild(AyudaDudasPage),
  ],
  exports: [
    AyudaDudasPage
  ]
})
export class AyudaDudasPageModule {}
