import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//Modelos
import { SolicitudMensaje } from '../../custom_elems/solicitud_mensaje/solicitud_mensaje';

//Proovedores
import { MensajeriaProvider } from '../../providers/mensajeria/mensajeria';


@IonicPage()
@Component({
  selector: 'page-mensajeria-revisar',
  templateUrl: 'mensajeria-revisar.html',
})
export class MensajeriaRevisarPage {

  //Solicitud
  solicitud: SolicitudMensaje;

  //Solo para formatear al mostrar
  total: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private mensajeria: MensajeriaProvider) {
    let self = this;

    //Obtenemos los datos de la solicitud
    self.solicitud = self.navParams.get('solicitud');
    self.solicitud.total.sub = 0;
    self.solicitud.total.tot = 0;
    self.total = parseFloat(""+self.solicitud.total.tot).toFixed(2);
    console.log("RECIBIMOS FINAL "+JSON.stringify(self.solicitud));
  }

  //Click Flecha
  clickBack() {
    this.navCtrl.pop({direction: 'back'});
  }

  //Click en pagar
  pagar() {
    let me = this;

    me.mensajeria.addMensajeria(me.solicitud).subscribe((respuesta) => {
      console.log("Se obtuvo respuesta "+JSON.stringify(respuesta));
      if(respuesta !== false) {
        //No es error
        //--->Terminamos loading
        //Mostramos nueva ventana
        me.navCtrl.push("MensajeriaFinalPage", { id: respuesta.id });
      }
    }, error => console.log("Error "+error));
  }

}
