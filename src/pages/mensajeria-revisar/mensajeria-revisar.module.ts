import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MensajeriaRevisarPage } from './mensajeria-revisar';

@NgModule({
  declarations: [
    MensajeriaRevisarPage,
  ],
  imports: [
    IonicPageModule.forChild(MensajeriaRevisarPage),
  ],
  exports: [
    MensajeriaRevisarPage
  ]
})
export class MensajeriaRevisarPageModule {}
