import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { TabsPage } from '../../pages/tabs/tabs';


@IonicPage()
@Component({
  selector: 'page-mensajeria-final',
  templateUrl: 'mensajeria-final.html',
})
export class MensajeriaFinalPage {

  id: number; //Id de solicitud

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //Obtenemos los datos enviados
    this.id = this.navParams.get("id");
  }

  //Click en Ir a Solicitud
  clickSolicitud() {
    let self = this;
    self.navCtrl.push('TabsPage');
  }

}
