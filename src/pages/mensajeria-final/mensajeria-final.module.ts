import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MensajeriaFinalPage } from './mensajeria-final';

@NgModule({
  declarations: [
    MensajeriaFinalPage,
  ],
  imports: [
    IonicPageModule.forChild(MensajeriaFinalPage),
  ],
  exports: [
    MensajeriaFinalPage
  ]
})
export class MensajeriaFinalPageModule {}
