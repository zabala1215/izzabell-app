import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BilleteraPage } from './billetera';

@NgModule({
  declarations: [
    BilleteraPage,
  ],
  imports: [
    IonicPageModule.forChild(BilleteraPage),
  ],
  exports: [
    BilleteraPage
  ]
})
export class BilleteraPageModule {}
