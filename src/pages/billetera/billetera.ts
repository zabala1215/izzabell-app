import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Loading, ViewController } from 'ionic-angular';
import { PagosServiceProvider } from '../../providers/pagos-service/pagos-service';
import { NativeStorage } from '@ionic-native/native-storage';


@IonicPage()
@Component({
  selector: 'page-billetera',
  templateUrl: 'billetera.html',
})
export class BilleteraPage {

  tarjetas: any;
  idUsuario: any;
  loading: Loading;

  //Tarjeta seleccionada
  tarjetaSelected: any;

  //Variable para determinar si es un modal o una pagina normal que viene desde ajustes
  tipoVista: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public PagosServiceProvider: PagosServiceProvider, private loadingCtrl: LoadingController, private nativeStorage: NativeStorage, public viewCtrl: ViewController) {

    this.tipoVista = navParams.data.tipoVista;
    // this.idUsuario = 53;
    // this.getMetodosPagos(this.idUsuario);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PagosPage');
  }

  getMetodosPagos(id){
    this.PagosServiceProvider.getTarjetasId(id)
    .then(data => {
      this.tarjetas = data;
      console.log(this.tarjetas);
    });
  }

  goToAddTarjetaPage(){
    this.navCtrl.push( 'TipoPagoPage', {id: this.idUsuario} );
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  ionViewWillEnter() {
    // this.showLoading();
    console.log('Entrando a la vista...');
    return this.nativeStorage.getItem('user')
    .then(
      data => {
        return this.getMetodosPagos(data.id),
        this.idUsuario = data.id;
        // this.loading.dismiss();
      },
      error => {
        console.error(error)
        // this.loading.dismiss();
      }
    );
  }

  seleccionado() {
    console.log('CAMBIO DE RADIO');

    //Cuando es un modal
    if(this.tipoVista == 1) {
      console.log('El tipo de vista es 1');
      this.viewCtrl.dismiss({idTarjeta: this.tarjetaSelected});
    }
  }

  dismiss() {
    console.log('dismiss');
    //No se escogio dirección
    this.viewCtrl.dismiss();
  }
  
}
