import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, AlertController, LoadingController } from 'ionic-angular';

import { ServiciosProvider } from '../../providers/servicios/servicios';

@IonicPage()
@Component({
  selector: 'page-servicio',
  templateUrl: 'servicio.html',
})
export class ServicioPage {

  servicio: any;
  caracteristicas: any;
  portada : any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public serviciosProvider: ServiciosProvider, public loadingController: LoadingController, public alertController: AlertController) {
    this.servicio = navParams.get('servicio');
    this.caracteristicas = [];
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicioPage');
    console.log(this.servicio.caracteristicas);
    let tam = this.servicio.caracteristicas[0].length;
    console.log('tam: ', tam);
    
    this.portada = 'assets/images/'+this.servicio.portada;

    for (var i=0; i < tam; i = i+3){
      this.caracteristicas.push({titulo: this.servicio.caracteristicas[0][i], descripcion: this.servicio.caracteristicas[0][i+1], icono: this.servicio.caracteristicas[0][i+2]});
    }

    console.log('caracteristicas: ', this.caracteristicas);
  }

  //NUEVA SOLICITUD
  solicitar() {
    let nav = this.navCtrl;
    let me = this;

    let loading = this.loadingController.create({
      content: "Enviando solicitud"
    });
    loading.present();


    //Añadimos el pago
    me.serviciosProvider.addServicio(me.servicio.id, me.servicio.nombre)
      .then(respuesta => {
        loading.dismiss();
        if(respuesta != false) {
          //Respuesta correcta
          me.crearAlerta("Solicitud enviada","Su solicitud ha sido enviada exitosamente. Pronto recibira una respuesta de nuestro sistema.", ['Aceptar']).present();
          nav.pop({direction: 'back'});
        } else {
          //Error
          console.log("Respuesta false");
          me.crearAlerta("Error","Ha ocurrido un error procesando su solicitud. Intente nuevamente en unos minutos.", ['Aceptar']).present();
        }
      }, error => {
        loading.dismiss();
        //Ocurrio un error
        console.log("Error "+error);
        me.crearAlerta("Error","Ha ocurrido un error procesando su solicitud.", ['Aceptar']).present();
      });
  }

  //Funcion para hacer nueva alerta
  crearAlerta(titulo: string, contenido: string, botones) {
    let alert = this.alertController.create({
      title: titulo,
      subTitle: contenido,
      buttons: botones
    });
    return alert;
  }

}
