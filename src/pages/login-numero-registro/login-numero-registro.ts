import { Component, Input, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { App  } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
import { ViewController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { NativeStorage } from '@ionic-native/native-storage';

@IonicPage()
@Component({
  selector: 'page-login-numero-registro',
  templateUrl: 'login-numero-registro.html',
})
export class LoginNumeroRegistroPage {
  loading: Loading;
  registerCredentials = { nombre: '' , telefono: ''};
  @ViewChild('input2') myInput ;

  constructor(public nav: NavController, public navParams: NavParams, public viewCtrl: ViewController, private auth: AuthServiceProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private app: App) {
    let info = this.auth.getUserInfo();
    this.registerCredentials.telefono = info['telefono'];
  }

  closeModal() {
    this.nav.pop({animation: "md-transition", direction: 'back'});
  }

 public registrar() {
    let nav = this.nav;
    this.showLoading();
    //Registramos
    this.auth.register(this.registerCredentials).subscribe(allowed => {
      if (allowed) {
          //Registrado
        console.log('Registro exitoso');
        this.loading.dismiss();
        nav.setRoot(TabsPage);
      } else {
        this.showError('Ha ocurrido un error al registrar el usuario');
      }
    },
      error => {
        this.showError(error);
      });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginNumeroPage');
  }

}
