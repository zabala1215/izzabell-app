import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginNumeroRegistroPage } from './login-numero-registro';

@NgModule({
  declarations: [
    LoginNumeroRegistroPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginNumeroRegistroPage),
  ],
  exports: [
    LoginNumeroRegistroPage
  ]
})
export class LoginNumeroRegistroPageModule {}
