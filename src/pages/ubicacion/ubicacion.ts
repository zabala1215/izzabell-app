import { Component, NgZone, trigger, transition, style, state, animate, keyframes } from '@angular/core';
import { Http } from '@angular/http';
import { App, NavController, NavParams, IonicPage } from 'ionic-angular';

//Modelos
import { Ubicacion } from '../../custom_elems/modelos/ubicacion';
import { SolicitudMensaje } from '../../custom_elems/modelos/solicitud_mensaje';
import { SolicitudCajero } from '../../custom_elems/modelos/solicitud_cajero';
import { Prediccion } from '../../custom_elems/modelos/prediccion';

//Nativo
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker,
 Geocoder,
 GeocoderRequest,
 GeocoderResult,
 BaseArrayClass,
} from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-ubicacion',
  templateUrl: 'ubicacion.html',
  animations: [
    trigger('preds_list', [
      state('show', style({
        'min-height': "100%",
        'max-height': "100%",
        'height': "100%",
        "overflow-y": "scroll"
      })),
      state('hide', style({
        'min-height': "0%",
        'max-height': "0%",
        'height': "0%"
      })),
      transition('hide <=> show', animate('300ms ease'))
    ]),
    /*trigger('mapa', [
      state('hide', style({
        'min-height': "100%",
        'max-height': "100%",
        'height': "100%"
      })),
      state('hide', style({
        'min-height': '0%'
      })),
      transition('hide => show', animate('800ms ease'))
    ]),*/
  ]
})
export class UbicacionPage {

  //Mapa
  map: GoogleMap;
  ubicacion: Ubicacion; //Ubicacion actual del mapa
  mapaHieghtSave = ""; //Para manejar eventos de animaciones
  mapaHeight = ""; //Tamaño de mapa
  //Predicciones de Mapa
  autocompleteService = new google.maps.places.AutocompleteService(); //Servicio de Prediccion de Ubicacion
  buscarUbicacion = { query: '' }; //Datos del cuadro de busqueda
  predicciones: Prediccion[] = []; //Arreglo con predicciones

  //Datos de envio
  solicitudCajero: SolicitudCajero;
  solicitudMensaje: SolicitudMensaje;
  tipo: string;
  cajero: boolean = false;
  mensaje: boolean = false;

  //Estados y Animaciones
  preds_state: string = "hide"; //Estado de vista de predicciones
  mapa_state: string = "show"; //Estado del mapa
  display_items_state = "none"; //Estado de prediccion (Item)

  constructor(private nav: NavController, private navParams: NavParams, private googleMaps: GoogleMaps, private geocoder: Geocoder, private statusbar: StatusBar, private zone: NgZone, private http: Http, private app: App) {
    let self = this;
    //Arreglamos estilos
    statusbar.overlaysWebView(true);
    statusbar.styleBlackTranslucent();

    //Obtenemos los elementos enviados
    self.tipo = navParams.get("tipo");
    //Verificamos que tipo es
    switch(self.tipo) {
      case "cajero": 
        self.cajero = true;
        self.solicitudCajero = navParams.get("solicitud");
        //Obtenemos los datos enviados y cargamos el mapa
        setTimeout(function () {
          self.cargarMapa(self.solicitudCajero.ubicacion);
        }, 100);
      break;
      case "mensaje":
        self.mensaje = true;
        self.solicitudMensaje = navParams.get("solicitud");
        //Obtenemos los datos enviados y cargamos el mapa
        /*setTimeout(function () {
          self.cargarMapa(self.solicitudMensaje.ubicacion);
        }, 100);*/
      break;
    }

    console.log("---> DATOS OBTENIDOS <---");
    console.log("Tipo "+self.tipo);
    console.log("Solicitud Cajero"+JSON.stringify(self.solicitudCajero));
    console.log("Solicitud Mensaje"+JSON.stringify(self.solicitudMensaje));
  }

  //Vista cargó
  ionViewDidLoad() {
    //Vista cargo
    console.log("Vista cargó");
    //Obtenemos tamaño total de la vista y restamos los elementos, luego establecemos el tamaño del mapa
    this.mapaHeight = (document.getElementById("ion-content").offsetHeight - document.getElementById("cabecera").offsetHeight - document.getElementById("opciones").offsetHeight) + "px";
    this.mapaHieghtSave = this.mapaHeight;
    console.log("Altura de opciones "+this.mapaHeight);
  }

  //Click Flecha
  clickBack() {
    let self = this;
    //Si predicciones esta abierto, el click quita blur
    if(self.preds_state == "show") {
      document.getElementById("buscar_dir").blur();
    } else {
      this.nav.pop({direction: 'back'});
    }
  }

  //Focus de input search
  input_focus() {
    console.log("FOCUS");
    let self = this;
    //self.predicciones= []; //Vaciamos predicciones
    self.display_items_state = "block";
    self.preds_state = "show"; //Animacion
    setTimeout(function() {
      self.mapaHeight = "0px";
    }, 200);
    console.log(self.preds_state);
  }

  //Blur de input search
  input_blur() {
    console.log("BLUR");
    let self = this;
    self.mapaHeight = self.mapaHieghtSave;
    self.preds_state = "hide";
    //self.predicciones = []; //Vaciamos predicciones
    self.display_items_state = "none";
    console.log(self.preds_state);
  }

  //Cargar mapa
  cargarMapa(ubicacion: Ubicacion){
    console.log("Cargando Mapa "+JSON.stringify(ubicacion));
    //Asignacion
    let me = this;
    let elemento: HTMLElement = document.getElementById("mapa"); //Div donde cargaremos mapa
    let camPos: CameraPosition = {
      target: {
        lat: ubicacion.latitud,
        lng: ubicacion.longitud
      },
      zoom: 18,
      tilt: 0
    };
    me.map = me.googleMaps.create(elemento, {
      controls: {
        myLocationButton: true,
        mapToolbar: true
      },
      gestures: {
        scroll: true,
        tilt: false,
        rotate: true,
        zoom: true
      },
      preferences: { building: false },
      styles: [{"elementType":"labels.text.fill","stylers":[{"color":"#666666"},{"weight":1.5}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#D5DCDC"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#D5DCDC"}]},{"featureType":"landscape.man_made","elementType":"geometry.stroke","stylers":[{"color":"#C7CDCD"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#f7f7f7"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#93C0DC"}]}],
      camera: camPos
    });

    //Esperamos a que el mapa este listo
    me.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      console.log("Mapa listo");
      me.map.clear();
      let markerOptions: MarkerOptions = {
        position: {lat: ubicacion.latitud, lng: ubicacion.longitud},
        flat: true,
        title: "Aquí estoy",
        icon: {
          url: 'www/assets/images/pin.png',
          size: {
            width: 44,
            height: 44
          }
        }
      };

      me.map.setCameraTarget({lat: ubicacion.latitud, lng: ubicacion.longitud}); //Posicionamos la camara

      me.map.addMarker(markerOptions)
        .then((marker: Marker) => {
          console.log("Se añadio marker");
        }).catch((error) => console.log("Error "+error));

      //Al mover camara, insertamos marcador
      me.map.on("camera_change").subscribe((evento) => {
        me.map.clear(); //Eliminamos todos los elementos de un mapa
        //Asignamos
        markerOptions.position.lat = evento.target.lat;
        markerOptions.position.lng = evento.target.lng;
        me.ubicacion = new Ubicacion(evento.target.lat, evento.target.lng, 0, 0);
        //Añadimos
        me.map.addMarker(markerOptions);
      }, error => console.log("Error moviendo camara "+error));

    });
    //Por si acaso, volvemos a posicionar la camara
    me.map.setCameraTarget({lat: ubicacion.latitud, lng: ubicacion.longitud}); //Posicionamos la camara

  }

  //Obtenemos las predcciones de la ubicacion
  getPredicciones() {
    //Asignacion
    let self = this;
    //Verificamos que el texto no este vacio
    if(self.buscarUbicacion.query == '') {
      self.predicciones = [];
    }
    //Obtenemos las predicciones -> Restringimos solamente a Panama (PA)
    let requerimientos = {input: self.buscarUbicacion.query, componentRestrictions: { country: 'PA' }};
    self.autocompleteService.getPlacePredictions(requerimientos, function(preds, status) {
      self.predicciones = []; //Eliminamos los items anteriores
      //Populamos el arreglo en background
      self.zone.run(function () {
        preds.forEach(function(pred) {
          self.predicciones.push(new Prediccion(pred.description, pred.place_id));
        });
      });
    });
  }

  //Click en prediccion
  clickPrediccion(predObtenida: Prediccion) {
    let self = this;
    let url = "https://maps.googleapis.com/maps/api/place/details/json?placeid="+predObtenida.placeId+"&key=AIzaSyDlZDcx723rlGAJBuoqVZYvJnEm5-s7xnU";
    //Obtenemos los datos del lugar seleccionado
    self.http.get(url)
      .map(res => res.json()).subscribe(respuesta => {
        console.log("DATOS DE CLICK "+JSON.stringify(respuesta.result.geometry.location));
        //Movemos la camara
        self.map.animateCameraZoomIn();
        self.map.animateCamera({
          target: {
            lat: respuesta.result.geometry.location.lat,
            lng: respuesta.result.geometry.location.lng
          },
          zoom: 18,
          duration: 500
        }).then(() => {
          //Esperamos que pase la animacion de la camara -> PROVOCA BUG
          setTimeout(function() {
            //Asignamos los datos
            self.ubicacion = new Ubicacion(respuesta.result.geometry.location.lat, respuesta.result.geometry.location.lng, 0, 0, predObtenida.description);
            self.buscarUbicacion.query = self.ubicacion.direccion; //Mostramos descripcion en input
          }, 700);
        });
      }, error => {
        console.log("Error obteniendo datos de place"+error);
      });

    //Cambiamos la ubicacion del mapa
    console.log("OBTENIDO "+JSON.stringify(predObtenida));
  }

  //Click en ver ubicaciones guardadas
  selUbicacion() {
    let self = this;
    self.nav.push('UbicacionListPage', { tipo: self.tipo, solicitud: '' });
  }

  //Click en Aquí Estoy
  setUbicacion() {
    let self = this;
    //Verificamos que tipo es
    if(self.cajero) {
      //Cajero
      self.solicitudCajero.ubicacion = self.ubicacion;
      console.log("ASIGNAMOS "+JSON.stringify(self.solicitudCajero));
      //Si la direccion es nula, obtenemos la direccion
      console.log("ANTES DE VERIFICAR");
      console.log("NULO "+self.solicitudCajero.ubicacion.direccion == null ? true:false);
      console.log("EMPTY "+self.solicitudCajero.ubicacion.direccion == "" ? true:false);
      
      if(self.solicitudCajero.ubicacion.direccion == null || self.solicitudCajero.ubicacion.direccion == "") {
        console.log("IF");
        self.solicitudCajero.ubicacion.getDireccion();
      }
      console.log("Procedemos a enviar cajero "+JSON.stringify(self.solicitudCajero));
      //self.nav.push("CajeroRevision", { solicitud: self.solicitud });
    }
  }
}
