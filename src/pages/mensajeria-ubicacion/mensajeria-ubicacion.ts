import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

//Modelos
import { SolicitudMensaje } from '../../custom_elems/solicitud_mensaje/solicitud_mensaje';
import { Ubicacion } from '../../custom_elems/ubicacion/ubicacion';

//Nativo
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import {
 GoogleMaps,
 GoogleMap,
 GoogleMapsEvent,
 LatLng,
 CameraPosition,
 MarkerOptions,
 Marker,
 Geocoder,
 GeocoderRequest,
 GeocoderResult,
 BaseArrayClass
} from '@ionic-native/google-maps';

@IonicPage()
@Component({
  selector: 'page-mensajeria-ubicacion',
  templateUrl: 'mensajeria-ubicacion.html',
})
export class MensajeriaUbicacionPage {

  //Mapa
  map: GoogleMap;
  ubicacion: Ubicacion;

  //Datos de solicitud
  solicitud: SolicitudMensaje;


  constructor(private nav: NavController, private navParams: NavParams, private googleMaps: GoogleMaps, private geocoder: Geocoder, private statusbar: StatusBar) {
    let me = this;

    //Arreglamos estilos
    statusbar.overlaysWebView(true);
    statusbar.styleBlackTranslucent();

    //Obtenemos los datos enviados y cargamos el mapa
    me.solicitud = navParams.get('solicitud');
    console.log("Obtuvimso de inicio "+JSON.stringify(me.solicitud));
    setTimeout(function () {
      me.cargarMapa(me.solicitud.ubicacion);
    }, 100);

  }

  //Click Flecha
  clickBack() {
    this.nav.pop({direction: 'back'});
  }

  //Cargar mapa
  cargarMapa(ubicacion: Ubicacion){
    //Asignacion
    let me = this;
    let elemento: HTMLElement = document.getElementById("mapa"); //Div donde cargaremos mapa
    let camPos: CameraPosition = {
      target: {
        lat: ubicacion.latitud,
        lng: ubicacion.longitud
      },
      zoom: 18,
      tilt: 0
    };
    me.map = me.googleMaps.create(elemento, {
      controls: {
        myLocationButton: true,
        mapToolbar: true
      },
      gestures: {
        scroll: true,
        tilt: false,
        rotate: true,
        zoom: true
      },
      camera: camPos
    });

    //Esperamos a que el mapa este listo
    me.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      console.log("Mapa listo");
      me.map.clear();
      let markerOptions: MarkerOptions = {
        position: {lat: ubicacion.latitud, lng: ubicacion.longitud},
        flat: true,
        title: "Aquí estoy",
        icon: {
          url: 'www/assets/images/pin.png',
          size: {
            width: 44,
            height: 44
          }
        }
      };

      me.map.setCameraTarget({lat: ubicacion.latitud, lng: ubicacion.longitud}); //Posicionamos la camara

      me.map.addMarker(markerOptions)
        .then((marker: Marker) => {
          console.log("Se añadio marker");
        }).catch((error) => console.log("Error "+error));

      //Al mover camara, insertamos marcador
      me.map.on("camera_change").subscribe((evento) => {
        me.map.clear(); //Eliminamos todos los elementos de un mapa
        //Asignamos
        markerOptions.position.lat = evento.target.lat;
        markerOptions.position.lng = evento.target.lng;
        me.solicitud.ubicacion.latitud = evento.target.lat;
        me.solicitud.ubicacion.longitud = evento.target.lng;
        me.getAdress(me.solicitud.ubicacion);

        //Añadimos
        me.map.addMarker(markerOptions);
      }, error => console.log("Error moviendo camara "+error));

    });

  }

  //Obtener dirección
  getAdress(ubicacion) {
    let self = this;
    //Creamos request
    let request: GeocoderRequest = {
      position: {
        lat: ubicacion.latitud,
        lng: ubicacion.longitud
      }
    };

    //Obtenemos address
    self.geocoder.geocode(request).then((resultado: BaseArrayClass<GeocoderResult>) => {
      console.log("GEOCODE "+JSON.stringify(resultado));
      var direccion: string = null;
      for(var i=0; i < resultado[0].extra.lines.length; i++) {
        direccion = direccion === null ? resultado[0].extra.lines[i] : direccion + ", "+resultado[0].extra.lines[i];
      }
      self.solicitud.ubicacion.direccion = direccion;
    }).catch((error) => console.log("Error geocoding "+error));
  }

  //Click en ver ubicaciones guardadas
  selUbicacion() {
    let me = this;
    console.log("UBICACION -> Enviaremos a ubicaciones guardadas "+JSON.stringify(me.solicitud));
    me.nav.push('MensajeriaUbicacionListPage', { solicitud: me.solicitud });
  }

  //Click en Aquí Estoy
  setUbicacion() {
    let me = this;

    console.log("Procedemos a enviar a metodos "+JSON.stringify(me.solicitud));
    me.nav.push("MensajeriaPagoPage", { solicitud: me.solicitud });
  }
}
