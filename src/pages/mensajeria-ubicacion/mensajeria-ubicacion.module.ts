import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MensajeriaUbicacionPage } from './mensajeria-ubicacion';

@NgModule({
  declarations: [
    MensajeriaUbicacionPage,
  ],
  imports: [
    IonicPageModule.forChild(MensajeriaUbicacionPage),
  ],
  exports: [
    MensajeriaUbicacionPage
  ]
})
export class MensajeriaUbicacionPageModule {}
