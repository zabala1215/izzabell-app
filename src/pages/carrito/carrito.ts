import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ViewController, AlertController } from 'ionic-angular';
import { NativeStorage } from "@ionic-native/native-storage";

@IonicPage()
@Component({
  selector: 'page-carrito',
  templateUrl: 'carrito.html',
})
export class CarritoPage {

  total: any;
  cantidad: any;

  direccionActual: any;
  direccionSelected: any;
  usuario: any;
  direcciones: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, public alertCtrl: AlertController, public nativeStorage: NativeStorage) {
    this.total = navParams.data.total;
    this.cantidad = this.total / 20;

    this.direcciones = [];
    this.nativeStorage.getItem('user')
    .then(
      data => {
        this.usuario = data;
        if(this.usuario.direccion_casa != null) 
          this.direcciones.push({direccion: this.usuario.direccion_casa, latitud: this.usuario.casa_lat, longitud: this.usuario.casa_long});
        if(this.usuario.direccion_trabajo != null) 
          this.direcciones.push({direccion: this.usuario.direccion_trabajo, latitud: this.usuario.trabajo_lat, longitud: this.usuario.trabajo_long});
        if(this.usuario.direccion_otro != null) 
          this.direcciones.push({direccion: this.usuario.direccion_otro, latitud: this.usuario.otro_lat, longitud: this.usuario.otro_long});

        this.direccionActual = this.direcciones[0].direccion;
        this.direccionSelected = this.direcciones[0];

        console.log('direcciones', this.direcciones);
          
        console.log("Usuario recuperado: "+JSON.stringify(data));
      },
      error => {
        console.error(error);
      }
    );

  }

  vaciar() {
    let confirm = this.alertCtrl.create({
      message: '¿Desea vaciar el carrito?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancelar');
          }
        },
        {
          text: 'Vaciar',
          handler: () => {
            console.log('Vaciar clickeado');
            this.total = 0;
            this.cantidad = 0;
          }
        }
      ]
    });
    confirm.present();
  }

  pedido() {
    let item = {
      total: this.total,
      direccion: this.direccionSelected,
      pedido: true
    }

    this.viewCtrl.dismiss(item);
  }

  mas (){
    this.total = this.total + 20;
    this.cantidad++;
  }

  menos() {
    if(this.total > 0){
      	this.total = this.total - 20;
        this.cantidad--;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CarritoPage');
  }

  dismiss() {

    let item = {
      total: this.total,
      pedido: false
    }

    this.viewCtrl.dismiss(item);
  }

}
