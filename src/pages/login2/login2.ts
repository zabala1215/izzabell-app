import { Component, Input, ViewChild, trigger, state, style, transition, animate, keyframes } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform , LoadingController, AlertController} from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { TabsPage } from '../tabs/tabs';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { SelectView } from '../../custom_elems/select_view/select_view';
import { Http, Headers, RequestOptions } from '@angular/http';

/**
 * Generated class for the Login2Page page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login2',
  templateUrl: 'login2.html',
  //Animaciones
  animations: [
    trigger('logo_anim', [
      state('focus', style({
        opacity: 0,
        height: '0%'
      })),
      state('unfocus', style({
        opacity: 1,
        height: '25%',
      })),
      transition('* => focus', [
        animate('250ms ease-in', keyframes([
          style({
            opacity: 1,
            height: '25%',
            offset: 0
          }),
          style({
            opacity: 0,
            height: '0%',
            offset: 1
          })
        ]))
      ]),
      transition('focus => unfocus', [
        animate('250ms ease-out', keyframes([
          style({
            opacity: 0,
            height: '0%',
            offset: 0
          }),
          style({
            opacity: 1,
            height: '25%',
            offset: 1
          })
        ]))
      ])
    ]),
    trigger('input', [
      state('focus', style({
        margin: '5% 0px 0px 0px'
      })), 
      state('unfocus', style({
        margin: '2.5% 0px 0px 0px'
      })),
      transition('* => focus', [
        animate('250ms ease-in', keyframes([
          style({margin: '2.5% 0px 0px 0px', offset: 0}),
          style({margin: '5% 0px 0px 0px', offset: 1})
        ]))
      ]),
      transition('focus => unfocus', [
        animate('250ms ease-out', keyframes([
          style({margin: '5% 0px 0px 0px', offset: 1}),
          style({margin: '2.5% 0px 0px 0px', offset: 1})
        ]))
      ])
    ]),
    trigger('label', [
      state('focus', style({
        opacity: 1,
        margin: '7% 0px 0px 0px'
      })),
      state('unfocus', style({
        opacity: 0,
        margin: '5% 0px 0px 0px'
      })),
      transition('* => focus', [
        animate('250ms ease-in', keyframes([
          style({opacity: 0, margin: '5% 0px 0px 0px', offset: 0}),
          style({opacity: 1, margin: '7% 0px 0px 0px', offset: 1})
        ]))
      ]),
      transition('focus => unfocus', [
        animate('250ms ease-out', keyframes([
          style({opacity: 1, margin: '7% 0px 0px 0px', offset: 0}),
          style({opacity: 0, margin: '5% 0px 0px 0px', offset: 1})
        ]))
      ])
    ]),
    trigger('social_btns', [
      state('focus', style({
        opacity: 0,
        height: '0%'
      })),
      state('unfocus', style({
        height: 'auto',
        opacity: 1
      })),
      transition('* => focus', [
        animate('170ms ease-in', keyframes([
          style({opacity: 1, height: 'auto', offset: 0}),
          style({opacity: 0, height: '0%', offset: 1})
        ]))
      ]),
      transition('focus => unfocus', [
        animate('170ms ease-out', keyframes([
          style({opacity: 0, height: '0%', offset: 0}),
          style({height: 'auto', opacity: 1, offset: 1})
        ]))
      ])
    ]),
    trigger('b_enviar', [
      state('focus', style({
        opacity: 1,
        height: 'auto'
      })),
      state('unfocus', style({
        opacity: 0,
        height: '0%'
      })),
      transition('* => focus', [
        animate('400ms ease-in', keyframes([
          style({height: '0%', opacity: 0, offset: 0}),
          style({height: 'auto', opacity: 1, offset: 1})
        ]))
      ]),
      transition('focus => unfocus', [
        animate('300ms ease-out', keyframes([
          style({height: 'auto', opacity: 1, offset: 0}),
          style({opacity: 0, height: '0%', offset: 1})
        ]))
      ])
    ]),
    trigger('flecha', [
      state('focus', style({
        transform: 'translate(0%,0%)',
        opacity: 1
      })),
      state('unfocus', style({
        transform: 'translate(-100%,0%)',
        opacity: 0
      })),
      transition('* => focus', [animate('100ms ease-in')]),
      transition('focus => unfocus', [animate('100ms ease-out')])
    ])
  ]

})

export class Login2Page {
  @ViewChild('input') myInput;

  //Estado de las animaciones
  focus_state: string = "";
  enviar_state: string = "";

  //Datos de login
  credenciales: any = {telefono: null, email: null, nombre: null, picture: null, idGoogle: null, idFacebook: null};
  num_tel = "";

  selV: SelectView = new SelectView();

  paises: any; //Arreglo de paises
  public select_state: string = "cerrado";



  constructor(public http: Http, platform: Platform, public nav: NavController, private keyboard: Keyboard, private auth: AuthServiceProvider, private loadingCtrl: LoadingController, public alertCtrl: AlertController, public Facebook: Facebook, private googlePlus: GooglePlus) {
    //Para evitar que la pantalla salga fuera de vista
    platform.ready().then(() => {
      this.keyboard.disableScroll(true);
    });

    //Llenamos el arreglo de paises
    this.paises = [
      {nombre : 'panama', codigo : '+507'},
      {nombre : 'venezuela', codigo : '+58'}
    ];
  }

  //Al clickear el textbox
  getFocus() {
    console.log("Focus state: "+this.focus_state);
    console.log("Enviar state: "+this.enviar_state);
    this.focus_state = "focus"; //Cambiamos el estado de la animacion
    setTimeout(() => {
      this.enviar_state = "focus"; //Mostramos el FAB
    },200);
    console.log("CAMBIO Focus state: "+this.focus_state);
    console.log("CAMBIO Enviar state: "+this.enviar_state);
  }

  //Al clickear back button
  backBtn() {
    this.keyboard.close();
    console.log("Focus state: "+this.focus_state);
    console.log("Enviar state: "+this.enviar_state);
    this.focus_state = "unfocus";
    this.enviar_state = "unfocus";
    console.log("CAMBIO Focus state: "+this.focus_state);
    console.log("CAMBIO Enviar state: "+this.enviar_state);
  }

  

  //Al clickear en FAB de continuar
  submitLogin() {
    let nav = this.nav;
    //this.keyboard.close();

    //Creamos vista de Loading
    let loading = this.loadingCtrl.create({
      content: 'Iniciando sesión...',
      dismissOnPageChange: true
    });
    loading.present(); //Mostramos Loading

    this.credenciales.telefono = this.paises[this.selV.sel_opt].codigo+this.num_tel;
    this.auth.login(this.credenciales).subscribe(allowed => {
      console.log("Allowed: "+JSON.stringify(allowed));

      //Verificamos si procedemos a registro o si vamos a home
      if(allowed.estado == "telefono") {
        loading.dismiss(); //Cerramos view
        //Pedir telefono
        console.log("Pedimos telefono...");
        this.nav.push("LoginRegistroPage", {tipo: 'tel', usuario: allowed.usuario});
      } else if(allowed.estado == "email") {
        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });
        //Enviamos peticion SMS
        this.http.post("https://api.nexmo.com/verify/json?api_key=d5826d26&api_secret=863df48605f90546&number=507"+this.num_tel+"&brand=IzzabellApp", null,options)
        .map(res => res.json())  
        .subscribe(res => {
          let usuario = allowed.usuario;
            loading.dismiss(); //Cerramos view
            console.log("RES: "+JSON.stringify(res));
            //Verificamos el estado del SMS
            if(res.status == 10 || res.status == 0) {
              console.log("Status 0 o 10");
              nav.push('VerificarNumeroPage', {
                request_id: res.request_id,
                siguiente: 'email',
                usuario: allowed.usuario,
                sms_status: res.status
              });
            }
          }, error => {
            console.log("ERROR SMS: "+error);
          });
      } else if(allowed == true) {
        //Logueado o registrado
        loading.dismiss(); //Cerramos view
          console.log("---LOGIN CORRECTO---");
          nav.setRoot(TabsPage);
      } else {
        loading.dismiss(); //Cerramos view
        console.log("Ha ocurrido un error inesperado");
        let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: "Ha ocurrido un error inesperado",
          buttons: ['Aceptar']
        });
        alert.present();
      }

      //nav.setRoot('TabsPage');
    });
  }

  /* ----------------------------- FACEBOOK ----------------------------- */
  loginFacebook(){

    //Creamos vista de Loading
    let loading = this.loadingCtrl.create({
      content: 'Iniciando sesión...',
      dismissOnPageChange: true
    });
    loading.present(); //Mostramos Loading

    let me = this;
    let auth = me.auth;

    //Permisos: Info básica de usuario y correo electrónico
    this.Facebook.login(['public_profile', 'email'])
      .then((res: FacebookLoginResponse) => {
        //ID de Facebook
        let userId = res.authResponse.userID;
        console.log("USER ID ---> "+userId);

        let params = new Array<string>();
        this.Facebook.api("/me?fields=name,email", params)
          .then(function(user) {
            user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";

            console.log("-----DATOS DE USUARIO-----");
            console.log("USUARIO: "+JSON.stringify(user));
            console.log("----------------------");

            console.log("VIENE ASIGNACION...");

            //Creamos los credenciales
            let fbCreds = {telefono: '', email: user.email, nombre: user.name, picture: user.picture, idGoogle: '', idFacebook: user.id};

            console.log("Datos de credenciales: "+JSON.stringify(fbCreds));

            //Hacemos LOGIN
            console.log("Antes de auth login");
            auth.login(fbCreds).subscribe(allowed => {
              loading.dismiss(); //Cerramos view
              console.log("Allowed: "+JSON.stringify(allowed));

              //Verificamos si procedemos a registro o si vamos a home
              if(allowed.estado == "tel") {
                //Pedir telefono
                console.log("Pedimos telefono...");
                me.nav.push("LoginRegistroPage", {tipo: 'tel', usuario: allowed.usuario});
              } else if(allowed.estado == "email") {
                //Pedir email
                console.log("Pedimos E-Mail...");
                me.nav.push("LoginRegistroPage", {tipo: 'email', usuario: allowed.usuario});
              } else if(allowed == true) {
                //Logueado o registrado
                  console.log("---LOGIN CORRECTO---");
                  me.nav.setRoot(TabsPage);
              } else {
                console.log("Ha ocurrido un error inesperado");
                let alert = this.alertCtrl.create({
                  title: "Error",
                  subTitle: "Ha ocurrido un error inesperado",
                  buttons: ['Aceptar']
                });
                alert.present();
              }
            });

          })
      })
      .catch(e => {
        console.log('Error logging into Facebook', e);
        loading.dismiss();
      });

  }

  /* ----------------------------- GOOGLE ----------------------------- */
  loginGoogle() {

    //Creamos vista de Loading
    let loading = this.loadingCtrl.create({
      content: 'Iniciando sesión...',
      dismissOnPageChange: true
    });
    loading.present(); //Mostramos Loading
    let me = this;
    let auth = me.auth;
    //Hacemos login
    this.googlePlus.login({
      'webClientId': '768009593256-chbniq7jgbq66vmqtrnqapsm4g7rj6ju.apps.googleusercontent.com', //ID de cliente Web. Requerido solo en Android
      'offline': true
    })
      .then(function(res) {
        /* --- Logueo correcto --- */

        console.log("-----DATOS DE USUARIO-----");
        console.log("USUARIO: "+JSON.stringify(res));
        console.log("----------------------");

        //Creamos los credenciales
        let googleCreds = {telefono: '', email: res.email, nombre: res.displayName, picture: res.imageUrl, idGoogle: res.userId, idFacebook: ''};

        console.log("Datos de credenciales: "+JSON.stringify(googleCreds));

        //Hacemos LOGIN
        auth.login(googleCreds).subscribe(allowed => {
          loading.dismiss(); //Cerramos view
          console.log("Allowed: "+JSON.stringify(allowed));

          //Verificamos si procedemos a registro o si vamos a home
          if(allowed.estado == "tel") {
            //Pedir telefono
            console.log("Pedimos telefono...");
            me.nav.push("LoginRegistroPage", {tipo: 'tel', usuario: allowed.usuario});
          } else if(allowed.estado == "email") {
            //Pedir email
            console.log("Pedimos E-Mail...");
            me.nav.push("LoginRegistroPage", {tipo: 'email', usuario: allowed.usuario});
          } else if(allowed == true) {
            //Logueado o registrado
              console.log("---LOGIN CORRECTO---");
              me.nav.setRoot(TabsPage);
          } else {
            console.log("Ha ocurrido un error inesperado");
            let alert = this.alertCtrl.create({
              title: "Error",
              subTitle: "Ha ocurrido un error inesperado",
              buttons: ['Aceptar']
            });
            alert.present();
          }
        });        

      })
      .catch(function(err) {
        /* --- Error --- */
        loading.dismiss();
        console.log('error: '+err);
      });
  }

}