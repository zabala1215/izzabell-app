import { Component, NgZone, ViewChild, trigger, transition, style, state, animate, group } from '@angular/core';
import { IonicPage , NavController, ModalController, ActionSheetController, ToastController, Platform, LoadingController, Loading, Events, AlertController , Tabs} from 'ionic-angular';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';


import { Keyboard } from '@ionic-native/keyboard';

import { SolicitudserviceProvider } from '../../providers/solicitudservice/solicitudservice';
import { RatingServiceProvider } from '../../providers/rating-service/rating-service';
import { SolicitudEnviadaPage } from '../../pages/solicitud-enviada/solicitud-enviada';
import { SolicitudPage } from '../solicitud/solicitud';

import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { NativeStorage } from '@ionic-native/native-storage';
import { Media, MediaObject } from '@ionic-native/media';
import { Vibration } from '@ionic-native/vibration';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
    trigger('fade', [
      state('visible', style({
        opacity: 1
      })),
      state('invisible', style({
        opacity: 0
      })),
      transition('invisible <=> visible', animate('200ms linear'))
    ]),
     trigger('fade2', [
      state('visible', style({
        opacity: 1
      })),
      state('invisible', style({
        opacity: 0
      })),
      transition('invisible <=> visible', animate('200ms linear'))
    ]),
    trigger('fade3', [
      state('visible', style({
        opacity: 1
      })),
      state('invisible', style({
        opacity: 0
      })),
      transition('invisible <=> visible', animate('200ms linear'))
    ]),
    trigger('fade4', [
      state('visible', style({
        opacity: 1
      })),
      state('invisible', style({
        opacity: 0
      })),
      transition('invisible <=> visible', animate('200ms linear'))
    ]),
    trigger('mic', [
      state('prev_press', style({
        transform: 'scale(1.4)'
      })),
      state('press', style({
        transform: 'scale(1.6)'
      })),
      state('nopress', style({
        transform: 'scale(1)'
      })),
      transition('nopress => prev_press', animate('100ms ease-in')),
      transition('prev_press => press', animate('30ms ease-in')),
      transition('* => nopress', animate('100ms ease-out'))
    ]),
    trigger('fade5', [
      state('visible', style({
        opacity: 1
      })),
      state('invisible', style({
        opacity: 0
      })),
      transition('invisible <=> visible', animate('200ms linear'))
    ]),
    trigger('flyInOut', [
      state('in', style({
        transform: 'translate(0, 0)'
      })),
      state('out', style({
        transform: 'translate(0, -300%)'
      })),
      transition('in => out', animate('200ms ease-in')),
      transition('out => in', animate('200ms ease-out'))
    ])
  ]
})
export class HomePage {
  tabBarElement: any;
  tab:Tabs;

  //Constructor alternativo
  media: Media = new Media(); //Inicializador de grabar audio
  vibration: Vibration = new Vibration(); //Inicializador de vibrar
  
  press_state="nopress"; //Estado del voice note
  path_audio: any;
  nota_voz: MediaObject;
  timepo_nota: number = 0; //Tiempo de la nota de voz

  tiempo_presion: number = 0; //Tiempo presionado
  estado_solicitud: string = "voz"; //Como se esta insertando la información
  existe_voz: boolean = false; //Para comprobar si existe nota de voz
  nombre_nV: string = "";

  //adjunto: any;
  query: any;
  adjunto: any;
  solicitud = { query: '', adjunto: '', id_usuario: ''};
  resultado: any;
  idUsuario: any;

  lastImage: string = null;
  loading: Loading;
  placeholderText: String = 'Escribe aquí lo que necesitas';

  abierto: boolean = false;

  fadeState: String = 'invisible';
  fadeState2: String = 'invisible';
  fadeState3: String = 'visible';
  fadeState4: String = 'invisible';

  flyInOutState: String = 'in';
  fadeInOutState: String = 'out';

  private typewriter_text: string = "Hola, Soy Izzabell. \nTu asistente personal";
  private typewriter_display: string = "";

  constructor(public navCtrl: NavController, public auth: AuthServiceProvider, public SolicitudserviceProvider: SolicitudserviceProvider, public RatingServiceProvider: RatingServiceProvider, public modalCtrl: ModalController, private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController, public nativeStorage: NativeStorage, private keyboard: Keyboard, private zone: NgZone, public alertCtrl: AlertController) {
    
    this.nativeStorage.getItem('user')
    .then(
      data => {this.idUsuario = data.id},
      error => console.error(error)
    );
    
    this.tab = this.navCtrl.parent;

    // this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }



  //Funcion para hacer vibrar el dispositivo
  vibrar(time) {
    console.log("Func vibrar");
    try {
      this.vibration.vibrate(time);
    } catch(e) {
      console.log("Error vibrando... "+e);
    }
  }

  toMove() {
    console.log("Movio");
  }

  //Presionar para grabar
  presionaVoz(event) {
    let me = this;
    //Inicializamos
    this.press_state = "prev_press";
    this.tiempo_presion = 0;

    this.path_audio = this.file.tempDirectory.replace(/^file:\/\//, ''); //Asignamos la dirección del audio
    //Esperamos que pase medio segundo
    let timer = Observable.interval(100).subscribe(x => {
      this.tiempo_presion = this.tiempo_presion + 100; 
    });
    //Establecemos que al pasar medio segundo, empieze a grabar
    setTimeout(() => {
      if(this.tiempo_presion >= 400) {
        timer.unsubscribe();
        console.log("Unsuscribe y tiempo "+this.tiempo_presion);
        me.vibrar(50); //Intentamos hacer vibrar el dispositivo
        this.press_state = "press";
        this.nombre_nV = "voz"+this.idUsuario+new Date().getTime()+".m4a";

        try {
          //Creamos el archivo de audio
          this.file.createFile(this.file.tempDirectory, this.nombre_nV, true).then(() => {
            //Creamos el audio
            this.nota_voz = this.media.create(this.path_audio+this.nombre_nV);
            this.nota_voz.startRecord();
          });
        } catch(e) {
          console.log("Error... "+e);
        }

      }
    }, 440);
    console.log("Por si acaso "+this.tiempo_presion);
  }

  //Soltar para grabar
  quitaVoz() {
    let me = this;
    console.log("TD END");
    this.press_state = "nopress";
    this.vibrar(50); //Intentamos hacer vibrar el dispositivo
    this.nota_voz.stopRecord(); //Dejamos de grabar
    this.existe_voz = true;
    console.log("Subiendo nota de voz...");
    this.registrarSolicitud("Nota de Voz", 2);
    setTimeout(() => {
      this.nota_voz.play();
      let duration: any = me.nota_voz.getDuration();
      console.log("DURO: "+duration);
    }, 3000); //Reproducimos
  }

  typingCallback(that) {
    let total_length = that.typewriter_text.length;
    let current_length = that.typewriter_display.length;
    if (current_length < total_length) {
      if(current_length <= 4){
        that.typewriter_display += that.typewriter_text[current_length];
        var id = setTimeout(that.typingCallback, 50, that); 
      }else if(current_length > 4 && current_length < 19){
        if(current_length == 5){
          that.typewriter_display += that.typewriter_text[current_length];
          var delay = setTimeout(that.typingCallback, 600, that);
        }else{
          var id = setTimeout(that.typingCallback, 40, that);
          that.typewriter_display += that.typewriter_text[current_length];
        } 
      }else{
        if(current_length == 19){
          that.typewriter_display += that.typewriter_text[current_length];
          var delay = setTimeout(that.typingCallback, 600, that);
        }else{
          var id = setTimeout(that.typingCallback, 40, that);
          that.typewriter_display += that.typewriter_text[current_length];
        } 
      }
    } else {
      // that.typewriter_display = "";
      var id = setTimeout(that.typingCallback, 50, that);
      clearTimeout(id);
      that.toggleFade();
      that.toggleFade2();
    }
  }

  showConfirm(id) {
    this.query = '';
    let confirm = this.alertCtrl.create({
      message: 'Su solicitud ha sido enviada',
      buttons: [
        {
          text: 'Ok',
          handler: () => {
            console.log('ok clicked');
          }
        },
        {
          text: 'Ver Solicitud',
          handler: () => {
            this.SolicitudserviceProvider.getSolicitud(id)
            .then(data => {
              console.log('La solicitud generada es', data);
              this.goToSolicitudPage(data);
            });
          }
        }
      ]
    });
    confirm.present();
  }

  goToSolicitudPage(solicitud){

    this.navCtrl.push( SolicitudPage, {
      solicitud: solicitud
    });
  	
  }

  ngOnInit() {
    this.typingCallback(this);
    // this.fadeState = (this.fadeState == 'invisible') ? 'visible' : 'invisible';
  }

  toggleFade() {
    this.fadeState = (this.fadeState == 'visible') ? 'invisible' : 'visible';    
  }

  toggleFade2() {
    this.fadeState2 = (this.fadeState2 == 'visible') ? 'invisible' : 'visible';    
  }

  toggleFade3() {
    this.fadeState3 = (this.fadeState3 == 'visible') ? 'invisible' : 'visible';    
  }

  toggleFade4() {
    this.fadeState4 = (this.fadeState4 == 'visible') ? 'invisible' : 'visible';    
  }

  toggleFlyOut(){
    this.flyInOutState = 'out';
  }

  toggleFlyIn(){
    this.flyInOutState = 'in';
  }

  toggleFadeOut(){
    this.fadeInOutState = 'out';
  }

  toggleFadeIn(){
    this.fadeInOutState = 'in';
  }

  ionViewWillEnter() {
    // document.querySelector(".tabbar")['style'].display = 'none';
    // document.querySelector(".show-tabbar")['style'].display = 'none';

    // this.fadeState = 'visible';
    // this.fadeState3 = 'visible';
    // this.fadeState4 = 'invisible';

    console.log("--------------------Verificacion de datos-------------------------");
    this.nativeStorage.getItem('user')
      .then(
        data => console.log(data),
        error => console.error(error)
      ); 

    if (this.platform.is('android')) {
      this.keyboard.onKeyboardShow().subscribe(() => {
        // this.event.publish('hideTabs');
        document.querySelector(".tabbar")['style'].display = 'none';
        this.abierto = true;
        this.toggleFlyOut();
        this.toggleFade();
        this.toggleFade3();
        this.toggleFade4();
        // this.tabBarElement.style.display = 'none';
      });
      this.keyboard.onKeyboardHide().subscribe(() => {
        // this.event.publish('showTabs');
        document.querySelector(".tabbar")['style'].display = 'flex';
        this.abierto = false;
        this.toggleFlyIn();
        this.toggleFade();
        this.toggleFade3();
        this.toggleFade4();
        // this.tabBarElement.style.display = 'flex';
      });
    }
  }

  ionViewDidLoad() {

    //Asignamos la posición inicial del record view
    console.log("DID LOAD");
    let placeholderArreglo: Array <String> = ['Quiero una pizza', 'Necesito un pintor', 'Hazme el súper', 'Reservame un hotel'];
    let i = 0;
    let me = this;
    setInterval(function() {
      // console.log(placeholderArreglo[i]);

      switch(i) { 
        case 0: {
            me.zone.run(() => {
                me.placeholderText =  placeholderArreglo[i];
            }); 
            i++;
            //statements; 
            break; 
        } 
        case 1: { 
            me.zone.run(() => {
                me.placeholderText =  placeholderArreglo[i];
            }); 
            i++;
            //statements; 
            break; 
        } 
        case 2: { 
            me.zone.run(() => {
                me.placeholderText =  placeholderArreglo[i];
            }); 
            i++;
            //statements; 
            break; 
        } 
        case 3: { 
            me.zone.run(() => {
                me.placeholderText =  placeholderArreglo[i];
            }); 
            i = 0;
            //statements; 
            break; 
        } 
      }
       
    }, 5000);

  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Enviar Archivo',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Agregar Documento',
          icon: 'document',
          handler: () => {
            //Adjuntar documento
          }
        },
        {
          text: 'Imagen de la galería',
          icon: 'image',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Tomar foto',
          icon: 'camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
  // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
}

// Create a new name for the image
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}

// Copy the image to a local folder
private copyFileToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.lastImage = newFileName;
  }, error => {
    this.presentToast('Error while storing file.');
  });
}

private presentToast(text) {
  let toast = this.toastCtrl.create({
    message: text,
    duration: 3000,
    position: 'top'
  });
  toast.present();
}

// Always get the accurate path to your apps folder
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + img;
  }
}

public uploadImage() {
  // Destination URL
  var url = "http://www.izzabell.com/subirImg.php";

  // File for Upload
  var targetPath = this.pathForImage(this.lastImage);

  // File name only
  var filename = this.lastImage;

  var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : {'fileName': filename}
  };

  const fileTransfer: TransferObject = this.transfer.create();

  this.loading = this.loadingCtrl.create({
    content: 'Cargando...',
  });
  this.loading.present();

  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath, url, options).then(data => {
    this.loading.dismissAll()
    this.presentToast('Se ha subido la imagen.');
    this.openModal();
  }, err => {
    this.loading.dismissAll()
    this.presentToast('Error al subir la imagen.');
  });
}

uploadAudio() {
  // Destination URL
  var url = "http://www.izzabell.com/subirImg.php";

  // File for Upload
  var targetPath = this.path_audio+this.nombre_nV;

  // File name only
  var filename = this.nombre_nV;

  var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : {'fileName': filename}
  };

  const fileTransfer: TransferObject = this.transfer.create();

  this.loading = this.loadingCtrl.create({
    content: 'Cargando...',
  });
  this.loading.present();

  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath, url, options).then(data => {
    this.loading.dismissAll()
    this.presentToast('Se ha subido el audio.');
    this.openModal();
  }, err => {
    this.loading.dismissAll()
    this.presentToast('Error al subir el audio.');
  });
}

  openModal() {
    let myModal = this.modalCtrl.create(SolicitudEnviadaPage);
    myModal.present();
    this.query = '';
  }

  cambiarEstado() {
    this.estado_solicitud = "texto";
  }

  registrarSolicitud(query, tipo){

    console.log(query);
    this.adjunto = null; //Inicializamos

    console.log('El id usuario es:', this.idUsuario);
    console.log("DATE "+new Date().getTime());

    this.solicitud.query = query;
    if(this.existe_voz) {
      this.solicitud.adjunto = this.nombre_nV;
      query = "Nota de Voz";
    } else {
      this.solicitud.adjunto = this.lastImage;
    }
    this.solicitud.id_usuario = this.idUsuario;

    this.SolicitudserviceProvider.addSolicitud('nombre='+query+'&adjunto='+this.solicitud.adjunto+'&id_usuario='+this.solicitud.id_usuario+'&tipo=0').then((result) => {
      this.resultado = result;
      console.log('resultado ', this.resultado.rest);

      if(result != null && result != false){
          console.log(result);
          if(this.existe_voz) {
            console.log("nombreNV: "+this.nombre_nV);
            this.uploadAudio();
          } else if(this.lastImage) {
            console.log('lastimage:', this.lastImage);
            this.uploadImage();
          } else {
            this.showConfirm(this.resultado.rest);
          }
      }
    }, (err) => {
      console.log(err);
    });
  }

}

@Component({
  selector: 'modal-voz',
  template: '<p>Holaaaaa</p>'
})
class Prueba {

 constructor() {
   console.log("Prueba de Modal");
 }

}