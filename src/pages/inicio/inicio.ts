import { Component, ViewChild, trigger, transition, style, state, animate, keyframes } from '@angular/core';
import { App, IonicPage, NavController, Platform, ActionSheetController, Tabs, AlertController, LoadingController } from 'ionic-angular';

import { Observable } from 'rxjs/Rx';

import { Keyboard } from '@ionic-native/keyboard';
import { NativeStorage } from '@ionic-native/native-storage';

//Adjuntar Foto
import { Camera } from '@ionic-native/camera';
//Adjuntar Audio
import { Media, MediaObject } from '@ionic-native/media';
import { Vibration } from '@ionic-native/vibration';
//Manejar y enviar archivos
import { FilePath } from '@ionic-native/file-path';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';

//Paginas
import { MensajeriaInicioPage } from '../../pages/mensajeria-inicio/mensajeria-inicio';

//Proveedores
import { SolicitudserviceProvider } from '../../providers/solicitudservice/solicitudservice';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
  animations: [
    trigger('voz_view', [
      state('grabando', style({
        visibility: "visible",
        left: "0%"
      })),
      state("send_prepare", style({
        visibility: "visible",
        left: "0%"
      })),
      state("nopress", style({
        visibility: "hidden",
        left: "100%"
      })),
      transition('* => grabando', animate('75ms linear')),
      transition('send_prepare => nopress', animate('50ms linear')),
    ]),
    trigger('input_view', [
      state('grabando', style({
        visibility: "hidden"
      })),
      state("send_prepare", style({
        visibility: "hidden"
      })),
      state("nopress", style({
        visibility: "visible"
      })),
      transition('* => grabando', animate('75ms linear')),
      transition('send_prepare => nopress', animate('50ms linear')),
    ]),
    trigger('busqueda_view', [
      state('oscuro', style({
        "background": "rgba(0,0,0,0.5)"
      })),
      state('claro', style({
        "background": "rgba(0,0,0,0.05)"
      })),
      transition('claro <=> oscuro', animate('50ms linear')),
    ]),
    trigger('container_audio', [
      state('grabando', style({
        visibility: "visible",
        top: "100%"
      })),
      state('send_prepare', style({
        visibility: "visible",
        top: "100%"
      })),
      state('nopress', style({
        visibility: "hidden",
        top: "0%"
      })),
      transition('* => grabando', animate('75ms linear')),
      transition('* => send_press', animate('50ms linear')),
      transition('* => nopress', animate('50ms linear')),
    ]),
  ]
})
export class InicioPage {
  @ViewChild('query_input') queryInput;
  tabBarElement: any;
  tab:Tabs;

  //Constructor alternativo
  media: Media = new Media(); //Inicializador de grabar audio
  vibration: Vibration = new Vibration(); //Inicializador de vibrar

  //Nota de Voz
  path_audio: any;
  nota_voz: MediaObject;
  timepo_nota: number = 0; //Tiempo de la nota de voz
  tiempo_presion: number = 0; //Tiempo presionado
  estado_solicitud: string = "voz"; //Como se esta insertando la información
  existe_voz: boolean = false; //Para comprobar si existe nota de voz
  nombre_nV: string = ""; //Nombre de la nota de voz
  current_nv: string = "" //Identificador de la nota de voz actual
  duracion: any = -1; //Duracion de la nota de voz
  tiempo_grabando: number = 0;

  //Estado de animaciones
  playing = false;
  linea_anim_play_state = "running";
  contador: any;
  press_state: string = "nopress";
  busqueda_state: string = "claro";
  minutos = "00";
  segundos = "00";

  //Solicitud
  query: string = null; //Texto de busqueda
  adjunto: any; //Imagen para enviar
  idUsuario: any;
  btn_enviar: boolean = true;

  //Imagen
  lastImage: string = null; //Última imagen adjuntada

  constructor(public app: App, private navCtrl:NavController, private platform: Platform, private keyboard: Keyboard, private actionSheetCtrl: ActionSheetController, private nativeStorage: NativeStorage, private file: File, private filePath: FilePath, private solicitud: SolicitudserviceProvider, private alertCtrl: AlertController, private transfer: Transfer, private loadingCtrl: LoadingController, private camera: Camera) {
    //Para evitar que la pantalla salga fuera de vista
    platform.ready().then(() => {
      this.keyboard.disableScroll(true);
    });
    
    //Obtenemos el id del usuario
    this.nativeStorage.getItem('user')
    .then(
      data => {this.idUsuario = data.id},
      error => console.error(error)
    );
    
    this.tab = this.navCtrl.parent;
  }

  //Deshabilitar teclado al tocar fuera de la pantalla
  disableFocus() {
    this.queryInput.blur();
  }

  //Cambiar entre voz y texto
  cambiarEstado() {
    var str: String = this.query;
    console.log("LENGTH "+str.length);
    if(str.length > 0) {
      console.log("MAYOR A 0");
      //Verificamos el tipo de solicitud
      this.estado_solicitud = "texto";
      this.btn_enviar = false;
    } else if(str.length == 0) {
      this.estado_solicitud = "voz";
      this.btn_enviar = true;
    } else {
      console.log("NO ES MAYOR A 0");
      this.estado_solicitud = "voz";
      this.btn_enviar = true;
    }
    console.log("LENGTH FINAL "+this.query.length);
  }

  //Funcion para hacer vibrar el dispositivo
  vibrar(time) { try { this.vibration.vibrate(time); } catch(e) { console.log("Error vibrando... "+e); } }

  //Presionar para grabar
  presionaVoz(event) {
    //Inicializamos
    let me = this;
    me.tiempo_presion = 0;
    me.press_state = "prevoz";
    me.linea_anim_play_state = "initial";
    me.existe_voz = false;
    this.path_audio = this.file.tempDirectory.replace(/^file:\/\//, ''); //Asignamos la dirección del audio

    //Esperamos que pase medio segundo
    let timer = Observable.interval(100).subscribe(x => {
      me.tiempo_presion = me.tiempo_presion + 100; 
    });

    //Verificamos el tiempo de presion
    setTimeout(() => {
      if(this.tiempo_presion >= 400) {
        me.current_nv = "voz"+me.idUsuario+new Date().getTime();
        me.nombre_nV = me.current_nv+".m4a"; //Nombre de la nota de voz
        me.press_state = "grabando"; 
        me.busqueda_state = "oscuro";
        me.existe_voz = true;

        console.log("Unsuscribe y tiempo "+this.tiempo_presion);

        //Acciones
        timer.unsubscribe();
        me.vibrar(50); //Intentamos hacer vibrar el dispositivo

        try {
          //Creamos el archivo de audio
          this.file.createFile(this.file.tempDirectory, this.nombre_nV, true).then((nose) => {
            //Creamos el audio
            console.log("NOSE "+JSON.stringify(nose));
            this.nota_voz = me.media.create(this.path_audio+this.nombre_nV);
            this.nota_voz.onStatusUpdate.subscribe(() => console.log("STATUS UPDATE"), error => console.log("ERROR STATUS"));
            this.nota_voz.startRecord();
            //Empezamos el contador
            me.contador = Observable.interval(1000).subscribe(tiempo => {
              tiempo++;
              me.tiempo_grabando = tiempo;
              if(tiempo >= 60) {
                var mins, secs, residuo: number;
                residuo = tiempo % 60;
                if(residuo > 0) {
                  mins = ""+Math.floor(tiempo / 60);
                  secs = residuo;
                  me.minutos = me.getMins(mins);
                  me.segundos = me.getSegs(secs);
                } else {
                  me.minutos = me.getMins(Math.floor(tiempo / 60));
                  me.segundos = me.getSegs(0);
                }
              } else {
                me.segundos = me.getSegs(tiempo);
              }
            });
          });
        } catch(e) {
          console.log("Error... "+e);
        }

      }
    }, 440);
    console.log("Por si acaso "+this.tiempo_presion);
  }

  //Formatear vista de contador
  getSegs(s) {if(s < 10) { return "0"+s; } else { return s; }}
  getMins(m) {if(m < 10) { return "0"+m; } else { return m; }}

  //Soltar para grabar
  quitaVoz() {
    let me = this;
    let cont = me.contador; // Asignamos el contador a una nueva variable **LIMITACION**
    me.tiempo_presion = 0;

    //Acciones
    me.nota_voz.stopRecord();
    me.contador.unsubscribe(); //Dejamos de contar
    me.vibrar(50);

    //Verificamos que se alla grabado por lo menos un segundo
    if(me.tiempo_grabando < 1) {
      //Asignación
      me.busqueda_state = "claro";
      me.press_state = "nopress";
      me.linea_anim_play_state = "paused";
    } else {
      //Asignación
      me.press_state = "send_prepare";
      me.linea_anim_play_state = "paused";

      //Reproducimos el audio hasta lograr conseguir la duración **ERROR IONIC**
      me.nota_voz.play(); //Reproducimos
      me.nota_voz.setVolume(0.0);

      let interval = setInterval(function() {
        if(me.duracion == -1) { me.duracion = me.nota_voz.getDuration(); } 
        else {
          me.nota_voz.stop();
          clearInterval(interval);
        }
      }, 100);

    }
  }

  //Reproducir la nota de voz
  playNV() {
    //Asignación
    let me = this;
    me.linea_anim_play_state = "running";
    me.playing = true;

    me.nota_voz.setVolume(1.0); //Nos aseguramos de que el volumen este alto
    console.log("DURACION "+me.duracion);
    me.nota_voz.play();

    //Al finalizar audio, paramos animación
    setTimeout(function() {
      me.linea_anim_play_state = "paused";
      me.playing = false;
      console.log("TERMINO DE REPRODUCIR. PARAMOS");
    }, me.duracion*1000);
  }

  //Paramos la nota de voz
  stopNV() {
    let me = this;
    me.nota_voz.stop();
    me.linea_anim_play_state = "paused";
    me.playing = false;
    console.log("Paramos Audio");
  }

  //Borrar nota de voz
  deleteNV() {
    //Asignamos
    let me = this;
    me.segundos = "00";
    me.minutos = "00";
    me.timepo_nota = 0;
    me.busqueda_state = "claro";
    me.nombre_nV = "";
    me.press_state = "nopress";
    me.estado_solicitud = "voz";
    me.existe_voz = false; 
    me.nota_voz.release(); //Necesario para Android
    me.nota_voz = null; //Borramos el audio
    console.log("Busqueda state "+me.busqueda_state);
    console.log("Press state "+me.press_state);
    console.log("Estado state "+me.estado_solicitud);
    console.log("Existe state "+me.existe_voz);
  }

  //Enviar nota de voz
  sendSolicitud() {
    //Asignamos
    let me = this;
    me.adjunto = null;
    var solicitud = { query: '', adjunto: '', id_usuario: me.idUsuario}; //Formato de solicitud

    let loading = me.loadingCtrl.create({
      content: 'Cargando...',
    });
    loading.present();

    //Verificamos si es texto o voz
    if(me.estado_solicitud === "voz") {
      //Voz
      solicitud.query = "Solicitud de Voz"
      solicitud.adjunto = "audio";
    } else if(me.lastImage != null) {
      //Imagen
      solicitud.query = me.query;
      solicitud.adjunto = "imagen";
    } else {
      //Texto
      solicitud.query = me.query;
      solicitud.adjunto = "";
    }

    var str: String = this.query;
    if((me.estado_solicitud == "texto" && str.length >= 3) || me.estado_solicitud == "voz") {
      me.solicitud.addSolicitud('nombre='+solicitud.query+'&adjunto='+solicitud.adjunto+'&id_usuario='+solicitud.id_usuario).then((result) => {
        console.log("RESULTADO ---> "+JSON.stringify(result));
        loading.dismissAll();
        if(result != false) {
          let id = result['rest']; //ID de solicitud
          if(me.existe_voz) {
            console.log("<---EXISTE VOZ--->")
            console.log("nombreNV: "+this.nombre_nV);
            me.uploadFile("audio", id);
          } else if(me.lastImage) {
            console.log("<---LASIMAGE--->")
            console.log('lastimage:', this.lastImage);
            me.uploadFile("imagen", id);
          } else {
            //Mostramos aviso
            console.log("<---ELSE--->")
            loading.dismissAll();
            me.aviso("¡Solicitud enviada con exito!", true, id);
            me.resetData();
          }
        } else {
          me.aviso("¡Lo sentimos! Ha ocurrido un error inesperado", false, 0);
        }
      }, error => {
        //Error subiendo solicitud
        loading.dismissAll();
        me.aviso("¡Lo sentimos! Ha ocurrido un error inesperado", false, 0);
      });
    }
  }

  //Resetear datos 
  resetData() {
    this.query = null;
    this.lastImage = null;
    this.nota_voz = null;
    this.current_nv = null;
    this.adjunto = null;
    this.estado_solicitud = "voz";
    this.deleteNV();
    console.log("--> DATA RESETED <--");
  }

  //Mostrar dialogos de aviso
  aviso(texto, solicitud, id) {
    //Asignamos
    let me = this;
    var botones: any;

    //Verificamos si es solicitud u otro
    if(solicitud) {
      botones = [{ text: 'Aceptar' }, {
        text: 'Ver Solicitud',
        handler: () => {
          me.solicitud.getSolicitud(id).then(data => {
              console.log('La solicitud generada es', data);
              me.navCtrl.push( 'SolicitudPage', {solicitud: data});
            });}}];
    } else {
      botones = [{ text: 'Aceptar' }];
    }

    let alerta = me.alertCtrl.create({
      message: texto,
      buttons: botones
    });
    alerta.present();
  }

  //Subir archivos
  uploadFile(tipo, id) {
    let loading = this.loadingCtrl.create({
      content: 'Cargando...',
    });
    loading.present();
    //Asignamos
    let me = this;
    let url = "http://www.izzabell.com/subirImg.php";

    //Dirección y nombre segun tipo de archivo
    var targetPath = "";
    var filename = "";
    switch(tipo) {
      case "audio":
        targetPath = me.path_audio+me.nombre_nV;
        filename = "voz_"+id+".wav";
      break;
      case "imagen": 
        targetPath = cordova.file.dataDirectory+me.lastImage;
        filename = "imagen_"+id+".jpg"
      break;
    }

    var opciones = {
      fileKey: "file",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': filename}
    };

    const fileTransfer: TransferObject = this.transfer.create();

    fileTransfer.upload(targetPath, url, opciones).then(data => {
      loading.dismissAll();
      me.aviso("¡Solicitud enviada con exito!", true, id);
      me.resetData();
    }, err => {
      loading.dismissAll();
      me.aviso("¡Lo sentimos! Ha ocurrido un error enviando su solicitud de voz", false, id);
      me.resetData();
    });
  }

  public obtenerImagen(sourceType) {
    //Asignación
    let me = this;
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    //Obtenemos la imagen
    me.camera.getPicture(options).then((imagePath) => {
      //Verificamos si es Android -> El trato en android es diferente a iOS
      var fileN = new Date().getTime()+".jpg";
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, fileN);
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, fileN);
      }
    }, error => {
      me.aviso("Error obteniendo la imagen", false, 0);
    });
  }

  //Copiar imagen a folder local
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      console.log("Error copiando la imagen");
      this.aviso("Error obteniendo la imagen", false, 0);
    });
  }

  //Mostrar opciones de adjunto
  private presentActionSheet() {
    //Inicializamos
    let botones: any;

    //Añadimos el boton de eliminar
    if(this.lastImage != null) {
      botones = [
        {
          text: 'Agregar Documento',
          icon: 'document',
          handler: () => {
            //Adjuntar documento
          }
        },
        {
          text: 'Imagen de la galería',
          icon: 'image',
          handler: () => {
            this.obtenerImagen(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Tomar foto',
          icon: 'camera',
          handler: () => {
            this.obtenerImagen(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Eliminar Adjunto',
          handler: () => {
            //Eliminar adjunto
            var que = this.query;
            this.resetData();
            this.query = que;
          },
          role: 'destructive',
        }
      ]
    } else {
      botones = [
        {
          text: 'Agregar Documento',
          icon: 'document',
          handler: () => {
            //Adjuntar documento
          }
        },
        {
          text: 'Imagen de la galería',
          icon: 'image',
          handler: () => {
            this.obtenerImagen(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Tomar foto',
          icon: 'camera',
          handler: () => {
            this.obtenerImagen(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    }

    let actionSheet = this.actionSheetCtrl.create({
      title: 'Enviar Archivo',
      cssClass: 'action-sheets-basic-page',
      buttons: botones
    });
    actionSheet.present();
  }

  //Click en item
  clickItem(pagina) {
    let me = this;
    console.log("CLICK en Pagina "+pagina);

    const root = me.app.getRootNav(); //Obtenemos el root -> TabsPage
    root.push(pagina); //Cambiamos de pagina
  }

}
