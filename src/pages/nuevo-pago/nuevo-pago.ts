import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import {PagoServicioProvider} from '../../providers/pago-servicio/pago-servicio';
import { PagosServiceProvider } from '../../providers/pagos-service/pagos-service';
import { NativeStorage } from '@ionic-native/native-storage';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-nuevo-pago',
  templateUrl: 'nuevo-pago.html',
})
export class NuevoPagoPage {

  servicio: any; //Datos del servicio

  btn_disabled = true; //Deshabilitar boton de pagar

  //Datos de Pago
  datos: any;

  monto: number; //Monto
  metodo_pago: number = null; //ID del método de pago
  metodos_pagos: any; //Arreglo con los metodos de pago a seleccionar
  
  tipo_pago: string = "datos"; //Tipo de Pago(Datos, Imagen)
  imagen: string; //Foto de Recibo
  dirImagen: string; //Direccion de Foto de Recibo

  constructor(public navCtrl: NavController, public navParams: NavParams, public pago: PagoServicioProvider, public PagosServiceProvider: PagosServiceProvider, public loadingController: LoadingController, public alertController: AlertController, public nativeStorage: NativeStorage, private camera: Camera, private file: File, private transfer: Transfer) {
    //Obtenemos datos enviados
    this.servicio = navParams.get('servicio');
  }

  ionViewWillEnter() {
    //Cargamos los metodos de pago
    this.getMetodosPagos();
  }

  closeModal() {
    this.navCtrl.pop({direction: 'back'});
  }

  //Hacer nuevo pago
  nuevoPago() {
    let nav = this.navCtrl;
    let me = this;

    //Verificamos que todos los campos esten llenados
    if(me.metodo_pago >= 0 && me.monto > 0) {
      console.log("Procedemos");
      //Mostramos un dialogo de carga
      let loadingview = this.loadingController.create({
        content: "Realizando pago..."
      });
      loadingview.present();

      console.log("Tipo pago "+me.tipo_pago);
      if(me.tipo_pago == "datos") {
        console.log("Tipo datos");
        me.datos = [];
        //Construimos los datos
        for(var d=0;d < me.servicio.requerido.length; d++) {
          let dato = me.servicio.requerido[d];
          console.log("Dato "+dato);
          console.log("Dato json "+JSON.stringify(dato));
          me.datos.push([dato[0],dato[4]]);
        }
        console.log("DATOS CONSTRUIDOS "+JSON.stringify(me.datos));
      } else {
        me.datos = "img";
      }

      //Añadimos el pago
      me.pago.addServicio(me.servicio.id,me.datos, me.monto, me.metodo_pago, me.tipo_pago)
        .then(respuesta => {
          if(respuesta != false) {
            //Respuesta correcta
            //Verificamos si debemos subir imagen
            if(me.tipo_pago == "foto") {
              //Foto
              console.log("FOTO");
              //Subimos la foto con el id recibido
              if(respuesta["estado"] == 100) {
                console.log("Estado 100");
                console.log("ID "+respuesta["id"]);
                me.subirFoto(respuesta["id"],loadingview); //Subimos la foto
              } else {
                loadingview.dismiss();
                me.crearAlerta("Error","Ha ocurrido un error procesando su pago.", ['Aceptar']).present();
                nav.pop({direction: 'back'});
              }
            } else {
              //No Foto
              console.log("NO FOTO");
              loadingview.dismiss();
              me.crearAlerta("Pago Enviado","Su pago ha sido enviado exitosamente. Pronto recibira una respuesta de nuestro sistema.", ['Aceptar']).present();
              me.resetearDatos();
              nav.pop({direction: 'back'});
            }
          } else {
            //Error
            console.log("Respuesta false");
            loadingview.dismiss();
            me.crearAlerta("Error","Ha ocurrido un error procesando su pago. Intente nuevamente en unos minutos.", ['Aceptar']).present();
          }
        }, error => {
          loadingview.dismiss();
          //Ocurrio un error
          console.log("Error "+error);
          me.crearAlerta("Error","Ha ocurrido un error procesando su pago.", ['Aceptar']).present();
        });
    } else {
      console.log("Falta campos");
      //Falta campos
      me.crearAlerta("Falta rellenar campos","Por favor revise y vuelva a intentarlo",['Aceptar']);
    }
  }

  //Funcion para subir imagen
  subirFoto(id,loading) {
    let me = this;
    let nav = this.navCtrl;
    let nuevaImagen = "pago_"+id+".jpg";

    //Creamos las opciones
    var opciones = {
      fileKey: "file",
      fileName: nuevaImagen,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      params : {'fileName': nuevaImagen}
    };

    //Creamos la coneccion de transferencia
    const fileTransfer: TransferObject = me.transfer.create();

    //Subimos la imagen
    fileTransfer.upload(me.dirImagen, "http://www.izzabell.com/subirImgPagos.php", opciones)
    .then(data => {
      //Se subio correctamente
      console.log("Se subio correctamente la imagen");
      loading.dismiss();
      let datos = JSON.parse(data.response);
      console.log("DATOS "+datos);
      if(datos["error"] == 0 || datos["error"] == "0") {
        console.log("ES CERO");
        me.crearAlerta("Pago Enviado","Su pago ha sido enviado exitosamente. Pronto recibira una respuesta de nuestro sistema.", ['Aceptar']).present();
        me.resetearDatos();
        nav.pop({direction: 'back'});
      } else {
        console.log("No es cero");
        me.crearAlerta("Error","Ha ocurrido un error procesando su pago.", ['Aceptar']).present();
      }
      return true;
    }, error => {
      //Error subiendo imagen
      console.log("Error subiendo imagen "+JSON.stringify(error));
      me.crearAlerta("Error","Ha ocurrido un error procesando su pago.", ['Aceptar']).present();
      return false;
    });
  }

  //Funcion para resetear datos
  resetearDatos() {
    this.imagen = "";
    this.dirImagen = "";
    this.tipo_pago = "datos";
  }

  //Funcion para hacer nueva alerta
  crearAlerta(titulo: string, contenido: string, botones) {
    let alert = this.alertController.create({
      title: titulo,
      subTitle: contenido,
      buttons: botones
    });
    return alert;
  }

  //Obtener los métodos de pago
  getMetodosPagos(){
    let me = this;
    me.nativeStorage.getItem('user')
    .then(
      data => {
        me.PagosServiceProvider.getTarjetasId(data.id)
        .then(data => {
          me.metodos_pagos = data;
          console.log(me.metodos_pagos);
        });
      },
      error => console.error(error)
    );
    
  }

  //Tomar foto
  pagoFoto() {
    let me = this;

    //Creamos las opciones de la camara
    var opciones = {
      quality: 100,
      sourceType: me.camera.PictureSourceType.CAMERA,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    //Obtenemos la foto
    me.camera.getPicture(opciones)
    .then((dirImagen) => {
      //Cambiamos el metodo de pago y obtenemos los datos de la foto/imagen
      me.tipo_pago = "foto";
      var nombreFoto = dirImagen.substr(dirImagen.lastIndexOf('/') + 1);
      var direccionFoto = dirImagen.substr(0, dirImagen.lastIndexOf('/') +1);
      var nuevo_nombreFoto = "pago_"+me.servicio.id+new Date().getTime()+".jpg";

      //Movemos la imagen al directorio local
      me.file.copyFile(direccionFoto, nombreFoto, cordova.file.dataDirectory, nuevo_nombreFoto)
      .then(success => {
        me.imagen = nuevo_nombreFoto;
        me.dirImagen = cordova.file.dataDirectory+nuevo_nombreFoto;
      }, error => {
        me.tipo_pago = "datos";
        console.log("Error moviendo la imagen "+error);
      });
    }, (error) => {
      me.tipo_pago = "datos";
      console.log("Error obteniendo la imagen "+error);
    });

  }

}
