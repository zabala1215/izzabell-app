import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams  } from 'ionic-angular';

import { PagoServicioProvider } from '../../providers/pago-servicio/pago-servicio';


@IonicPage()
@Component({
  selector: 'page-pagos-servicio',
  templateUrl: 'pagos-servicio.html'
})
export class PagosServicioPage {

  searchQuery: string = '';
  items: any;
  itemsCargados: any;

  indice_cat = 0;
  vista = "categoria";

  carga: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public PagoServicioProvider: PagoServicioProvider) {
    this.items = navParams.get('items');
    this.itemsCargados = navParams.get('items');
  }

  initializeItems() {
    this.items = this.itemsCargados;
  }

  // getAllServicios() {
  
  //   this.PagoServicioProvider.getAllServicios()
  //   .then(data => {
  //     this.items = data;
  //     this.itemsCargados = data;
  //     this.carga = false;
  //     console.log(data);
  //   });
  // }

  getItems(ev: any) {
    
    // Reset items back to all of the items
    this.initializeItems();
    

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  irServicio(event, servicio){
  	this.navCtrl.push('NuevoPagoPage', {
      servicio: servicio
    });
  }

}
