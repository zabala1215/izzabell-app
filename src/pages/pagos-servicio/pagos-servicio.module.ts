import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PagosServicioPage } from './pagos-servicio';

@NgModule({
  declarations: [
    PagosServicioPage,
  ],
  imports: [
    IonicPageModule.forChild(PagosServicioPage)
  ],
  exports: [
    PagosServicioPage
  ]
})
export class PagosServicioPageModule {}