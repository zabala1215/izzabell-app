import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolicitudEnviadaPage } from './solicitud-enviada';

@NgModule({
  declarations: [
    SolicitudEnviadaPage,
  ],
  imports: [
    IonicPageModule.forChild(SolicitudEnviadaPage),
  ],
  exports: [
    SolicitudEnviadaPage
  ]
})
export class SolicitudEnviadaPageModule {}
