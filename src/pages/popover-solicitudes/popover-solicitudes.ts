import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-popover-solicitudes',
  templateUrl: 'popover-solicitudes.html',
})
export class PopoverSolicitudesPage {

  estado_sol: any;
  primeraVez: boolean = true;

  constructor(private navParams: NavParams, public viewCtrl: ViewController) {
    
  }

  ngOnInit() {
    if (this.navParams.data) {
      this.estado_sol = this.navParams.data.estado_sol;
    }
  }


  changeEstado() {
    if(this.primeraVez){
      this.primeraVez = false;
    }else{
      this.viewCtrl.dismiss(this.estado_sol);
    }
    if (this.estado_sol) this.estado_sol = this.estado_sol;
    
  }

}