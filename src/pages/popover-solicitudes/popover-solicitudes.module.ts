import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverSolicitudesPage } from './popover-solicitudes';

@NgModule({
  declarations: [
    PopoverSolicitudesPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverSolicitudesPage)
  ],
  exports: [
    PopoverSolicitudesPage
  ]
})
export class PopoverSolicitudesPageModule {}