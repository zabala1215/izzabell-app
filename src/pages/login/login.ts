import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading , ModalController} from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { NativeStorage } from '@ionic-native/native-storage';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

import { Http } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { AuthServiceProvider } from '../../providers/auth-service/auth-service';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  registerCredentials = {telefono: '', email: '', nombre: '', picture: '', idGoogle: '', idFacebook: ''};
  codPais;

  constructor(public nav: NavController, private nativeStorage: NativeStorage, public modalCtrl: ModalController, public Facebook: Facebook, private googlePlus: GooglePlus, public loadingCtrl: LoadingController, private auth: AuthServiceProvider, public http: Http) {
    this.codPais = 0;
  }

  // public registrar(credential) {

  //   console.log('Entrando a registrar las credenciales', credential);
  //   // let nav = this.nav;
  //   console.log('paso el nav');

  //   this.auth.loginSocial(credential).subscribe(allowed => {
  //     // console.log(allowed);
  //     if (allowed) {
  //       console.log('Login correcto');
  //       // nav.setRoot(TabsPage);
  //       this.nav.setRoot(TabsPage);
  //     }
  //   },
  //   error => {
  //     console.log(error);
  //   });
  // }

  //Guardar datos en local
  saveLocal(datos: any) {
    console.log("Entro a funcion saveLocal");
    return new Promise((resolve,reject) => {
      console.log("Entro a promise");
      this.nativeStorage.setItem('user',{
        id: datos.id,
        nombre: datos.nombre,
        telefono: datos.telefono,
        email:  datos.email,
        picture: datos.picture,
        facebook: datos.facebook,
        idSocial: datos.idSocial,
        latitud: datos.latitud,
        longitud: datos.longitud,
        altitud: datos.altitud,
        direccion_casa: datos.direccion_casa,
        direccion_trabajo: datos.direccion_trabajo,
        direccion_otro: datos.direccion_otro,
        tipo_plan: datos.tipo_plan,
        password: datos.password
      })
      .then(
        () => {
          console.log("Correcto guarod en promise");
          resolve();
        },
        error => {
          console.log("Error guardando en promise "+error);
          reject(error);
        }
      );
    });
  }

  //Login con Facebook
  doFbLogin(){
    //Dialogo de carga
    let loadingSesion = this.loadingCtrl.create({
      content: "Iniciando sesión..."
    });
    //Mostramos el dialogo de carga
    loadingSesion.present();

    //Evitar errores
    let ns = this.nativeStorage;
    let nav = this.nav;
    let credential = this.registerCredentials;
    let auth = this.auth;
    let http = this.http;
    let me = this;

    console.log("-----CREDENCIALES-----");
    console.log("Credenciales: "+JSON.stringify(credential));
    console.log("----------------------");

    //Permisos: Info básica de usuario y correo electrónico
    this.Facebook.login(['public_profile', 'email'])
      .then((res: FacebookLoginResponse) => {
        //ID de Facebook
        let userId = res.authResponse.userID;
        console.log("USER ID ---> "+userId);

        let params = new Array<string>();
        this.Facebook.api("/me?fields=name,email", params)
          .then(function(user) {
            user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";

            console.log("-----DATOS DE USUARIO-----");
            console.log("USUARIO: "+JSON.stringify(user));
            console.log("----------------------");

            //Asignamos los datos obtenidos a credenciales
            credential.nombre = user.name;
            credential.email = user.email;
            credential.picture = user.picture;

            console.log("Datos de credenciales: "+JSON.stringify(credential));

            let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
            let options = new RequestOptions({ headers: headers });

            console.log("HEADER, VA HACIA LOGIN!!!");

            //Procedemos a login
            me.auth.login(credential).subscribe(allowed => {
              console.log("LOGIN RETORNO ALGO");
              // console.log(allowed);
              if (allowed) {
                console.log('Login correcto FB');
                this.loading.dismiss();
                nav.setRoot(TabsPage);
              } else {
                console.log('No se ha registrado el usuario FB');
              }
            },
            error => {
              console.log("ERROR: -> "+error);
              this.showError(error);
            });

            //Verificamos si usuario existe
            /*http.post('http://izzabell.com/api-kingbakery/login/social/', JSON.stringify(credential), options)
            .map(res => res.json())
            .subscribe(res => {
              //Verificamos si el usuario retornado no es nulo, sino procedemos al registro
              if(res.user != null){
                console.log("El usuario existe y los datos son ", JSON.stringify(res.user));
                //Una vez que guardo, enviamos a la pantalla principal
                me.saveLocal(res.user).then(
                  () => {
                    console.log("Guardo el login de fb");
                    me.closeLoading(loadingSesion);
                    nav.setRoot(TabsPage);
                  },
                  error => {
                    console.log("Error: -----"+error);
                    me.closeLoading(loadingSesion);
                  }
                );
              }else{
                //Registramos porque no existe el usuario en servidor
                console.log('No existe el usuario');
                http.post('http://izzabell.com/api-kingbakery/usuarios/social/', JSON.stringify(credential), options)
                  .map(res => res.json())
                  .subscribe(res => {
                    console.log("Se ha registrado exitoso y los datos son", res);
                    //Verificamos si retorna usuario
                    if(res != null){
                      //Registro exitoso, retorno usuario
                      console.log('El id es:', res.id);
                      me.saveLocal(res).then(
                        () => {
                          console.log("Guardo el registro de fb");
                          me.closeLoading(loadingSesion);
                          nav.setRoot(TabsPage);
                        },
                        error => {
                          console.log("Error registro: -----"+error);
                          me.closeLoading(loadingSesion);
                        }
                      );
                    }else{
                      console.log('Error al registrar');
                    }
                      // resolve(res);
                  }, (err) => {
                    console.log(err);
                  });
              }
                // resolve(res);
            }, (err) => {
              console.log(err);
            });*/
          })
      })
      .catch(e => console.log('Error logging into Facebook', e));

  }

  loginGoogle() {
    let me = this;
    let ns = this.nativeStorage;
    //Creamos dialogo de carga
    let loadingSesion = this.loadingCtrl.create({
      content: "Iniciando sesión..."
    });

    let credential = this.registerCredentials;
    let auth = this.auth;
    let nav = this.nav;
    let http = this.http;

    //Mostramos el dialogo de carga
    loadingSesion.present();

    //Hacemos login
    this.googlePlus.login({
      'webClientId': '768009593256-chbniq7jgbq66vmqtrnqapsm4g7rj6ju.apps.googleusercontent.com', //ID de cliente Web. Requerido solo en Android
      'offline': true
    })
      .then(function(res) {
        /* --- Logueo correcto --- */

        console.log("-----DATOS DE USUARIO-----");
        console.log("USUARIO: "+JSON.stringify(res));
        console.log("----------------------");

        //credential.id = res.userId;
        credential.nombre = res.displayName;
        credential.email = res.email;
        credential.picture = res.imageUrl;

        console.log("ID: "+res.userId);
        console.log("Nombre: "+res.displayName);
        console.log("Email: "+res.email);
        console.log("Imagen: "+res.imageUrl);

        let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        let options = new RequestOptions({ headers: headers });

        http.post('http://izzabell.com/api-kingbakery/login/social/', JSON.stringify(credential), options)
            .map(res => res.json())
            .subscribe(res => {
              if(res.user != null){
                console.log("El usuario existe y los datos son ", res.user);
                me.saveLocal(res.user).then(
                  () => {
                    console.log("Guardo el login de gp");
                    me.closeLoading(loadingSesion);
                    nav.setRoot(TabsPage);
                  },
                  error => {
                    console.log("Error de gp login "+error);
                    me.closeLoading(loadingSesion);
                  }
                );
              }else{
                console.log('No existe el usuario');
                http.post('http://izzabell.com/api-kingbakery/usuarios/social/', JSON.stringify(credential), options)
                  .map(res => res.json())
                  .subscribe(res => {
                    console.log("Se ha registrado exitoso y los datos son", res);
                    if(res != null){
                      console.log('El id es:', res.id);
                      me.saveLocal(res).then(
                        () => {
                          console.log("Guardo el registro de gp");
                          me.closeLoading(loadingSesion);
                          nav.setRoot(TabsPage);
                        },
                        error => {
                          console.log("Error de gp registro "+error);
                          me.closeLoading(loadingSesion);
                        }
                      );
                    }else{
                      console.log('Error al registrar');
                    }
                      // resolve(res);
                  }, (err) => {
                    console.log(err);
                  });
              }
                // resolve(res);
            }, (err) => {
              console.log(err);
            });

        // Guardamos los datos del usuario
        // ns.setItem('user', {
        //   id: res.userId,
        //   nombre: res.displayName,
        //   email: res.email,
        //   picture: res.imageUrl
        // })
        // .then(function() {
        //     console.log("Successfull");
        //     this.registrar();
        // }, function(error) {
        //     console.log("Error"+error);
        // });

        loadingSesion.dismiss(); //Quitamos Loading Dialog

      })
      .catch(function(err) {
        /* --- Error --- */

        console.log('error: '+err);
      });
  }

  openModal() {
    let nav = this.nav; //Necesario para evitar error
    //Llamamos al modal para solicitar numero
    let myModal = this.modalCtrl.create('LoginNumeroPage');

    nav.push('LoginNumeroPage', {}, {animation: "md-transition"});
    //Al regresar, abrimos nueva pagina
    // myModal.onDidDismiss(function(registro) {
    //   if(registro) {
    //     nav.push(LoginNumeroRegistroPage);
    //   } else {
    //     console.log("No hay registro, no abre nueva ventana");
    //   }
    // });
    // myModal.present();
  }

  // openModal2() {
  //   let myModal = this.modalCtrl.create(LoginSocialPage);
  //   myModal.present();
  // }

  //Cerrar loading dialogos
  closeLoading(loading: Loading) {
    loading.dismiss();
  }

}
