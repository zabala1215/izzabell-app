import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolicitudAdicionalesPage } from './solicitud-adicionales';

@NgModule({
  declarations: [
    SolicitudAdicionalesPage,
  ],
  imports: [
    IonicPageModule.forChild(SolicitudAdicionalesPage),
  ],
  exports: [
    SolicitudAdicionalesPage
  ]
})
export class SolicitudAdicionalesPageModule {}
