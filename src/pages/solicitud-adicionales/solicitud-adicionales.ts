import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ToastController, Platform, Loading, LoadingController, ViewController, Content } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';

import { SolicitudserviceProvider } from '../../providers/solicitudservice/solicitudservice';

import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { NativeStorage } from '@ionic-native/native-storage';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-solicitud-adicionales',
  templateUrl: 'solicitud-adicionales.html',
})
export class SolicitudAdicionalesPage {
  @ViewChild(Content) content: Content;
  
  adjunto: any;
  query: any;
  lastImage: string = null;
  loading: Loading;

  solicitud: any;
  resultado: any;
  comentarios: any;
  carga: boolean = true;

  placeholderText = 'Escriba aquí un comentario';
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public SolicitudserviceProvider: SolicitudserviceProvider, private camera: Camera, private transfer: Transfer, private file: File, private filePath: FilePath, public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, public platform: Platform, public loadingCtrl: LoadingController, public nativeStorage: NativeStorage, public viewCtrl: ViewController, public keyboard: Keyboard) {
    
    this.solicitud = navParams.get('solicitud');
    this.comentarios = [];

    this.keyboard.onKeyboardShow()
			.subscribe(data => {
        this.content.scrollToBottom();
			});
		this.keyboard.onKeyboardHide()
			.subscribe(data => {
        console.log('Teclado oculto');
			});
  }

  ionViewWillEnter(){
    
    console.log('Entrando a la vista');
    this.carga = true;
    let me = this;
    this.SolicitudserviceProvider.getSolicitudesComentarios(this.solicitud.id)
    .then(data => {
      me.comentarios = data;
      this.carga = false;
      console.log(data);
    });
    
  }

  addComentario(query){

    console.log(query);
    this.adjunto = null;
    this.adjunto = this.lastImage;

    this.SolicitudserviceProvider.addComentarios('comentario='+query+'&adjunto='+this.lastImage+'&id_solicitud='+this.solicitud.id).then((result) => {
      this.resultado = result;
      if(this.lastImage){
        console.log('lastimage:', this.lastImage);
        this.comentarios.push({comentario: query, adjunto: this.lastImage});
        this.uploadImage();
      }else{
        this.comentarios.push({comentario: query, adjunto: null});
      }
      this.content.scrollToBottom();
      this.query = '';
      console.log('resultado ', this.resultado);
    }, (err) => {
      console.log(err);
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Enviar Archivo',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Agregar Documento',
          icon: 'document',
          handler: () => {
            //Adjuntar documento
          }
        },
        {
          text: 'Imagen de la galería',
          icon: 'image',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Tomar foto',
          icon: 'camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
  // Create options for the Camera Dialog
    var options = {
      quality: 100,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
}

// Create a new name for the image
private createFileName() {
  var d = new Date(),
  n = d.getTime(),
  newFileName =  n + ".jpg";
  return newFileName;
}

// Copy the image to a local folder
private copyFileToLocalDir(namePath, currentName, newFileName) {
  this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    this.lastImage = newFileName;
  }, error => {
    this.presentToast('Ha ocurrido un error mientras se almacenaba el archivo.');
  });
}

private presentToast(text) {
  let toast = this.toastCtrl.create({
    message: text,
    duration: 3000,
    position: 'top'
  });
  toast.present();
}

// Always get the accurate path to your apps folder
public pathForImage(img) {
  if (img === null) {
    return '';
  } else {
    return cordova.file.dataDirectory + img;
  }
}

public uploadImage() {
  // Destination URL
  var url = "http://www.izzabell.com/subirImg.php";

  // File for Upload
  var targetPath = this.pathForImage(this.lastImage);

  // File name only
  var filename = this.lastImage;

  var options = {
    fileKey: "file",
    fileName: filename,
    chunkedMode: false,
    mimeType: "multipart/form-data",
    params : {'fileName': filename}
  };

  const fileTransfer: TransferObject = this.transfer.create();

  this.loading = this.loadingCtrl.create({
    content: 'Cargando...',
  });
  this.loading.present();

  // Use the FileTransfer to upload the image
  fileTransfer.upload(targetPath, url, options).then(data => {
    this.loading.dismissAll()
    this.presentToast('Se ha subido la imagen.');
    this.lastImage = null;
  }, err => {
    this.loading.dismissAll()
    this.presentToast('Error al subir la imagen.');
  });
}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SolicitudAdicionalesPage');
  }

  dismiss() {
    //No se escogio dirección
    this.viewCtrl.dismiss();
  }

}
