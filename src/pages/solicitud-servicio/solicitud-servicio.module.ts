import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolicitudServicioPage } from './solicitud-servicio';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    SolicitudServicioPage,
  ],
  imports: [
    IonicPageModule.forChild(SolicitudServicioPage),
    Ionic2RatingModule
  ],
  exports: [
    SolicitudServicioPage
  ]
})
export class SolicitudServicioPageModule {}