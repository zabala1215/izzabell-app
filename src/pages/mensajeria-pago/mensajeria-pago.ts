import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//Nativo
import { NativeStorage } from '@ionic-native/native-storage';

//Modelos
import { MetodoPago } from '../../custom_elems/metodo_pago/metodo_pago';
import { SolicitudMensaje } from '../../custom_elems/solicitud_mensaje/solicitud_mensaje';

//Proovedores
import { PagosServiceProvider } from '../../providers/pagos-service/pagos-service';

@IonicPage()
@Component({
  selector: 'page-mensajeria-pago',
  templateUrl: 'mensajeria-pago.html',
})
export class MensajeriaPagoPage {

  //Metodos de Pago
  metodos_pago: MetodoPago[] = [];
  metodo_seleccionado: number = null; //Indice de metodo seleccionado

  //Solicitud
  solicitud: SolicitudMensaje;

  constructor(public navCtrl: NavController, private navParams: NavParams, private nativeStorage: NativeStorage, public pagos: PagosServiceProvider) {
    let self = this;

    //Obtenemos los datos enviados
    self.solicitud = self.navParams.get('solicitud');
    console.log("Recibimos de ubicacion "+JSON.stringify(self.solicitud));
  }

  //Al cargar vista
  ionViewDidLoad() {
    console.log("Cargo vista");
    //Obtenemos los métodos de pago
    this.getMetodosPagos(); 
  }

  //Click Flecha
  clickBack() {
    this.navCtrl.pop({direction: 'back'});
  }

  //Click en un metodo 
  selMetodo(metodo: number) {
    //Asignamos
    let self = this;
    self.metodo_seleccionado = metodo; //Indice recibido
    self.solicitud.metodo_pago = self.metodos_pago[metodo]; //Asignamos a la solicitud
  }

  //Click en Seleccionar
  setMetodo() {
    let me = this;
    me.solicitud.metodo_pago = me.metodos_pago[me.metodo_seleccionado]; //Asignamos metodo

    console.log("Enviamos solicitud final "+JSON.stringify(me.solicitud));
    me.navCtrl.push("MensajeriaRevisarPage", { solicitud: me.solicitud});
  }

  //Obtener los métodos de pago
  getMetodosPagos() {
    let me = this;
    me.pagos.getTarjetasId(me.solicitud.usuario)
      .then(data => {
        console.log("Data retornada "+JSON.stringify(data));
        me.metodos_pago = data;
        console.log("Metodo retornado "+JSON.stringify(me.metodos_pago));
      });
  }

}
