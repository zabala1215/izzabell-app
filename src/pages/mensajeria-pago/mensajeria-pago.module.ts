import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MensajeriaPagoPage } from './mensajeria-pago';

@NgModule({
  declarations: [
    MensajeriaPagoPage,
  ],
  imports: [
    IonicPageModule.forChild(MensajeriaPagoPage),
  ],
  exports: [
    MensajeriaPagoPage
  ]
})
export class MensajeriaPagoPageModule {}
