import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServiciosProvider } from '../../providers/servicios/servicios';


@IonicPage()
@Component({
  selector: 'page-servicios',
  templateUrl: 'servicios.html',
})
export class ServiciosPage {

  searchQuery: string = '';
  items: any;
  itemsCargados: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public ServiciosProvider: ServiciosProvider) {
    this.items = navParams.get('items');
    this.itemsCargados = navParams.get('items');
  }

  initializeItems() {
    this.items = this.itemsCargados;
  }

  // getAllServicios() {
  
  //   this.ServiciosProvider.getAllServicios()
  //   .then(data => {
  //     this.items = data;
  //     this.itemsCargados = data;
  //     // this.carga = false;
  //     console.log(data);
  //   });
  // }

  cargarServicio(servicio) {
    this.navCtrl.push('ServicioPage', {
      servicio: servicio
    });
  }

  getItems(ev: any) {
    
    console.log(ev);
    // Reset items back to all of the items
    this.initializeItems();
    

    // set val to the value of the searchbar
    let val = ev;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.nombre.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServiciosPage');
  }

}
