import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiciosPage } from './servicios';

@NgModule({
  declarations: [
    ServiciosPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiciosPage)
  ],
  exports: [
    ServiciosPage
  ]
})
export class ServiciosPageModule {}