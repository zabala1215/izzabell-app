import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginNumeroPage } from './login-numero';

@NgModule({
  declarations: [
    LoginNumeroPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginNumeroPage),
  ],
  exports: [
    LoginNumeroPage
  ]
})
export class LoginNumeroPageModule {}
