import { Component, Input, ViewChild } from '@angular/core';
import { IonicPage, App, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { ViewController } from 'ionic-angular';
import { AuthServiceProvider } from '../../providers/auth-service/auth-service';
import { NativeStorage } from '@ionic-native/native-storage';

import { Keyboard } from '@ionic-native/keyboard';

@IonicPage()
@Component({
  selector: 'page-login-numero',
  templateUrl: 'login-numero.html'
})
export class LoginNumeroPage {

  loading: Loading;
  registerCredentials = {telefono: '', email: '', nombre: '', picture: '', idGoogle: '', idFacebook: ''};
  // @ViewChild('input') myInput ;

  constructor(public nav: NavController, public navParams: NavParams, public viewCtrl: ViewController, private auth: AuthServiceProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private app: App, private keyboard: Keyboard) {
  }

  closeModal() {
    this.nav.pop({animation: "md-transition", direction: 'back'});
  }

  public createAccount() {
    this.nav.push('RegisterPage');
  }

  public login() {

    let nav = this.nav;
    this.showLoading() //Mostramos dialogo de carga

    this.auth.login(this.registerCredentials).subscribe(allowed => {
      // console.log(allowed);
      if (allowed) {
        console.log('Login correcto');
        this.loading.dismiss();
        nav.setRoot(TabsPage);
      } else {
        console.log('No se ha registrado el usuario');
        //Enviamos 1 para que si no se recibe dato al dismiss, no habra ventana de registrar nombre
        // let registro = 1;
        // this.viewCtrl.dismiss(registro); //Quitamos modal y enviamos dato de regreso
        //Enviamos a la siguiente pantalla
        nav.push('LoginNumeroRegistroPage', {}, {animation: 'md-transition'});
      }
    },
    error => {
      this.showError(error);
    });
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Por favor espere...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  // ionViewDidLoad() {
  //   console.log('ionViewDidLoad LoginNumeroPage');
  //   setTimeout(() => {
  //     this.myInput.setFocus();
  //     this.keyboard.show; //for android
  //   },150);
  // }

}
