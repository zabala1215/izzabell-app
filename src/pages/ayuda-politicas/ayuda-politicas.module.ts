import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AyudaPoliticasPage } from './ayuda-politicas';

@NgModule({
  declarations: [
    AyudaPoliticasPage,
  ],
  imports: [
    IonicPageModule.forChild(AyudaPoliticasPage),
  ],
  exports: [
    AyudaPoliticasPage
  ]
})
export class AyudaPoliticasPageModule {}
