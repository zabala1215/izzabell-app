import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { EditarCuentaPage } from '../editar-cuenta/editar-cuenta';
// import { PlanesPage } from '../planes/planes';
// import { PagosPage } from '../pagos/pagos';
// import { CajeroPage } from '../cajero/cajero';
// import { AyudaPage } from '../ayuda/ayuda';
// import { AfiliadosPage } from '../afiliados/afiliados';
import { UsuarioServiceProvider } from '../../providers/usuario-service/usuario-service';
import { NativeStorage } from '@ionic-native/native-storage';



@IonicPage()
@Component({
  selector: 'page-ajustes',
  templateUrl: 'ajustes.html',
})
export class AjustesPage {

  usuario: any;
  idUsuario: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public UsuarioServiceProvider: UsuarioServiceProvider, private nativeStorage: NativeStorage) {
    this.usuario = {};
  }

  ionViewWillEnter() {
    //No es necesario traer datos del servidor en Ajustes
    /*this.nativeStorage.getItem('user')
      .then(
        data => {this.getUsuario(data.id)},
        error => console.error(error)
      );*/
  }

  getUsuario(id) {
    //No es necesario traer datos del servidor en Ajustes
    /*this.UsuarioServiceProvider.getUsuarioId(id)
    .then(data => {
      this.usuario = data;
      console.log(this.usuario);
    });*/
  }

  goToSolicitudesTab(){
    this.navCtrl.parent.select(0);
  }

  goToCuentaPage(){
  	this.navCtrl.push('CuentaPage', {usuario: this.usuario} );
  }

  goToEditarCuentaPage(){
  	this.navCtrl.push( 'EditarCuentaPage', {usuario: this.usuario} );
  }

  goToPlanesPage(){
  	this.navCtrl.push( 'PlanesPage' );
  }

  goToPagosPage(){
  	this.navCtrl.push( 'BilleteraPage', {tipoVista : 0} );
  }

  goToAyudaPage(){
  	this.navCtrl.push( 'AyudaPage' );
  }

  goToComoFuncionaPage(){
  	this.navCtrl.push( 'ComoFuncionaPage' );
  }

  goToAfiliadosPage(){
  	this.navCtrl.push( 'AfiliadosPage' );
  }

  goToServiciosPage(){
  	this.navCtrl.push( 'CategoriaServiciosPage' );
  }

  goToPagoServiciosPage(){
  	this.navCtrl.push( 'CategoriaPagosPage' );
  }

  goToLoQueQuieras(){
  	this.navCtrl.push( 'LoQueQuierasPage' );
  }

  goToCajero(){
  	this.navCtrl.push( 'CajeroPage', null , {animate: false} );
  }

  goToAsistencia(){
  	this.navCtrl.push( 'CategoriaAsistenciaPage' );
  }

}
