import { Component } from '@angular/core';

/**
 * Generated class for the BackgroundImageComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'background-image',
  templateUrl: 'background-image.html'
})
export class BackgroundImageComponent {

  text: string;

  constructor() {
    console.log('Hello BackgroundImageComponent Component');
    this.text = 'Hello World';
  }

}
