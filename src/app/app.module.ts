import { NgModule, ErrorHandler, NgZone, ViewChild, Pipe, PipeTransform } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

//IONIC NATIVE PLUGINS
import { NativeStorage } from '@ionic-native/native-storage';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { Keyboard } from '@ionic-native/keyboard';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { CardIO } from '@ionic-native/card-io';
import { PayPal } from '@ionic-native/paypal';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Vibration } from '@ionic-native/vibration';
import { NativeAudio } from '@ionic-native/native-audio';
import { GoogleMaps, Geocoder } from '@ionic-native/google-maps';

//DIRECTIVAS
// import { CreditCardDirectivesModule } from 'angular-cc-library';
// import { Ionic2RatingModule } from 'ionic2-rating';

//PAGINAS
import { TabsPage } from '../pages/tabs/tabs';
//import { InicioPage } from '../pages/inicio/inicio';
// import { PlanesPage } from '../pages/planes/planes';
// import { PagosPage } from '../pages/pagos/pagos';
// import { EditarCuentaPage } from '../pages/editar-cuenta/editar-cuenta';
// import { EditarValorPage } from '../pages/editar-valor/editar-valor';
// import { NuevaDireccionPage } from '../pages/nueva-direccion/nueva-direccion';
// import { NuevaTarjetaPage } from '../pages/nueva-tarjeta/nueva-tarjeta';
// import { AutocompletePage } from '../pages/autocomplete/autocomplete';
// import { SolicitudEnviadaPage } from '../pages/solicitud-enviada/solicitud-enviada';
// import { ComoFuncionaPage } from '../pages/como-funciona/como-funciona';
// import { AyudaPage } from '../pages/ayuda/ayuda';
// import { AfiliadosPage } from '../pages/afiliados/afiliados';
// import { AyudaPoliticasPage } from '../pages/ayuda-politicas/ayuda-politicas';
// import { AyudaTerminosPage } from '../pages/ayuda-terminos/ayuda-terminos';
// import { RatingModalPage } from '../pages/rating-modal/rating-modal';
// import { LoginPage } from '../pages/login/login';
// import { LoginSocialPage } from '../pages/login-social/login-social';
// import { LoginNumeroPage } from '../pages/login-numero/login-numero';
// import { LoginNumeroRegistroPage } from '../pages/login-numero-registro/login-numero-registro';
// import { InicioSlidesPage } from '../pages/inicio-slides/inicio-slides';
// import { AfiliateFormPage } from '../pages/afiliate-form/afiliate-form';

//PROVIDERS
import { SolicitudserviceProvider } from '../providers/solicitudservice/solicitudservice';
import { UsuarioServiceProvider } from '../providers/usuario-service/usuario-service';
import { PagosServiceProvider } from '../providers/pagos-service/pagos-service';
import { RatingServiceProvider } from '../providers/rating-service/rating-service';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { PagoServicioProvider } from '../providers/pago-servicio/pago-servicio';
import { ServiciosProvider } from '../providers/servicios/servicios';
import { BackgroundImageComponent } from '../components/background-image/background-image';
import { MensajeriaProvider } from '../providers/mensajeria/mensajeria';
import { EnLaCalleProvider } from '../providers/en-la-calle/en-la-calle';
import { CajeroProvider } from '../providers/cajero/cajero';

// import { ArrayFilterPipe } from "../app/pipes/solicitudes-filter.pipe";

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    BackgroundImageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule, 
    IonicModule.forRoot(MyApp, {
      mode: 'ios',
      backButtonText: '',
      platforms: {
        ios: {
          pageTransition: 'ios-transition'
        },
        md: {
          pageTransition: 'md-transition'
        }
      }
    })  
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    File,
    Transfer,
    Camera,
    FilePath,
    NativeStorage,
    Keyboard,
    Facebook,
    GooglePlus,
    Geolocation,
    Diagnostic,
    CardIO,
    PayPal,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SolicitudserviceProvider,
    UsuarioServiceProvider,
    PagosServiceProvider,
    RatingServiceProvider,
    AuthServiceProvider,
    Vibration,
    PagoServicioProvider,
    ServiciosProvider,
    NativeAudio,
    GoogleMaps,
    Geocoder,
    MensajeriaProvider,
    EnLaCalleProvider,
    CajeroProvider
  ]
})
export class AppModule {}
