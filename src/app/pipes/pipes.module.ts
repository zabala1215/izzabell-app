import { NgModule } from '@angular/core';
import { ArrayFilterPipe } from "./solicitudes-filter.pipe";

@NgModule({
    declarations: [
        ArrayFilterPipe
    ],
    imports: [
 
    ],
    exports: [
        ArrayFilterPipe
    ]
})
export class PipesModule {}