import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { NavController, App } from "ionic-angular";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';

import { TabsPage } from '../pages/tabs/tabs';
import { Login2Page } from '../pages/login2/login2';
import { InicioSlidesPage } from '../pages/inicio-slides/inicio-slides';
import { InicioPage } from '../pages/inicio/inicio';

import { AuthServiceProvider } from '../providers/auth-service/auth-service';


@Component({
  template: '<ion-nav [root]="rootPage"></ion-nav>',
  
})
export class MyApp {

  private rootPage: any;

  constructor(private platform: Platform, StatusBar: StatusBar, SplashScreen: SplashScreen, public NativeStorage: NativeStorage, private app: App, private auth: AuthServiceProvider, public events: Events) {
    // this.rootPage = LoginPage;

    // enableProdMode();
    platform.ready().then(() => {
      let env = this;
     /*NativeStorage.getItem('primeraVez')
      .then(function (data) {
        NativeStorage.getItem('user')
        .then( function (data) {
          
          env.rootPage = TabsPage;
          SplashScreen.hide();
        }, function (error) {

          env.rootPage = 'Login2Page';
          SplashScreen.hide();
        });
      }, function (error) {
        NativeStorage.setItem('primeraVez', {
          estado: false
        })
        env.rootPage = 'InicioSlidesPage';
        SplashScreen.hide();
      });
      
      // SplashScreen.hide();
      StatusBar.styleDefault();*/

      env.rootPage = TabsPage;
          SplashScreen.hide();

      this.events.subscribe('user:logout', () => { 
          this.app.getRootNav().setRoot('Login2Page'); 
      });
    });
  }
}
